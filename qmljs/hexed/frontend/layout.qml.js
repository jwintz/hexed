.pragma library

// --

.import QtQuick          as Q
.import QtQuick.Controls as Q

.import hexed.Backend    as H

// --

var component;
var container;
var highlight;
var placehold;

var bt_id = 1;
var ct_id = 1;
var bt = null;

const minimum_window_width = 250;
const minimum_window_height = 150;

// -- Core Helpers

function Timer() {
    return Q.Qt.createQmlObject("import QtQuick; Timer {}", placehold);
}

// -- API

function binary_tree() {
    
    var self = this;
    
    this.root = null;
    this.curt = null;
    
    self.make = function(value) {
        var node = {};
        node.parent = null;
        node.value = value;
        node.file = null;
        node.left = null;
        node.right = null;
        node.item = null;
        
        node.line = 1;
        node.column = 1;
        
        return node;
    };

    self.del = function() {
        
        let node = this.curt.parent;
        
        if(!node)
            return;
        
        var is_left  = this.curt == this.curt.parent.left;
        var is_right = this.curt == this.curt.parent.right;
        
        let other = is_left ? this.curt.parent.right : this.curt.parent.left;
        
        node.item.destroy();
    
        function build(n, o) {
        
            if(!o)
                return;
        
            if (o.value) {
                
                var parent = n.parent ? n.parent.item : placehold;
                
                n.item = component.createObject(parent, { value: o.value });
                n.item.objectName = "rect" + o.value;
                n.item.parent = parent;
                n.value = o.value;
                
                n.left = null;
                n.right = null;
                
                if (parent == placehold)
                    n.item.anchors.fill = parent;
                else
                    n.item.parent.insertItem(1, n.item);
                
                H.Buffers.set(n, o.file, o);
            
            } else {
                
                var parent = n.parent ? n.parent.item : placehold;
                
                n.item = container.createObject(parent);
                n.item.objectName = "split" + ct_id++;
                n.item.orientation = o.item.orientation;
                n.item.parent = parent;
                n.value = null;
            
                if (parent == placehold)
                    n.item.anchors.fill = parent;
                else
                    n.item.parent.insertItem(1, n.item);
                
                if (o.left) {
                    n.left = self.make(null);
                    n.left.parent = n;
                    build(n.left,  o.left);
                }
                
                if (o.right) {
                    n.right = self.make(null);
                    n.right.parent = n;
                    build(n.right,  o.right);
                }
            }
        }
        
        build(node, other);
        
        function traverse(node) {
            
            if(node.value) {
                self.curt = node;
                return;
            }
            
            traverse(node.left);
            traverse(node.right);
        }
        
        traverse(node);
    }

    self.del_others = function() {
        
        if(self.curt == self.root) {
            return;
        }
        
        let node = this.curt;
        let file = this.curt.item.buffer.file;
        
        this.root.item.destroy();
        this.root = node;
        this.curt = node;
        this.curt.item = component.createObject(placehold, { value: this.curt.value });
        this.curt.item.objectName = "rect" + node.value;
        this.curt.item.anchors.fill = placehold;
        this.curt.parent = null;
        
        H.Buffers.set(this.curt, file, this.curt);
    }
    
    self.add = function(value, orientation) {
        
        if(!this.root) {
            var node = this.make(value);
            this.root = node;
            this.curt = node;
            this.curt.item = component.createObject(placehold, { value: value });
            this.curt.item.objectName = "rect" + node.value;
            this.curt.item.anchors.fill = placehold;
            
            H.Buffers.set(this.curt, '*playground*', this.curt);
            
        } else {
            this.insert(value, orientation);
        }
        
        return this;
    };
    
    self.insert = function(value, orientation) {
        
        let parent = this.curt.item.parent;
        let parent_value = this.curt.value;
        let parent_file = this.curt.file;
        
        let node = this.curt;
        node.value = null;
        node.item.destroy();
        node.item = container.createObject(parent);
        node.item.objectName = "split" + ct_id++;
        node.item.orientation = orientation;
        node.item.parent = parent;
        node.left = this.make(parent_value);
        node.right = this.make(value);
        node.file = null;
        
        if(!node.parent) {
            node.item.anchors.fill = placehold;
        } else {
            parent.insertItem(1, node.item);
        }
        
        let left = node.left;
        left.file = parent_file;
        left.item = component.createObject(node.item, { value: left.value });
        left.item.objectName = "rect" + left.value;
        left.item.parent = node.item;
        left.parent = node;
        
        let right = node.right;
        right.file = parent_file;
        right.item = component.createObject(node.item, { value: right.value });
        right.item.objectName = "rect" + right.value;
        right.item.parent = node.item;
        right.parent = node;
        
        node.item.insertItem(0, left.item);
        node.item.insertItem(1, right.item);
        
        H.Buffers.set(left, parent_file, node);
        H.Buffers.set(right, parent_file, node);
        
        self.curt = left;
    };
    
    self.set = function(value) {
        
        var traverse = function(node) {
            if (!node) return;
            if (value === node.value) {
                self.curt = node;
                return;
            }
            traverse(node.left);
            traverse(node.right);
        };
        traverse(this.root);
        return this;
    }
    
    self.find = function(value) {
        
        var traverse = function(node) {
            if (!node)
                return null;
            
            if (value === node.value) {
                return node;
            }
            
            var found = traverse(node.left);
            
            if (found)
                return found;
                
            found = traverse(node.right);
            
            if (found)
                return found;
            
            return null;
        };
        
        return traverse(this.root);
    }

    self.contains = function(value) {
        var node = this.root;
        var traverse = function(node) {
            if (!node) return false;
            if (value === node.value) {
                return true;
            } else if (value > node.value) {
                return traverse(node.right);
            } else if (value < node.value) {
                return traverse(node.left);
            }
        };
        return traverse(node);
    };
    
    self.depth = function() {
        var node = this.root;
        var maxDepth = 0;
        var traverse = function(node, depth) {
            if (!node) return null;
            if (node) {
                maxDepth = depth > maxDepth ? depth : maxDepth;
                traverse(node.left, depth + 1);
                traverse(node.right, depth + 1);
            }
        };
        traverse(node, 0);
        return maxDepth;
    };
    
    self.leaves = function() {
        var count = 0;
        var node = this.root;
        var traverse = function(node) {
            if (!node) return null;
            if (!node.left && !node.right) count++;
            else traverse(node.left) + traverse(node.right);
        };
        traverse(node);
        return count;
    };
    
    self.print = function() {
        
        var node = this.root;
        
        var traverse = function(node, level) {
            
            if(!node) return;
            
            console.log(" ".repeat(3*level), '[', node.value, node == bt.curt ? '*' : ' ', ']');
            console.log(" ".repeat(3*level), 'item:        ', node.item);
            console.log(" ".repeat(3*level), 'item.parent: ', node.item ? node.item.parent : '/');
            console.log(" ".repeat(3*level), 'item.anchors:', node.item ? node.item.anchors.fill : '/');

            traverse(node.left, level+1);
            traverse(node.right, level+1);
        }
        
        console.log('------------------------');
        
        traverse(node, 0);
        
        return this;
    }
    
    self.breadthFirstLTR = function() {
        var node = this.root;
        var queue = [node];
        var result = [];
        while (node = queue.shift()) {
            result.push(node.value);
            node.left && queue.push(node.left);
            node.right && queue.push(node.right);
        }
        return result;
    };
    
    self.breadthFirstRTL = function() {
        var node = this.root;
        var queue = [node];
        var result = [];
        while (node = queue.shift()) {
            result.push(node.value);
            node.right && queue.push(node.right);
            node.left && queue.push(node.left);
        }
        return result;
    };
    
    self.depthFirstPreOrder = function() {
        var result = [];
        var node = this.root;
        var traverse = function(node) {
            result.push(node.value);
            node.left && traverse(node.left);
            node.right && traverse(node.right);
        };
        traverse(node);
        return result;
    };
    
    self.depthFirstInOrder = function() {
        var result = [];
        var node = this.root;
        var traverse = function(node) {
            node.left && traverse(node.left);
            result.push(node.value);
            node.right && traverse(node.right);
        };
        traverse(node);
        return result;
    };

    self.depthFirstPostOrder = function(callback) {
        var result = [];
        var node = this.root;
        var traverse = function(node) {
            node.left && traverse(node.left);
            node.right && traverse(node.right);
            if (callback)
                callback(node);
            result.push(node.value);
        };
        traverse(node);
        return result;
    };
}

// --

function delay(delayTime, cb) {
    let timer = new Timer();
    timer.interval = delayTime;
    timer.repeat = false;
    timer.triggered.connect(cb);
    timer.triggered.connect(function () { timer.destroy(); });
    timer.start();
}

// --

function focus(target) {
    
    highlight.enableSmoothBehavior();
    highlight.fire();
       
    if(!target)
        return;
    
    highlight.x = placehold.mapFromItem(target.item, 0, 0).x;
    highlight.y = placehold.mapFromItem(target.item, 0, 0).y;
    highlight.width = target.item.width;
    highlight.height = target.item.height;
    
    bt.curt = target;
    bt.curt.item.forceActiveFocus();
    
    H.Hooks.run_hooks('window_focus_hooks');
}

function reveal() {
    
    delay(100, () => {
        
        if(!bt.curt.item)
            return;
        
        highlight.disableSmoothBehavior();
        
        highlight.x = placehold.mapFromItem(bt.curt.item, 0, 0).x;
        highlight.y = placehold.mapFromItem(bt.curt.item, 0, 0).y;
        highlight.width = bt.curt.item.width;
        highlight.height = bt.curt.item.height;
        
        highlight.fire();
        
        bt.curt.item.forceActiveFocus();
    });
}
