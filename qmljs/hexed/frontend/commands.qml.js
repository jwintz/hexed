.pragma library

.import QtQuick          as Q
.import QtQuick.Controls as Q

.import hexed.Core       as H
.import hexed.Backend    as H

.import "layout.qml.js"  as Layout

// --

var dialog_buffers;
var fs;
var fs_helper;
var header;
var highlight;
var placehold;
var window;

// -- Core helpers

function CompleterFS() {
    return Q.Qt.createQmlObject("import hexed.Core; CompleterFS {}", window);
}

function Matcher() {
    return Q.Qt.createQmlObject("import hexed.Core; Matcher {}", window);
}

// -- File commands

function file_find() {
    
    let completer = new CompleterFS();
    
    let node = Layout.bt.curt;
    
    header.prefix = "Find";
    if (node.file && !node.file.startsWith("*"))
        header.text = node.file;
    else
        header.text = fs_helper.homePath();
    header.forceActiveFocus();
    
    header.completion_callback = (text) =>
    {
        return completer.complete(text);
    }
    
    header.completion_handle = (text, candidate, index) =>
    {
        return completer.apply(text, candidate);
    }
    
    let accepted;
    let rejected;
    let finalise = () => {
        
        if (completer) {
            completer.destroy();
            completer = null;
        }
        
        Layout.reveal();
        
        header.accepted.disconnect(accepted);
        header.accepted.disconnect(finalise);
        header.rejected.disconnect(rejected);
        header.rejected.disconnect(finalise);
        
        H.Hooks.run_hooks('file_find_hooks');
    }
    
    rejected = () =>
    {
        
    };
    
    accepted = () =>
    {
        H.Buffers.set(Layout.bt.curt, header.text, Layout.bt.curt);
    };
    
    header.accepted.connect(accepted);
    header.accepted.connect(finalise);
    header.rejected.connect(rejected);
    header.rejected.connect(finalise);
}

function file_save() {
    
    let node = Layout.bt.curt;
    
    if (fs_helper.exists(node.file)) {
        fs.write(node.file, node.item.contents);
        
        header.echo("Wrote file " + node.file, 2000);
    
    } else {
        
        file_save_as();
    }
}

function file_save_as() {
    
    let node = Layout.bt.curt;
    
    let completer = new CompleterFS();
    
    header.prefix = "Save as";
    if (node.file && !node.file.startsWith("*"))
        header.text = node.file;
    else
        header.text = fs_helper.homePath();
    header.forceActiveFocus();
    
    header.completion_callback = (text) =>
    {
        return completer.complete(text);
    }
    
    header.completion_handle = (text, candidate, index) =>
    {
        return completer.apply(text, candidate);
    }
    
    let accepted;
    let rejected;
    let finalise = () => {
        
        if (completer) {
            completer.destroy();
            completer = null;
        }
                
        Layout.reveal();
        
        header.accepted.disconnect(accepted);
        header.accepted.disconnect(finalise);
        header.rejected.disconnect(rejected);
        header.rejected.disconnect(finalise);
        
        H.Hooks.run_hooks('file_save_hooks');
    }
    
    rejected = () =>
    {
        
    };
    
    accepted = () =>
    {
        fs.write(header.text, node.item.contents);
        
        H.Buffers.set(Layout.bt.curt, header.text, Layout.bt.curt);
    };
    
    header.accepted.connect(accepted);
    header.accepted.connect(finalise);
    header.rejected.connect(rejected);
    header.rejected.connect(finalise);
}

function file_quit() {
    
    H.Hooks.run_hooks('file_quit_hooks');
    
    Q.Qt.quit();
}

// -- Buffers

function buffer_switch() {
    
    let matcher   = new Matcher();
    
    var candidates = [];
    var candidate_ = null;
    
    header.prefix = "Switch";
    header.text = '';
    header.forceActiveFocus();
    header.completion_callback = (text) =>
    {
        let candidates = []
        
        for (const [key, value] of H.Buffers.buffers.entries()) {
            
            if (key.startsWith("*")) {
                candidates.push(key);
            } else {
                candidates.push(fs_helper.fileName(key));
            }
        }
        
        let matches = matcher.filter(text, candidates);
        
        candidates = matches.results;
        
        return matches;
    }
    header.completion_handle = (text, candidate, index) =>
    {
        candidate_ = candidate;
        
        return candidate;
    }
    header.initiate();
    
    let accepted;
    let rejected;
    let finalise = () => {
        
        if (matcher) {
            matcher.destroy();
            matcher = null;
        }
        
        Layout.reveal();
        
        header.accepted.disconnect(accepted);
        header.accepted.disconnect(finalise);
        header.rejected.disconnect(rejected);
        header.rejected.disconnect(finalise);
        
        H.Hooks.run_hooks('buffer_switch_hooks');
    }
    
    rejected = () =>
    {
        
    };
    
    accepted = () =>
    {
        let file = null;
        
        let buffers = Array.from(H.Buffers.buffers.keys());
        
        for (var i = 0; i < buffers.length; i++) {
            
            if (buffers[i].endsWith(candidate_))
                file = buffers[i];
        }

        if (file)
            H.Buffers.buffer_switch(Layout.bt.curt, file);
    };
    
    header.accepted.connect(accepted);
    header.accepted.connect(finalise);
    header.rejected.connect(rejected);
    header.rejected.connect(finalise);
}

function buffer_list() {
    
    let accepted;
    let rejected;
    let finalise = () => {
                
        Layout.reveal();
        
        dialog_buffers.accepted.disconnect(accepted);
        dialog_buffers.accepted.disconnect(finalise);
        dialog_buffers.rejected.disconnect(rejected);
        dialog_buffers.rejected.disconnect(finalise);
        
        H.Hooks.run_hooks('buffer_list_hooks');
    }
    
    rejected = () =>
    {
        // console.log('COMMANDS.qml', 'buffer_list', 'REJECTED');
    };
    
    accepted = () =>
    {
        let candidate = Array.from(H.Buffers.buffers.keys())[dialog_buffers.candidate_index];
        if (candidate)
            H.Buffers.buffer_switch(Layout.bt.curt, candidate);
    };
    
    dialog_buffers.model.clear();
    
    for (const [key, value] of H.Buffers.buffers.entries()) {
        let buffer;
        let path;
        if (key.startsWith("*")) {
            buffer = key;
            path = '';
        } else {
            buffer = fs_helper.fileName(key);
            path = key;
        }
        dialog_buffers.model.append({
            "buffer": buffer,
            "path": path
        });
    }
    
    dialog_buffers.accepted.connect(accepted);
    dialog_buffers.accepted.connect(finalise);
    dialog_buffers.rejected.connect(rejected);
    dialog_buffers.rejected.connect(finalise);
    dialog_buffers.open();
}

// -- Commands

function interactive_command() {
    header.prefix = "M-x";
    header.forceActiveFocus();
    
    let accepted;
    let rejected;
    let finalise = () => {
        Layout.reveal();
        
        header.accepted.disconnect(accepted);
        header.accepted.disconnect(finalise);
        header.rejected.disconnect(rejected);
        header.rejected.disconnect(finalise);
    }
    
    rejected = () =>
    {
        
    };
    
    accepted = () =>
    {
        
    };
    
    header.accepted.connect(accepted);
    header.accepted.connect(finalise);
    header.rejected.connect(rejected);
    header.rejected.connect(finalise);
}

// -- Window commands

function window_remove() {
    Layout.bt.del();
    Layout.reveal();
    
    H.Hooks.run_hooks('window_remove_hooks');
}

function window_remove_others() {
    Layout.bt.del_others();
    Layout.reveal();
    
    H.Hooks.run_hooks('window_remove_hooks');
}

function window_split_vertical() {
    
    if (Layout.bt.curt.item.height / 2 < Layout.minimum_window_height) {
        header.echo("No room left to split vertically", 2000);
        return;
    }
    
    Layout.bt.add(++Layout.bt_id, Q.Qt.Vertical);
    Layout.reveal();
    
    H.Hooks.run_hooks('window_split_hooks');
}

function window_split_horizontal() {
    
    if (Layout.bt.curt.item.width / 2 < Layout.minimum_window_width) {
        header.echo("No room left to split horizontally", 2000);
        return;
    }
    
    Layout.bt.add(++Layout.bt_id, Q.Qt.Horizontal);
    Layout.reveal();
    
    H.Hooks.run_hooks('window_split_hooks');
}

function window_move_focus_right() {
    var x = placehold.mapFromItem(Layout.bt.curt.item, 0, 0).x;
    var y = placehold.mapFromItem(Layout.bt.curt.item, 0, 0).y;
    
    var target = null;
    var target_dx = window.width;
    var target_dy = window.height;
    
    var heuristic = function(node) {
    
        if(!node)                        return;
        if(!node.value)                  return;
        if( node.value == Layout.bt.curt.value)
            return;
    
        var n_x = placehold.mapFromItem(node.item, 0, 0).x;
        var n_y = placehold.mapFromItem(node.item, 0, 0).y;
    
        var dx = x - n_x;
        var dy = y - n_y;
    
        if(dx == 0) return;
        if(dx  > 0) return;
    
        dx = Math.abs(dx);
        dy = Math.abs(dy);
    
        if(dx <= target_dx && dy <= target_dy) {
            target = node;
            target_dx = dx;
            target_dy = dy;
        }
     }
    
    Layout.bt.depthFirstPostOrder(heuristic);
    Layout.focus(target);
}

function window_move_focus_left() {
    var x = placehold.mapFromItem(Layout.bt.curt.item, 0, 0).x;
    var y = placehold.mapFromItem(Layout.bt.curt.item, 0, 0).y;
    
    var target = null;
    var target_dx = window.width;
    var target_dy = window.height;
    
    var heuristic = function(node) {
    
        if(!node)                        return;
        if(!node.value)                  return;
        if( node.value == Layout.bt.curt.value)
            return;
    
        var n_x = placehold.mapFromItem(node.item, 0, 0).x;
        var n_y = placehold.mapFromItem(node.item, 0, 0).y;
    
        var dx = x - n_x;
        var dy = y - n_y;
    
        if(dx == 0) return;
        if(dx  < 0) return;
    
        dx = Math.abs(dx);
        dy = Math.abs(dy);
    
        if(dx <= target_dx && dy <= target_dy) {
            
            target = node;
            target_dx = dx;
            target_dy = dy;
        }
     }
    
    Layout.bt.depthFirstPostOrder(heuristic);
    Layout.focus(target);
}

function window_move_focus_up() {
    var x = placehold.mapFromItem(Layout.bt.curt.item, 0, 0).x;
    var y = placehold.mapFromItem(Layout.bt.curt.item, 0, 0).y;
    
    var target = null;
    var target_dx = window.width;
    var target_dy = window.height;
    
    var heuristic = function(node) {
    
        if(!node)                        return;
        if(!node.value)                  return;
        if( node.value == Layout.bt.curt.value)
            return;
    
        var n_x = placehold.mapFromItem(node.item, 0, 0).x;
        var n_y = placehold.mapFromItem(node.item, 0, 0).y;
    
        var dx = x - n_x;
        var dy = y - n_y;
    
        if(dy == 0) return;
        if(dy  < 0) return;
    
        dx = Math.abs(dx);
        dy = Math.abs(dy);
    
        if(dy <= target_dy && dx <= target_dx) {
            target = node;
            target_dx = dx;
            target_dy = dy;
        }
     }
    
    Layout.bt.depthFirstPostOrder(heuristic);
    Layout.focus(target);
}

function window_move_focus_down() {
    var x = placehold.mapFromItem(Layout.bt.curt.item, 0, 0).x;
    var y = placehold.mapFromItem(Layout.bt.curt.item, 0, 0).y;
    
    var target = null;
    var target_dx = window.width;
    var target_dy = window.height;
    
    var heuristic = function(node) {
    
        if(!node)                        return;
        if(!node.value)                  return;
        if( node.value == Layout.bt.curt.value)
            return;
    
        var n_x = placehold.mapFromItem(node.item, 0, 0).x;
        var n_y = placehold.mapFromItem(node.item, 0, 0).y;
    
        var dx = x - n_x;
        var dy = y - n_y;
    
        if(dy == 0) return;
        if(dy  > 0) return;
    
        dx = Math.abs(dx);
        dy = Math.abs(dy);
    
        if(dy <= target_dy && dx <= target_dx) {
            target = node;
            target_dx = dx;
            target_dy = dy;
        }
     }
    
    Layout.bt.depthFirstPostOrder(heuristic);
    Layout.focus(target);
}

// -- Edit

function edit_buffer_scroll_up() {
    
    Layout.bt.curt.item.scroll_up();
}
