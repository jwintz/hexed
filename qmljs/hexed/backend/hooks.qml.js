.pragma library

// --

.import QtQuick          as Q
.import QtQuick.Controls as Q

// --

.import "buffers.qml.js" as Buffers

// --

var hooks = {
    'file_find_hooks': [],
    'file_save_hooks': [],
    'file_quit_hooks': [],
    
    'buffer_switch_hooks': [],
    'buffer_list_hooks': [],
    
    'window_remove_hooks': [],
    'window_split_hooks': [],
    'window_focus_hooks': [],
}

// ---

function add_hook(hook_var, hook_lambda)
{
    // console.log('Adding hook to', hook_var);
    
    hooks[hook_var].push(hook_lambda);
}

function run_hooks(hook_var)
{
    // console.log('Running hooks for', hook_var); // TODO: this goes into the journal
    
    hooks[hook_var].forEach(hook => {
        hook()
    });
}
