.pragma library

// --

.import QtQuick          as Q
.import QtQuick.Controls as Q

.import hexed.Core       as H

// --

var buffers = new Map;
var fs;
var window;

// --- API

function make(node, file) {
    
    var buffer = {};
    
    buffer.file = file;
    buffer.node = node;
    buffer.contents = "";
    buffer.line = 1; // Only used in buffer_switch function so far
    // buffer.items = [];
    
    if (file == '*playground*') {
        node.line = 9;
        
        buffer.contents = `// buffer1 | ˈbʌfə |
// noun
// a temporary area in which code is edited before it is being saved or evaluated.

// playground | ˈpleɪɡraʊnd |
// noun
// an area provided to play in, especially in a code editor.

`;
    }
    
    return buffer;
}

function set(node, file, from) {
    
    // console.log('BUFFERS.qml.js: Setting', file, 'to', node.value);
    
    let buffer = null;
    
    if (buffers.has(file)) {
        buffer = buffers.get(file);
    } else {
        buffer = make(node, file);
        buffers.set(file, buffer);
    }
    
    node.file = file;
    
    node.item.node = node;
    node.item.buffer = buffer;
    if(!file.startsWith('*'))
        node.item.contents = fs.read(file);
    else
        node.item.contents = buffer.contents;
    node.item.setCurrentLine(from.line);
    
    // console.log([...buffers.entries()]);
}

function buffer_switch(node, file) {
    
    if (!buffers.has(file)) {
        return;
    }
    
    if (node.file == file) {
        return;
    }
    
    let buffer = buffers.get(file);
    
    node.file = file;
    
    node.item.node = node;
    node.item.buffer = buffer;
    node.item.contents = buffer.contents;
    node.item.setCurrentLine(buffer.line);
    
    // console.log([...buffers.entries()]);
}
