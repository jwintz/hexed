import QtQuick
import QtQuick.Controls

import Qt.labs.platform

import hexed.Frontend as H

MenuBar {
    
    property var root;
    property var buffer;
    
    Menu {
       
        title: "File"
       
        MenuItem {
            text: "Find";
            shortcut: "Meta+X, Meta+F";
            onTriggered: H.Commands.file_find();
        }
        
        MenuItem {
            text: "Save";
            shortcut: "Meta+X, Meta+S";
            onTriggered: H.Commands.file_save();
        }
        
        MenuItem {
            text: "Save as";
            shortcut: "Meta+X, Meta+W";
            onTriggered: H.Commands.file_save_as();
        }
        
        MenuSeparator {}
    
        MenuItem {
            text: "Quit";
            shortcut: "Meta+X, Meta+C";
            onTriggered: H.Commands.file_quit();
        }
    }

    Menu {
       
        title: "Edit"
        
        MenuItem {
            text: "Scroll up";
            shortcut: "Meta+V";
            onTriggered: H.Commands.edit_scroll_up();
        }
    }
    
    Menu {
        
        title: "Buffers"
        
        MenuItem {
            text: "Switch"
            shortcut: "Meta+X, B"
            onTriggered: H.Commands.buffer_switch();
        }
        
        MenuItem {
            text: "List"
            shortcut: "Meta+X, Meta+B"
            onTriggered: H.Commands.buffer_list();
        }
    }
    
    Menu {
        
        title: "Layout"
        
        MenuItem {
            text: "Remove window"
            shortcut: "Meta+X, 0"
            onTriggered: H.Commands.window_remove();
        }
        
        MenuItem {
            text: "Remove others"
            shortcut: "Meta+X, 1"
            onTriggered: H.Commands.window_remove_others();
        }
        
        MenuSeparator {}
        
        MenuItem {
            text: "Split Vertically"
            shortcut: "Meta+X, 2"
            onTriggered: H.Commands.window_split_vertical();
        }
        
        MenuItem {
            text: "Split Horizontally"
            shortcut: "Meta+X, 3"
            onTriggered: H.Commands.window_split_horizontal();
        }
        
        MenuSeparator {}
        
        MenuItem {
            text: "Reveal focus"
            shortcut: "Alt+."
            onTriggered: H.Layout.reveal();
        }
        
        MenuItem {
            text: "Move focus to window right"
            shortcut: "Alt+Right"
            onTriggered: H.Commands.window_move_focus_right();
        }
        
        MenuItem {
            text: "Move focus to window left"
            shortcut: "Alt+Left"
            onTriggered: H.Commands.window_move_focus_left();
        }
        
        MenuItem {
            text: "Move focus to window up"
            shortcut: "Alt+Up"
            onTriggered: H.Commands.window_move_focus_up();
        }
        
        MenuItem {
            text: "Move focus to window down"
            shortcut: "Alt+Down"
            onTriggered: H.Commands.window_move_focus_down();
        }
    }
    
    Menu {
       
        title: "Commands"
       
        MenuItem {
            text: "Enter interactive command";
            shortcut: "Escape, x";
            onTriggered: H.Commands.interactive_command();
        }
    }
    
    Menu {
        
        title: "Window"
        
        MenuItem {
            text: root.drawel_expanded ? "Hide left drawer" : "Show left drawer";
            shortcut: "Ctrl+0";
            onTriggered: root.drawel_expanded = !root.drawel_expanded;
        }
        
        MenuItem {
            text: root.drawer_expanded ? "Hide right drawer" : "Show right drawer";
            shortcut: "Ctrl+Alt+0";
            onTriggered: root.drawer_expanded = !root.drawer_expanded;
        }
        
        MenuSeparator {}
        
        MenuItem {
            text: "Show project navigator";
            shortcut: "Ctrl+1";
            onTriggered: {
                root.drawel_index = 0;
                root.drawel_expanded = true;
            }
        }
        
        MenuItem {
            text: "Show version navigator";
            shortcut: "Ctrl+2";
            onTriggered: {
                root.drawel_index = 1;
                root.drawel_expanded = true;
            }
        }
        
        MenuItem {
            text: "Show search navigator";
            shortcut: "Ctrl+3";
            onTriggered: {
                root.drawel_index = 2;
                root.drawel_expanded = true;
            }
        }
        
        MenuItem {
            text: "Show journal navigator";
            shortcut: "Ctrl+4";
            onTriggered: {
                root.drawel_index = 3;
                root.drawel_expanded = true;
            }
        }
        
        MenuSeparator {}
        
        MenuItem {
            text: "Show symbol navigator";
            shortcut: "Ctrl+Alt+1";
            onTriggered: {
                root.drawer_index = 0;
                root.drawer_expanded = true;
            }
        }
        
        MenuItem {
            text: "Show diagnostics navigator";
            shortcut: "Ctrl+Alt+2";
            onTriggered: {
                root.drawer_index = 1;
                root.drawer_expanded = true;
            }
        }
    }
   
    Menu {
        
        title: "Help"
        
        MenuItem {
            text: "Hexed documentation";
            onTriggered: Qt.openUrlExternally("http://dream.inria.fr/j");
        }
    }
    
    // ---
    
    function attempt(event, buffer) {
        
        let shortcut = "";
        
        if (event.modifiers & Qt.AltModifier)
            shortcut = shortcut + "Alt";
        
        if(shortcut !== "") {
            
            shortcut = shortcut + "+";
            
            if (event.key == Qt.Key_Left)
                shortcut = shortcut + "Left";

            if (event.key == Qt.Key_Right)
                shortcut = shortcut + "Right";
            
            // console.log("---------------------");
            // console.log("Considering", shortcut, event.key);
            
            for(var group in menus) {
                
                // console.log("Iterating over", menus[group].title);
                
                for(var command in menus[group].items) {
                    
                    // console.log("Iterating over", menus[group].items[command].text);
                    
                    if (menus[group].items[command].shortcut == shortcut) {
                        menus[group].items[command].onTriggered();
                        
                        event.accepted = false;
                        
                        return true;
                    }
                }
            }
        }
        
        return false;
    }
}
