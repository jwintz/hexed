#include <QtDebug>
#include <QtCore>
#include <QtQuick>

#include <hexed_core/buffer.h>
#include <hexed_core/completer.h>
#include <hexed_core/completer_fs.h>
#include <hexed_core/fs.h>
#include <hexed_core/fs_helper.h>
#include <hexed_core/fs_model.h>
#include <hexed_core/journal.h>
#include <hexed_core/journal_item.h>
#include <hexed_core/matcher.h>

#include <hexed_lsp/lsp_client.h>
#include <hexed_lsp/lsp_helper.h>

// #include <hexed_sitter/sitter.h>

#include <hexed_term/term_keyloader.h>
#include <hexed_term/term_textrender.h>
#include <hexed_term/term_utilities.h>

#include "main+window.h"

void hxCopyFileFromResources(QString from, QString to)
{
    QFileInfo toFile(to);
    if (!toFile.exists()) {
        QFile newToFile(toFile.absoluteFilePath());
        QResource res(from);
        if (newToFile.open(QIODevice::WriteOnly)) {
            newToFile.write(reinterpret_cast<const char*>(res.data()));
            newToFile.close();
        } else {
            qWarning() << "Failed to copy default config from resources to" << toFile.filePath();
        }
    }
}

void hxJournalHandler(QtMsgType type, const QMessageLogContext &context, const QString &message)
{
    QString log = QString("%1 (%2:%3, %4) %5")
        .arg(message)
        .arg(context.file)
        .arg(context.line)
        .arg(context.function)
        .arg(context.category);
    
    if(QString(context.category) != "hexed") {
    QByteArray localMsg   = message.toLocal8Bit();
    const char *file      = context.file     ? context.file     : "";
    const char *function  = context.function ? context.function : "";
    const char *timestamp = QDateTime::currentDateTime().toString().toLocal8Bit();
    switch (type) {
    case    QtDebugMsg: fprintf(stderr, "hexed - [%s] -    Debug: %s (%s:%u, %s)\n", timestamp, localMsg.constData(), file, context.line, function); break;
    case     QtInfoMsg: fprintf(stderr, "hexed - [%s] -     Info: %s (%s:%u, %s)\n", timestamp, localMsg.constData(), file, context.line, function); break;
    case  QtWarningMsg: fprintf(stderr, "hexed - [%s] -  Warning: %s (%s:%u, %s)\n", timestamp, localMsg.constData(), file, context.line, function); break;
    case QtCriticalMsg: fprintf(stderr, "hexed - [%s] - Critical: %s (%s:%u, %s)\n", timestamp, localMsg.constData(), file, context.line, function); break;
    case    QtFatalMsg: fprintf(stderr, "hexed - [%s] -    Fatal: %s (%s:%u, %s)\n", timestamp, localMsg.constData(), file, context.line, function); break;
    };
    return;
    }

    switch (type) {
    case    QtDebugMsg: hxJournal::instance()->addDebugMsg(log);   break;
    case     QtInfoMsg: hxJournal::instance()->addInfoMsg(log);    break;
    case  QtWarningMsg: hxJournal::instance()->addWarningMsg(log); break;
    case QtCriticalMsg: hxJournal::instance()->addErrorMsg(log);   break;
    case    QtFatalMsg: hxJournal::instance()->addErrorMsg(log);   break;
    }
}

int main(int argc, char **argv)
{
    qputenv("QT_QUICK_CONTROLS_HOVER_ENABLED", "1");
    
    QCoreApplication::setApplicationName("hexed");
    QCoreApplication::setOrganizationName("jwintz");
    QCoreApplication::setOrganizationDomain("me");
    QCoreApplication::setAttribute(Qt::AA_ShareOpenGLContexts);
        
    qmlRegisterType<hxBuffer>      ("hexed.Core", 1, 0, "Buffer");
    qmlRegisterType<hxCompleterFS> ("hexed.Core", 1, 0, "CompleterFS");
    qmlRegisterType<hxFS>          ("hexed.Core", 1, 0, "FS");
    qmlRegisterType<hxFSHelper>    ("hexed.Core", 1, 0, "FSHelper");
    qmlRegisterType<hxFSModel>     ("hexed.Core", 1, 0, "FSModel");
    qmlRegisterType<hxMatcher>     ("hexed.Core", 1, 0, "Matcher");

    qmlRegisterType<hxLSPHelper>   ("hexed.LSP",  1, 0, "Helper");
    
    qmlRegisterAnonymousType<hxJournalItem>("hexed.Core", 1);

    qmlRegisterSingletonInstance("hexed.Core", 1, 0, "Journal", hxJournal::instance());
    
    qInstallMessageHandler(hxJournalHandler);

// ////////////////////////////////////////////////////////////////////////////
//
// ////////////////////////////////////////////////////////////////////////////

    qmlRegisterType<hxTermRender>  ("hexed.Term", 1, 0, "TermRender");
    
    qmlRegisterUncreatableType<hxTermUtil>("hexed.Term", 1, 0, "TermUtil", "");
    
    hxTermUtil term_util(QDir::homePath() + "/.hexed/term/settings.ini");
    term_util.setKeyboardLayout("english");
    
    qmlRegisterSingletonInstance("hexed.Term", 1, 0, "TermUtil", &term_util);

    // hxCopyFileFromResources(":/data/menu.xml",       term_util.configPath() + "/menu.xml");
    // hxCopyFileFromResources(":/data/english.layout", term_util.configPath() + "/english.layout");
    // hxCopyFileFromResources(":/data/finnish.layout", term_util.configPath() + "/finnish.layout");
    // hxCopyFileFromResources(":/data/french.layout",  term_util.configPath() + "/french.layout");
    // hxCopyFileFromResources(":/data/german.layout",  term_util.configPath() + "/german.layout");
    // hxCopyFileFromResources(":/data/qwertz.layout",  term_util.configPath() + "/qwertz.layout");

    hxKeyLoader term_loader;
    term_loader.setUtil(&term_util);
    term_loader.loadLayout(":/data/english.layout");
    
    qmlRegisterSingletonInstance("hexed.Term", 1, 0, "TermKeyLoader", &term_loader);
    
    // ////////////////////////////////////////////////////////////////////////////

    QGuiApplication application(argc, argv);
    
    QQmlApplicationEngine engine;
    engine.addImportPath("qrc:///qml");
    engine.addImportPath("qrc:///qmljs");
    engine.addImportPath("qrc:///app");
    engine.load(QUrl("qrc:///app/hexed/main.qml"));
    
#if !defined(Q_OS_IOS)
    hxWindowHelperWrap(&engine);
#endif

// ////////////////////////////////////////////////////////////////////////////
// Tree Sitter test
// ////////////////////////////////////////////////////////////////////////////

//  hxSitterTest("unused for now");

// ////////////////////////////////////////////////////////////////////////////
    
    return application.exec();
}
