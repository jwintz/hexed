import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

import hexed.Controls as H

Page {
    id: self
    
    property alias current_index: contents.currentIndex;

    clip: true;
    
    header: ToolBar {
        height: 40;
      
        leftPadding: 0;
        rightPadding: 0;
        
        Rectangle {
            height: 1;
            color: "#66666266";
            anchors.left: parent.left;
            anchors.right: parent.right;
            anchors.bottom: parent.bottom;
        }
        
        background: Rectangle {
            color: "#00000000";
        }
    }
    
    ColumnLayout {

        anchors.fill: parent;

        spacing: 0;

        RowLayout {
            
            Layout.leftMargin: 8;
            Layout.rightMargin: 8;
            Layout.fillWidth: true;
            Layout.minimumHeight: 28 - 2;
            Layout.maximumHeight: 28 - 2;
            Layout.preferredHeight: 28 - 2;
            
            spacing: 0;
            
            Item { Layout.fillHeight: true; Layout.preferredWidth: (self.width - 2 * 4) / 2; Text { color: current_index == 0 ? "#cab2fa" : "#cccccc" ; font.family: "Material Design Icons"; font.pointSize: 16; text: '󱦜'; anchors.centerIn: parent; } MouseArea { anchors.fill: parent; hoverEnabled: true; onClicked: current_index = 0; } } // Symbol navigator
            Item { Layout.fillHeight: true; Layout.preferredWidth: (self.width - 2 * 4) / 2; Text { color: current_index == 1 ? "#cab2fa" : "#cccccc" ; font.family: "Material Design Icons"; font.pointSize: 16; text: '󰗖'; anchors.centerIn: parent; } MouseArea { anchors.fill: parent; hoverEnabled: true; onClicked: current_index = 1; } } // Diagnostics navigator
        }
        
        Rectangle {
            height: 1;
            color: "#66666266";
            Layout.fillWidth: true;
        }
        
        StackLayout {
            
            id: contents
            
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.margins: 0
    
            Item {}
            H.NavigatorDiagnostics { id: navigator_diagnostics }
        }
    }
    
    Text {
        anchors.centerIn: parent;
        color: "#aacccccc";
        font.pointSize: 22
        text: current_index == 0 ? "Symbol navigator"
        : current_index == 1 ? "Diagnostics navigator"
        : "";
        visible: true // current_index != 0
    }
    
    background: Rectangle {
        color: "#00000000";
    }
}

