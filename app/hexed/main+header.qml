import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

import hexed.Frontend as H

FocusScope {
    id: self;

    // --
    
    property alias prefix: prefix_text.text;
    property alias text: input.text;
    
    property var completion_callback: null;
    property var completion_handle: null;
    property var completion_result: null;
    
    property bool echoing: false;
    
    // --
    
    function echo(message, duration) {
        
        self.text = message;
        self.echoing = true;
        
        var timer = Qt.createQmlObject("import QtQuick; Timer {}", self);
        timer.interval = duration;
        timer.repeat = false;
        timer.triggered.connect(function () { self.text = ''; self.echoing = false; });
        timer.triggered.connect(function () { timer.destroy(); });
        timer.start();
    }
    
    function initiate() {
        
        self.completion_result = self.completion_callback(self.text);
        
        candidates.model = self.completion_result.results;
        
        if (candidates.model.length) {
            if(!popup.opened)
                popup.open();
        } else {
            if (popup.opened)
                popup.close();
        }
    }
    
    function reset() {
        prefix = '';
          text = '';
        
        completion_callback = null;
        completion_handle   = null;
        completion_result   = null;
        
        candidates.model = [];
    }
    
    // --
    
    signal accepted();
    signal rejected();
    
    // --
    
    onAccepted: popup.close();
    onRejected: popup.close();
    
    // --
    
    Rectangle {
        
        anchors.fill: parent;
        
        color: "#27222D";
    } // background
    
    RowLayout {
        
        id: layout;
        
        anchors.fill: parent;
        anchors.leftMargin: window.drawel_width > 78 ? 0 : 78 - window.drawel_width;
        
        Item {
            
            Layout.maximumWidth: 52;
            Layout.minimumWidth: 52;
            
            Layout.leftMargin: 8;
            
            visible: self.prefix !== '';
            
            height: self.height - 2*8;
            
            Rectangle {
                anchors.top: parent.top;
                anchors.right: parent.right;
                anchors.bottom: parent.bottom;
                width: 4;
                color: "#4E454D";
            }
            
            Rectangle {
                color: "#ae91e8";
                radius: 4;
                anchors.fill: parent;
            }
            
            Rectangle {
                anchors.top: parent.top;
                anchors.right: parent.right;
                height: 4;
                width: 4;
                color: "#ae91e8";
            }

            Rectangle {
                anchors.bottom: parent.bottom;
                anchors.right: parent.right;
                height: 4;
                width: 4;
                color: "#ae91e8";
                
                visible: !popup.opened;
            }
            
            Rectangle {
                anchors.left: parent.left;
                anchors.bottom: parent.bottom;
                height: 4;
                width: 4;
                color: "#ae91e8";
                visible: popup.visible;
            }
            
            Text {
                id: prefix_text
                
                anchors.centerIn: parent
                
                text: ""
                
                font.family: "Roboto Mono"
                font.pointSize: 12
                font.weight: Font.Light
                
                color: "#ffffff"
            }
        }
        
        Rectangle {
            
            id: bar;
            
            Layout.fillWidth: true;
            Layout.rightMargin: 8;
            Layout.leftMargin: self.prefix !== '' ? -layout.spacing : 8;
            
            height: self.height - 2*8;
            radius: 4;
            color: "#4E454D";
            
            Rectangle {
                anchors.top: parent.top;
                anchors.left: parent.left;
                anchors.bottom: parent.bottom;
                width: 4;
                color: "#4E454D";
                visible: self.prefix != '';
            }
            
            Rectangle {
                anchors.right: parent.right;
                anchors.bottom: parent.bottom;
                height: 4;
                width: 4;
                color: "#4E454D";
                visible: popup.visible;
            }
            
            TextInput {
                
                id: input;
                
                anchors.verticalCenter: parent.verticalCenter;
                anchors.left: parent.left;
                anchors.leftMargin: 10;
                
                 clip: true;
                focus: true;
                
                enabled: self.prefix != '';
                
                width: parent.width - 2*10;
                
                font.family: "Roboto Mono";
                font.pointSize: 12;
                font.weight: Font.Light;
                
                selectionColor: "#6A598B";
                color: "#ffffff";
                
                Rectangle {
                    id: cursor;
                    
                    radius: 1;
                    width: 2;
                    color: "#AE91E8";
                    
                    visible: input.activeFocus;
                    
                    x: 4;
                    y: 0;
                    
                    height: 0;
                    
                    Behavior on x { NumberAnimation { duration: 75 } }
                    // Behavior on y { NumberAnimation { duration: 50 } }
                    
                    SequentialAnimation on opacity { running: true; loops: Animation.Infinite;
                        NumberAnimation { to: 0.5; duration: 500; easing.type: "OutQuad"}
                        NumberAnimation { to: 1.0; duration: 500; easing.type: "InQuad"}
                    }
                }
                
                cursorDelegate: Item {
                    id: cursor_delegate;
                    
                    onXChanged: cursor.x = x;
                    onYChanged: cursor.y = y;
                    onHeightChanged: cursor.height = height;
                }
                
                onTextEdited: {
                    if(!self.completion_callback)
                        return;
                    
                    self.completion_result = self.completion_callback(self.text);
                    
                    candidates.model = self.completion_result.results;
                    
                    if (candidates.model.length) {
                        if(!popup.opened)
                            popup.open();
                    } else {
                        if (popup.opened)
                            popup.close();
                    }
                }
                
                Keys.onDownPressed: {
                    if (candidates.currentIndex <  candidates.count - 1)
                        candidates.currentIndex = (candidates.currentIndex + 1);
                }
                
                Keys.onUpPressed: {
                    if (candidates.currentIndex > 0)
                        candidates.currentIndex = (candidates.currentIndex - 1);
                }
                
                Keys.onTabPressed: {
                    if(!self.completion_handle)
                        return;
                        
                    if(!popup.opened) {
                        self.initiate();
                        return;
                    }
                    
                    text = self.completion_handle(text, candidates.model[candidates.currentIndex], candidates.currentIndex);
                    
                    self.completion_result = self.completion_callback(self.text);
                    
                    candidates.model = self.completion_result.results;
                }
                
                Keys.onReturnPressed: {
                    if (self.completion_handle && candidates.count)
                        text = self.completion_handle(text, candidates.model[candidates.currentIndex], candidates.currentIndex);
                    
                    self.accepted();
                    self.reset();
                }
                
                Keys.onPressed: (event) => {
                    
                    if (event.modifiers & Qt.MetaModifier && event.key == Qt.Key_G) {
                        event.accepted = true;
                        self.rejected();
                        self.reset();
                    }
                }
            }
            
            Text {
                anchors.right: parent.right;
                anchors.rightMargin: 8;
                anchors.verticalCenter: parent.verticalCenter;
                
                color: "#ffffff"
                visible: self.echoing
                font.family: "Material Design Icons"
                font.pointSize: 14
                text: '󰵅'
            }
        }
    }
    
    Rectangle {
        height: 1;
        color: "#66666266";
        anchors.left: parent.left;
        anchors.right: parent.right;
        anchors.bottom: parent.bottom;
    }
    
    // onActiveFocusChanged: {
    //
    //     if (self.activeFocus) {
    //         // run window enter hooks!!! :-)
    //     }
    // }
    
    Popup {
        id: popup;
        
        x: layout.x + 8;
        y: layout.y + layout.height - 8
        z: Infinity
        
        clip: true;

        modal: false;
        focus: false;
        
        width: layout.width - 2*8;
        height: Math.min(candidates.count * 32 +  2 * 6, window.height * 1/2);
        
        Behavior on height { NumberAnimation { id: popup_animation; duration: 125; } }
        
        ListView {
            
            id: candidates;
            
            anchors.fill: parent;
            
            model: [];
            
            delegate: Item {
                
                width: ListView.view.width;
                height: 32;
                
                Rectangle {
            
                    anchors.fill: parent;
                    anchors.leftMargin: 2;
                    anchors.rightMargin: 2;
            
                    color: "#00000000";
            
                    Text {
                        color: "white";
                        text: {
                            let s = modelData;
                            
                            if (self.completion_result
                             && self.completion_result.starts.length
                             && self.completion_result.starts.length > index
                             && self.completion_result.starts[index].length) {
                                let beg = self.completion_result.starts[index];
                                let end = self.completion_result.starts[index][0]
                                + self.completion_result.lengths[index][0]
                                + 25;
                                let b = "<b><font color='#C9B2FA'>";
                                let e = "</font></b>";
                                
                                s = [s.slice(0, beg), b, s.slice(beg)].join('');
                                s = [s.slice(0, end), e, s.slice(end)].join('');
                            }
                            
                            return s;
                        }
                        
                        anchors.verticalCenter: parent.verticalCenter;
                        anchors.left: parent.left;
                        anchors.leftMargin: 9;
                    }
                }
            }
            
            highlight: Rectangle {
                   
                width: candidates.width;
                height: 32;
                   
                y: candidates.currentItem ? candidates.currentItem.y : 0;
                   
                color: "#44000000";
                   
                radius: 4;
                   
                Behavior on y { NumberAnimation { duration: 100 } }
            }
            
            ScrollIndicator.vertical: ScrollIndicator {
                visible: !popup_animation.running && candidates.contentHeight > candidates.height
                
                contentItem: Rectangle {
                    implicitWidth: 4
                    radius: 2;
                    color: "#aaaaaa"
                }
            }
        }
        
        background: Rectangle {
            
            color: "#4E454D";
            radius: 4;
            
            Rectangle {
                anchors.top: parent.top;
                anchors.left: parent.left;
                anchors.right: parent.right;
                height: 4;
                color: "#4E454D";
            }
        }
    }
}
