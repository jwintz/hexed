import QtQuick
import QtQuick.Controls

import Qt.labs.platform
import Qt.labs.settings

import hexed          as H
import hexed.Backend  as H
import hexed.Controls as H
import hexed.Core     as H
import hexed.Frontend as H
import hexed.Term     as H

ApplicationWindow {

    id: window;
    
    width: 1000
    height: 800
    
    visible: true
    
    property var drawel_expanded: true;
    property var drawer_expanded: true;
    
    property alias drawel_index: drawel.current_index;
    property alias drawer_index: drawer.current_index;
    
    property alias drawel_width: drawel.width;

    SplitView {
     
        id: splitView;
     
        anchors.fill: parent;
     
        H.Drawel {
            id: drawel;
            
            SplitView.maximumWidth: 250;
            SplitView.minimumWidth: drawel_expanded && !drawel_animation.running ? 250 : 0;
            SplitView.preferredWidth: drawel_expanded ? 250 : 0;
            SplitView.fillHeight: true;
            
            Behavior on SplitView.preferredWidth { NumberAnimation { id: drawel_animation; easing.type: Easing.OutQuad } }
        }

        Item {
            
            SplitView.fillWidth: true;
            SplitView.fillHeight: true;
            
            H.Header {
                id: header;
                
                anchors.top: parent.top;
                anchors.left: parent.left;
                anchors.right: parent.right;
                
                height: 40-1;
                
                Behavior on height { NumberAnimation { duration: 100; easing.type: Easing.OutQuad; } }
            }
        
            SplitView {
                
                id: innerSplitView;
                
                orientation: Qt.Vertical
                
                anchors.top: header.bottom;
                anchors.left: parent.left;
                anchors.right: parent.right;
                anchors.bottom: parent.bottom;
                    
                Item {
                    
                    id: placehold;

                    SplitView.fillWidth: true;
                    SplitView.fillHeight: true;
                    
                    objectName: "placehold";
                    
                    H.Hglght {
                        id: highlight;
                    }
                }
        
                H.Term {
                    SplitView.fillWidth: true;
                    SplitView.preferredHeight: 100;
                }
            }
        }
        
        H.Drawer {
            id: drawer;
            
            SplitView.preferredWidth: drawer_expanded ? 250 : 0;
            SplitView.fillHeight: true;
        
            Behavior on SplitView.preferredWidth { NumberAnimation { easing.type: Easing.OutQuad } }
        }
        
        handle: Rectangle {
            
            id: splitView_handle
            
            implicitWidth: 1
            implicitHeight: 1
            color: (splitView_handle == splitView.children[1] && drawel.width < 10) ? "#00000000" :
                   (splitView_handle == splitView.children[2] && drawer.width < 10) ? "#00000000" :"#66666266";

            enabled: !(splitView_handle == splitView.children[1]);
                   
            // visible: (splitView_handle == splitView.children[1] && drawel.width < 10) ? false :
            //          (splitView_handle == splitView.children[2] && drawer.width < 10) ? false : false;
            
            containmentMask: Item {
                x: -4
                width: 8
                height: splitView.height
            }
        }
    }
    
    background: Rectangle {
        color: "#00000000";
    }

    H.Cmmnds { id: commands;  root: window; }
    H.Compnt { id: component; }
    H.Contnr { id: container; }
    
    H.DialogBuffers { id: dialog_buffers; }

    H.Settgs   { window: window }
    H.FS       { id: fs         }
    H.FSHelper { id: fs_helper  }
    
    Component.onCompleted: {
        
        H.Buffers.window = window;
        H.Buffers.fs     = fs;
        
        H.Commands.dialog_buffers = dialog_buffers;
        H.Commands.fs             = fs;
        H.Commands.fs_helper      = fs_helper;
        H.Commands.header         = header;
        H.Commands.highlight      = highlight;
        H.Commands.placehold      = placehold;
        H.Commands.window         = window;
 
        H.Layout.component = component;
        H.Layout.container = container;
        H.Layout.placehold = placehold;
        H.Layout.highlight = highlight;
 
        H.Layout.bt = new H.Layout.binary_tree();
        H.Layout.bt
          .add(1, Qt.Horizontal)
          .set(1);
        
        H.Layout.bt.curt.item.forceActiveFocus();
        
        console.log(journal_category, 'log');
        console.debug(journal_category, 'debug');
        console.info(journal_category, 'info');
        console.warn(journal_category, 'warn');
        console.error(journal_category, 'error');
    }
    
    FontLoader { source: "qrc:///ttf/MaterialDesignIcons.ttf" }
    FontLoader { source: "qrc:///ttf/CartographCF-Bold.ttf" }
    FontLoader { source: "qrc:///ttf/CartographCF-BoldItalic.ttf" }
    FontLoader { source: "qrc:///ttf/CartographCF-DemiBold.ttf" }
    FontLoader { source: "qrc:///ttf/CartographCF-DemiBoldItalic.ttf" }
    FontLoader { source: "qrc:///ttf/CartographCF-ExtraBold.ttf" }
    FontLoader { source: "qrc:///ttf/CartographCF-ExtraBoldItalic.ttf" }
    FontLoader { source: "qrc:///ttf/CartographCF-ExtraLight.ttf" }
    FontLoader { source: "qrc:///ttf/CartographCF-ExtraLightItalic.ttf" }
    FontLoader { source: "qrc:///ttf/CartographCF-Heavy.ttf" }
    FontLoader { source: "qrc:///ttf/CartographCF-HeavyItalic.ttf" }
    FontLoader { source: "qrc:///ttf/CartographCF-Light.ttf"; id: cartograph }
    FontLoader { source: "qrc:///ttf/CartographCF-LightItalic.ttf" }
    FontLoader { source: "qrc:///ttf/CartographCF-Regular.ttf" }
    FontLoader { source: "qrc:///ttf/CartographCF-RegularItalic.ttf" }
    FontLoader { source: "qrc:///ttf/CartographCF-Thin.ttf" }
    FontLoader { source: "qrc:///ttf/CartographCF-ThinItalic.ttf" }
    FontLoader { source: "qrc:///ttf/Firicico.ttf" }
    FontLoader { source: "qrc:///ttf/Firicico Italic.ttf" }
    FontLoader { source: "qrc:///ttf/JetBrains Mono Bold Italic Nerd Font Complete.ttf" }
    FontLoader { source: "qrc:///ttf/JetBrains Mono Bold Italic Nerd Font Complete Mono.ttf" }
    FontLoader { source: "qrc:///ttf/JetBrains Mono Bold Nerd Font Complete.ttf" }
    FontLoader { source: "qrc:///ttf/JetBrains Mono Bold Nerd Font Complete Mono.ttf" }
    FontLoader { source: "qrc:///ttf/JetBrains Mono ExtBd Ita Nerd Font Complete.ttf" }
    FontLoader { source: "qrc:///ttf/JetBrains Mono ExtBd Ita Nerd Font Complete Mono.ttf" }
    FontLoader { source: "qrc:///ttf/JetBrains Mono ExtraBold ExBd I Nerd Font Complete.ttf" }
    FontLoader { source: "qrc:///ttf/JetBrains Mono ExtraBold ExBd I Nerd Font Complete Mono.ttf" }
    FontLoader { source: "qrc:///ttf/JetBrains Mono ExtraBold ExtBd Nerd Font Complete.ttf" }
    FontLoader { source: "qrc:///ttf/JetBrains Mono ExtraBold ExtBd Nerd Font Complete Mono.ttf" }
    FontLoader { source: "qrc:///ttf/JetBrains Mono Extra Bold Nerd Font Complete.ttf" }
    FontLoader { source: "qrc:///ttf/JetBrains Mono Extra Bold Nerd Font Complete Mono.ttf" }
    FontLoader { source: "qrc:///ttf/JetBrains Mono Italic Nerd Font Complete.ttf" }
    FontLoader { source: "qrc:///ttf/JetBrains Mono Italic Nerd Font Complete Mono.ttf" }
    FontLoader { source: "qrc:///ttf/JetBrains Mono Medium Italic Nerd Font Complete.ttf" }
    FontLoader { source: "qrc:///ttf/JetBrains Mono Medium Italic Nerd Font Complete Mono.ttf" }
    FontLoader { source: "qrc:///ttf/JetBrains Mono Medium Med Ita Nerd Font Complete.ttf" }
    FontLoader { source: "qrc:///ttf/JetBrains Mono Medium Med Ita Nerd Font Complete Mono.ttf" }
    FontLoader { source: "qrc:///ttf/JetBrains Mono Medium Medium Nerd Font Complete.ttf" }
    FontLoader { source: "qrc:///ttf/JetBrains Mono Medium Medium Nerd Font Complete Mono.ttf" }
    FontLoader { source: "qrc:///ttf/JetBrains Mono Medium Nerd Font Complete.ttf" }
    FontLoader { source: "qrc:///ttf/JetBrains Mono Medium Nerd Font Complete Mono.ttf" }
    FontLoader { source: "qrc:///ttf/JetBrains Mono Regular Nerd Font Complete.ttf" }
    FontLoader { source: "qrc:///ttf/JetBrains Mono Regular Nerd Font Complete Mono.ttf" }
    FontLoader { source: "qrc:///ttf/MaterialDesignIcons.ttf" }
    FontLoader { source: "qrc:///ttf/MonoLisa-Black.ttf" }
    FontLoader { source: "qrc:///ttf/MonoLisa-BlackItalic.ttf" }
    FontLoader { source: "qrc:///ttf/MonoLisa-Bold.ttf" }
    FontLoader { source: "qrc:///ttf/MonoLisa-BoldItalic.ttf" }
    FontLoader { source: "qrc:///ttf/MonoLisa-ExtraLight.ttf" }
    FontLoader { source: "qrc:///ttf/MonoLisa-ExtraLightItalic.ttf" }
    FontLoader { source: "qrc:///ttf/MonoLisa-Light.ttf" }
    FontLoader { source: "qrc:///ttf/MonoLisa-LightItalic.ttf" }
    FontLoader { source: "qrc:///ttf/MonoLisa-Medium.ttf" }
    FontLoader { source: "qrc:///ttf/MonoLisa-MediumItalic.ttf" }
    FontLoader { source: "qrc:///ttf/MonoLisa-Regular.ttf" }
    FontLoader { source: "qrc:///ttf/MonoLisa-RegularItalic.ttf" }
    FontLoader { source: "qrc:///ttf/MonoLisa-Thin.ttf" }
    FontLoader { source: "qrc:///ttf/MonoLisa-ThinItalic.ttf" }
    FontLoader { source: "qrc:///ttf/OperatorMono-Bold.otf" }
    FontLoader { source: "qrc:///ttf/OperatorMono-BoldItalic.otf" }
    FontLoader { source: "qrc:///ttf/OperatorMono-Book.otf" }
    FontLoader { source: "qrc:///ttf/OperatorMono-BookItalic.otf" }
    FontLoader { source: "qrc:///ttf/OperatorMono-Light.otf" }
    FontLoader { source: "qrc:///ttf/OperatorMono-LightItalic.otf" }
    FontLoader { source: "qrc:///ttf/OperatorMono-Medium.otf" }
    FontLoader { source: "qrc:///ttf/OperatorMono-MediumItalic.otf" }
    FontLoader { source: "qrc:///ttf/OperatorMono-XLight.otf" }
    FontLoader { source: "qrc:///ttf/OperatorMono-XLightItalic.otf" }
    FontLoader { source: "qrc:///ttf/RobotoMono-Bold.ttf" }
    FontLoader { source: "qrc:///ttf/RobotoMono-BoldItalic.ttf" }
    FontLoader { source: "qrc:///ttf/RobotoMono-ExtraLight.ttf" }
    FontLoader { source: "qrc:///ttf/RobotoMono-ExtraLightItalic.ttf" }
    FontLoader { source: "qrc:///ttf/RobotoMono-Italic.ttf" }
    FontLoader { source: "qrc:///ttf/RobotoMono-Light.ttf" }
    FontLoader { source: "qrc:///ttf/RobotoMono-LightItalic.ttf" }
    FontLoader { source: "qrc:///ttf/RobotoMono-Medium.ttf" }
    FontLoader { source: "qrc:///ttf/RobotoMono-MediumItalic.ttf" }
    FontLoader { source: "qrc:///ttf/RobotoMono-Regular.ttf" }
    FontLoader { source: "qrc:///ttf/RobotoMono-SemiBold.ttf" }
    FontLoader { source: "qrc:///ttf/RobotoMono-SemiBoldItalic.ttf" }
    FontLoader { source: "qrc:///ttf/RobotoMono-Thin.ttf" }
    FontLoader { source: "qrc:///ttf/RobotoMono-ThinItalic.ttf" }
    
    LoggingCategory {
        id: journal_category
        name: "hexed"
        defaultLogLevel: LoggingCategory.Debug
    }
}
