import QtQuick
import QtQuick.Controls

Component {
    
    SplitView {
        
        id: self;
        
        SplitView.preferredWidth: parent ? parent.width/2 : 40;
        SplitView.preferredHeight: parent ? parent.height/2 : 40;
        
        handle: Rectangle {
            implicitWidth: 1
            implicitHeight: 1
            color: "#66666266";

            containmentMask: Item {
                x: self.orientation == Qt.Horizontal ? -5 :  0;
                y: self.orientation == Qt.Horizontal ?  0 : -5;
                z: Infinity;
                 width: self.orientation == Qt.Horizontal ? 10 : splitView.width;
                height: self.orientation == Qt.Horizontal ? splitView.height : 10;
            }
        }
    }
}
