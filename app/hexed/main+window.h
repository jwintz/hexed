#pragma once

#include <QtCore>
#include <QtGui>
#include <QtQml>
#include <QtQuick>

class hxWindowHelper : public QObject
{
    Q_OBJECT

public:
     hxWindowHelper(QQuickWindow *parent = nullptr);
    ~hxWindowHelper(void);

private:
    class hxWindowHelperPrivate *d;
};

void hxWindowHelperWrap(QQmlApplicationEngine *engine);
