import QtQuick
import QtQuick.Controls

import hexed.Controls as H
import hexed.Frontend as H

Component {
    
    H.Buffer {
    
        id: buffer;
    
        SplitView.preferredWidth: parent ? parent.width/2 : 40;
        SplitView.preferredHeight: parent ? parent.height/2 : 40;
        
        SplitView.minimumWidth: H.Layout.minimum_window_width;
        SplitView.minimumHeight: H.Layout.minimum_window_height;
    }
}
