#include "main+window.h"

#include <QtGlobal>
#include <QtQml>

#include <AppKit/AppKit.h>

class hxWindowHelperPrivate
{
public:
    QQuickWindow *window = nullptr;
};

hxWindowHelper::hxWindowHelper(QQuickWindow *parent) : QObject(parent)
{
    d = new hxWindowHelperPrivate;
    d->window = parent;
    d->window->setColor(QColor(Qt::transparent));
    
    NSView    *nativeView    = reinterpret_cast<NSView *>(d->window->winId());
    NSWindow  *nativeWindow  = [nativeView window];
    NSToolbar *nativeToolbar = [[NSToolbar alloc] init];

    [nativeView setAutoresizingMask: NSViewWidthSizable | NSViewHeightSizable];
    [nativeView removeFromSuperview];
    
    NSVisualEffectView *nativeEffectView = [[NSVisualEffectView alloc] init];
    nativeEffectView.blendingMode = NSVisualEffectBlendingModeBehindWindow;
    nativeEffectView.appearance = [NSAppearance appearanceNamed: NSAppearanceNameVibrantDark];
    [nativeEffectView setAutoresizingMask: NSViewWidthSizable | NSViewHeightSizable];
    
    [nativeWindow setContentView: nativeEffectView];
    [nativeWindow setStyleMask: [nativeWindow styleMask]
     | NSWindowStyleMaskTitled
     | NSWindowStyleMaskMiniaturizable
     | NSWindowStyleMaskResizable
     | NSWindowStyleMaskClosable
     | NSWindowStyleMaskFullSizeContentView];
    [nativeWindow setTitleVisibility: NSWindowTitleHidden];
    [nativeWindow setTitlebarAppearsTransparent: YES];
    [nativeWindow setMovableByWindowBackground: NO];
    [nativeWindow setToolbar: nativeToolbar];
    
    [nativeEffectView addSubview:nativeView positioned:NSWindowBelow relativeTo:nil];
}

hxWindowHelper::~hxWindowHelper(void)
{
    delete d;
}

void hxWindowHelperWrap(QQmlApplicationEngine *engine)
{
    Q_UNUSED(new hxWindowHelper(qobject_cast<QQuickWindow *>(engine->rootObjects().at(0))));
}
