import QtQuick
import QtQuick.Window
import QtQuick.Controls
import Qt.labs.settings

Item
{
    property Window window

    Settings
    {
        id: s
        property int x
        property int y
        property int width
        property int height
        property int visibility
    }

    Component.onCompleted:
    {
        if (s.width && s.height)
        {
            window.x = s.x;
            window.y = s.y;
            window.width = s.width;
            window.height = s.height;
            window.visibility = s.visibility;
        }
    }

    Connections
    {
        target: window
        function onXChanged() { saveSettingsTimer.restart(); }
        function onYChanged() { saveSettingsTimer.restart(); }
        function onWidthChanged() { saveSettingsTimer.restart(); }
        function onHeightChanged() { saveSettingsTimer.restart(); }
        function onVisibilityChanged() { saveSettingsTimer.restart(); }
    }

    Timer
    {
        id: saveSettingsTimer
        interval: 1000
        repeat: false
        onTriggered: saveSettings()
    }

    function saveSettings()
    {
        switch(window.visibility)
        {
        case ApplicationWindow.Windowed:
            // console.log('App is Windowed');
            s.x = window.x;
            s.y = window.y;
            s.width = window.width;
            s.height = window.height;
            s.visibility = window.visibility;
            break;
        case ApplicationWindow.FullScreen:
            // console.log('App is FullScreen');
            s.visibility = window.visibility;
            break;
        case ApplicationWindow.Maximized:
            // console.log('App is Maximized');
            s.visibility = window.visibility;
            break;
        }
    }
}
