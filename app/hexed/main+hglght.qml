import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

Rectangle {
    
    id: self;
    
    color: "#44ae91e8";
    radius: 4;
    
    x: 10;
    y: 10;
    z: Infinity;
    
    width: parent.width - 2*10;
    height: parent.height - 2*10;
    
    border.color: "#ae91e8";
    border.width: 3;
    
    opacity: 0;
    
    Rectangle {
        anchors.centerIn: parent;
        
        width: layout.width + 2*12;
        height: 28;
        
        color: "#ae91e8";
        radius: 4;
        
        RowLayout {
            id: layout;
            anchors.centerIn: parent;
            spacing: 4;
            Text {
                text: '󰋱';
                color: "#ffffff";
                font.family: "Material Design Icons";
                font.pointSize: 16;
            }
            
            Text {
                text: 'Move focus to window';
                color: "#ffffff";
                font.pointSize: 13;
            }
        }
        // Then layout.minimumSizes in compntn
    }
    
    Behavior on x { id: b_x; PropertyAnimation { duration: 100 } }
    Behavior on y { id: b_y; PropertyAnimation { duration: 100 } }
    Behavior on width  { id: b_w; PropertyAnimation { duration: 100 } }
    Behavior on height { id: b_h; PropertyAnimation { duration: 100 } }
    
    function enableSmoothBehavior() {
        b_x.enabled = true;
        b_y.enabled = true;
        b_w.enabled = true;
        b_h.enabled = true;
    }
    
    function disableSmoothBehavior() {
        b_x.enabled = false;
        b_y.enabled = false;
        b_w.enabled = false;
        b_h.enabled = false;
    }
    
    function fire() {
        self_animation.running ? self_animation.restart() : self_animation.start();
    }
    
    SequentialAnimation {
        
        id: self_animation;

        PropertyAnimation {
            target: self;
            properties: "opacity";
            from: 1; to: 1; duration: 100;
        }

        PropertyAnimation {
            target: self;
            properties: "opacity";
            from: 1; to: 0; duration: 400;
        }
    }
}
