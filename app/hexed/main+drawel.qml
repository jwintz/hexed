import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

import hexed.Core     as H
import hexed.Controls as H

Page {
    
    property alias current_index: contents.currentIndex;
    
    clip: true;
    
    header: ToolBar {
        height: 40;
        
        leftPadding: 0;
        rightPadding: 0;
        
        Rectangle {
            height: 1;
            color: "#66666266";
            anchors.left: parent.left;
            anchors.right: parent.right;
            anchors.bottom: parent.bottom;
        }
        
        background: Rectangle {
            color: "#00000000";
        }
    }
    
    ColumnLayout {
        
        anchors.fill: parent;
        spacing: 0;
        
        RowLayout {
            
            Layout.leftMargin: 8;
            Layout.rightMargin: 8;
            Layout.fillWidth: true;
            Layout.minimumHeight: 28 - 2;
            Layout.maximumHeight: 28 - 2;
            Layout.preferredHeight: 28 - 2;
            
            spacing: 0;
            
            Item { Layout.fillHeight: true; Layout.preferredWidth: (250 - 2 * 8) / 4; Text { color: current_index == 0 ? "#cab2fa" : "#cccccc" ; font.family: "Material Design Icons"; font.pointSize: 16; text: '󰉖'; anchors.centerIn: parent; } MouseArea { id: area1; anchors.fill: parent; hoverEnabled: true; onClicked: current_index = 0; } } // Project navigator
            Item { Layout.fillHeight: true; Layout.preferredWidth: (250 - 2 * 8) / 4; Text { color: current_index == 1 ? "#cab2fa" : "#cccccc" ; font.family: "Material Design Icons"; font.pointSize: 16; text: '󰘬'; anchors.centerIn: parent; } MouseArea { id: area2; anchors.fill: parent; hoverEnabled: true; onClicked: current_index = 1; } } // VCS navigator
            Item { Layout.fillHeight: true; Layout.preferredWidth: (250 - 2 * 8) / 4; Text { color: current_index == 2 ? "#cab2fa" : "#cccccc" ; font.family: "Material Design Icons"; font.pointSize: 16; text: '󰍉'; anchors.centerIn: parent; } MouseArea { id: area4; anchors.fill: parent; hoverEnabled: true; onClicked: current_index = 2; } } // Search navigator
            Item { Layout.fillHeight: true; Layout.preferredWidth: (250 - 2 * 8) / 4; Text { color: current_index == 3 ? "#cab2fa" : "#cccccc" ; font.family: "Material Design Icons"; font.pointSize: 16; text: '󰨸'; anchors.centerIn: parent; } MouseArea { id: area6; anchors.fill: parent; hoverEnabled: true; onClicked: current_index = 3; } } // Journal navigator
        }
        
        Rectangle {
            height: 1;
            color: "#66666266";
            Layout.fillWidth: true;
        }
        
        StackLayout {
            
            id: contents
            
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.margins: current_index == 3 ? 0 : 8
        
            H.NavigatorProject { id: navigator_project }
            Item {}
            Item {}
            H.NavigatorJournal { id: navigator_journal }
        }
    }
    
    Text {
        anchors.centerIn: parent;
        color: "#aacccccc";
        font.pointSize: 22
        text: current_index == 0 ? "Project navigator"
        : current_index == 1 ? "Version navigator"
        : current_index == 2 ? "Search navigator"
        : current_index == 3 ? "Journal navigator"
        : "";
        visible: true // current_index != 0
    }
    
    background: Rectangle {
        color: "#00000000";
    }
}
