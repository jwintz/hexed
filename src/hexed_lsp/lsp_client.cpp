#include "lsp_client.h"

#include <QCoreApplication>
#include <QJsonDocument>
#include <QJsonObject>

#include <iostream>

using RequestID = std::string;

hxLSPClient::hxLSPClient(QString path, QStringList args)
{
    clientProcess = new QProcess();
    clientProcess->setProgram(path);
    clientProcess->setArguments(args);

    connect(clientProcess, SIGNAL(errorOccurred(QProcess::ProcessError)), this,
            SLOT(onClientError(QProcess::ProcessError)));
    connect(clientProcess, SIGNAL(readyReadStandardOutput()), this, SLOT(onClientReadyReadStdout()));
    connect(clientProcess, SIGNAL(readyReadStandardError()), this, SLOT(onClientReadyReadStderr()));
    connect(clientProcess, SIGNAL(finished(int, QProcess::ExitStatus)), this,
            SLOT(onClientFinished(int, QProcess::ExitStatus)));

    clientProcess->start();
}

// slots

void hxLSPClient::onClientReadyReadStdout()
{
    // Fixme(coder3101): If all buffer does not comes in one go, then we are in trouble
    // We can fix it later by specifying that if incomplete buffer recieved in one time
    // then in the next time. We should append and emit the signal only onces.

    QByteArray buffer = clientProcess->readAllStandardOutput();

    int messageStart = buffer.indexOf("\r\n\r\n") + 4;
    int lenStart = buffer.indexOf("Content-Length: ") + 16;
    int lenEnd = buffer.indexOf("\r\n");
    bool ok = false;
    int contentLength = buffer.mid(lenStart, lenEnd - lenStart).toInt(&ok);

    if (!ok)
        return;

    QByteArray payload = buffer.mid(messageStart);
    if (payload.size() != contentLength)
    {
        // Warning: Incomplete message has arrived,
        // At this point we should keep it in buffer but we are discarding this message;
        return;
    }
    QJsonParseError error{};

    auto msg = QJsonDocument::fromJson(payload, &error);

    if (error.error != QJsonParseError::NoError || !msg.isObject())
    {
        // Some JSON Parse Error
        return;
    }
    auto obj = msg.object();

    if (obj.contains("id"))
    {
        if (obj.contains("method"))
        {
            emit onRequest(obj["method"].toString(), obj["param"].toObject(), obj["id"].toObject());
        }
        else if (obj.contains("result"))
        {
            emit onResponse(obj["id"].toObject(), obj["result"].toObject());
        }
        else if (obj.contains("error"))
        {
            emit onError(obj["id"].toObject(), obj["error"].toObject());
        }
    }
    else if (obj.contains("method"))
    {
        // notification
        if (obj.contains("params"))
        {
            emit onNotify(obj["method"].toString(), obj["params"].toObject());
        }
    }
}

void hxLSPClient::onClientReadyReadStderr()
{
    QString content = clientProcess->readAllStandardError();
    if (!content.isEmpty())
        emit newStderr(content);
}

void hxLSPClient::onClientError(QProcess::ProcessError error)
{
    emit onServerError(error);
}

void hxLSPClient::onClientFinished(int exitCode, QProcess::ExitStatus status)
{
    emit onServerFinished(exitCode, status);
}

// Protocol methods
RequestID hxLSPClient::initialize(option<DocumentUri> rootUri)
{
    if (hasInitialized)
        return "[Didn't send request because the server is already initialized]";
    hasInitialized = true;
    hxLSPInitializeParams params;
    params.processId = static_cast<unsigned int>(QCoreApplication::applicationPid());
    params.rootUri = rootUri;
    return SendRequest("initialize", params);
}

RequestID hxLSPClient::shutdown()
{
    return SendRequest("shutdown", json());
}
RequestID hxLSPClient::sync()
{
    return SendRequest("sync", json());
}
void hxLSPClient::exit()
{
    SendNotification("exit", json());
}
void hxLSPClient::initialized()
{
    SendNotification("initialized", json());
}
RequestID hxLSPClient::registerCapability()
{
    return SendRequest("client/registerCapability", json());
}
void hxLSPClient::didOpen(DocumentUri uri, string_ref text, string_ref languageId = "cpp")
{
    hxLSPDidOpenTextDocumentParams params;
    params.textDocument.uri = uri;
    params.textDocument.text = text;
    params.textDocument.languageId = languageId;
    SendNotification("textDocument/didOpen", params);
}
void hxLSPClient::didClose(DocumentUri uri)
{
    hxLSPDidCloseTextDocumentParams params;
    params.textDocument.uri = uri;
    SendNotification("textDocument/didClose", params);
}
void hxLSPClient::didChange(DocumentUri uri, std::vector<hxLSPTextDocumentContentChangeEvent> &changes,
                          option<bool> wantDiagnostics)
{
    hxLSPDidChangeTextDocumentParams params;
    params.textDocument.uri = uri;
    params.contentChanges = std::move(changes);
    params.wantDiagnostics = wantDiagnostics;
    SendNotification("textDocument/didChange", params);
}
RequestID hxLSPClient::rangeFomatting(DocumentUri uri, hxLSPRange range)
{
    hxLSPDocumentRangeFormattingParams params;
    params.textDocument.uri = uri;
    params.range = range;
    return SendRequest("textDocument/rangeFormatting", params);
}
RequestID hxLSPClient::foldingRange(DocumentUri uri)
{
    hxLSPFoldingRangeParams params;
    params.textDocument.uri = uri;
    return SendRequest("textDocument/foldingRange", params);
}
RequestID hxLSPClient::selectionRange(DocumentUri uri, std::vector<hxLSPPosition> &positions)
{
    hxLSPSelectionRangeParams params;
    params.textDocument.uri = uri;
    params.positions = std::move(positions);
    return SendRequest("textDocument/selectionRange", params);
}
RequestID hxLSPClient::onTypeFormatting(DocumentUri uri, hxLSPPosition position, string_ref ch)
{
    hxLSPDocumentOnTypeFormattingParams params;
    params.textDocument.uri = uri;
    params.position = position;
    params.ch = ch;
    return SendRequest("textDocument/onTypeFormatting", params);
}
RequestID hxLSPClient::formatting(DocumentUri uri)
{
    hxLSPDocumentFormattingParams params;
    params.textDocument.uri = uri;
    return SendRequest("textDocument/formatting", params);
}
RequestID hxLSPClient::codeAction(DocumentUri uri, hxLSPRange range, hxLSPCodeActionContext context)
{
    hxLSPCodeActionParams params;
    params.textDocument.uri = uri;
    params.range = range;
    params.context = std::move(context);
    return SendRequest("textDocument/codeAction", std::move(params));
}
RequestID hxLSPClient::completion(DocumentUri uri, hxLSPPosition position, option<hxLSPCompletionContext> context)
{
    hxLSPCompletionParams params;
    params.textDocument.uri = uri;
    params.position = position;
    params.context = context;
    return SendRequest("textDocument/completion", params);
}
RequestID hxLSPClient::signatureHelp(DocumentUri uri, hxLSPPosition position)
{
    hxLSPTextDocumentPositionParams params;
    params.textDocument.uri = uri;
    params.position = position;
    return SendRequest("textDocument/signatureHelp", params);
}
RequestID hxLSPClient::gotoDefinition(DocumentUri uri, hxLSPPosition position)
{
    hxLSPTextDocumentPositionParams params;
    params.textDocument.uri = uri;
    params.position = position;
    return SendRequest("textDocument/definition", params);
}
RequestID hxLSPClient::gotoDeclaration(DocumentUri uri, hxLSPPosition position)
{
    hxLSPTextDocumentPositionParams params;
    params.textDocument.uri = uri;
    params.position = position;
    return SendRequest("textDocument/declaration", params);
}
RequestID hxLSPClient::references(DocumentUri uri, hxLSPPosition position)
{
    hxLSPReferenceParams params;
    params.textDocument.uri = uri;
    params.position = position;
    return SendRequest("textDocument/references", params);
}
RequestID hxLSPClient::switchSourceHeader(DocumentUri uri)
{
    hxLSPTextDocumentIdentifier params;
    params.uri = uri;
    return SendRequest("textDocument/references", params);
}
RequestID hxLSPClient::rename(DocumentUri uri, hxLSPPosition position, string_ref newName)
{
    hxLSPRenameParams params;
    params.textDocument.uri = uri;
    params.position = position;
    params.newName = newName;
    return SendRequest("textDocument/rename", std::move(params));
}
RequestID hxLSPClient::hover(DocumentUri uri, hxLSPPosition position)
{
    hxLSPTextDocumentPositionParams params;
    params.textDocument.uri = uri;
    params.position = position;
    return SendRequest("textDocument/hover", params);
}
RequestID hxLSPClient::documentSymbol(DocumentUri uri)
{
    hxLSPDocumentSymbolParams params;
    params.textDocument.uri = uri;
    return SendRequest("textDocument/documentSymbol", params);
}
RequestID hxLSPClient::documentColor(DocumentUri uri)
{
    hxLSPDocumentSymbolParams params;
    params.textDocument.uri = uri;
    return SendRequest("textDocument/documentColor", params);
}
RequestID hxLSPClient::documentHighlight(DocumentUri uri, hxLSPPosition position)
{
    hxLSPTextDocumentPositionParams params;
    params.textDocument.uri = uri;
    params.position = position;
    return SendRequest("textDocument/documentHighlight", params);
}
RequestID hxLSPClient::symbolInfo(DocumentUri uri, hxLSPPosition position)
{
    hxLSPTextDocumentPositionParams params;
    params.textDocument.uri = uri;
    params.position = position;
    return SendRequest("textDocument/symbolInfo", params);
}
RequestID hxLSPClient::typeHierarchy(DocumentUri uri, hxLSPPosition position, hxLSPTypeHierarchyDirection direction, int resolve)
{
    hxLSPTypeHierarchyParams params;
    params.textDocument.uri = uri;
    params.position = position;
    params.direction = direction;
    params.resolve = resolve;
    return SendRequest("textDocument/typeHierarchy", params);
}
RequestID hxLSPClient::workspaceSymbol(string_ref query)
{
    hxLSPWorkspaceSymbolParams params;
    params.query = query;
    return SendRequest("workspace/symbol", params);
}
RequestID hxLSPClient::executeCommand(string_ref cmd, option<hxLSPTweakArgs> tweakArgs, option<hxLSPWorkspaceEdit> workspaceEdit)
{
    hxLSPExecuteCommandParams params;
    params.tweakArgs = std::move(tweakArgs);
    params.workspaceEdit = std::move(workspaceEdit);
    params.command = cmd;
    return SendRequest("workspace/executeCommand", std::move(params));
}
RequestID hxLSPClient::didChangeWatchedFiles(std::vector<hxLSPFileEvent> &changes)
{
    hxLSPDidChangeWatchedFilesParams params;
    params.changes = std::move(changes);
    return SendRequest("workspace/didChangeWatchedFiles", std::move(params));
}
RequestID hxLSPClient::didChangeConfiguration(hxLSPConfigurationSettings &settings)
{
    hxLSPDidChangeConfigurationParams params;
    params.settings = std::move(settings);
    return SendRequest("workspace/didChangeConfiguration", std::move(params));
}

// general send and notify
void hxLSPClient::sendNotification(string_ref method, QJsonDocument &jsonDoc)
{
    json doc = toNlohmann(jsonDoc);
    notify(method, doc);
}

RequestID hxLSPClient::sendRequest(string_ref method, QJsonDocument &jsonDoc)
{
    std::string id = method.str();
    json doc = toNlohmann(jsonDoc);
    request(method, doc, id);
    return id;
}

// general send and notify
void hxLSPClient::SendNotification(string_ref method, json jsonDoc)
{
    notify(method, std::move(jsonDoc));
}

RequestID hxLSPClient::SendRequest(string_ref method, json jsonDoc)
{
    std::string id = method.str();
    request(method, std::move(jsonDoc), id);
    return id;
}

// private

void hxLSPClient::writeToServer(std::string &content)
{
    writeToServerBuffer.push_back("Content-Length: " + std::to_string(content.length()) + "\r\n");
    writeToServerBuffer.push_back("\r\n");
    writeToServerBuffer.push_back(content);
    if (clientProcess != nullptr && clientProcess->state() == QProcess::Running)
    {
        for (auto s : writeToServerBuffer)
            clientProcess->write(s.c_str());
        writeToServerBuffer.clear();
    }
}

json hxLSPClient::toNlohmann(QJsonDocument &doc)
{
    return json{doc.toJson().toStdString()};
}

QJsonDocument hxLSPClient::toJSONDoc(json &nlohman)
{
    QString rawStr = QString::fromStdString(nlohman.dump());
    return QJsonDocument::fromJson(rawStr.toLocal8Bit());
}

void hxLSPClient::notify(string_ref method, json value)
{
    json payload = {{"jsonrpc", "2.0"}, {"method", method}, {"params", value}};
    std::string content = payload.dump();
    writeToServer(content);
}

void hxLSPClient::request(string_ref method, json param, RequestID id)
{
    json rpc = {{"jsonrpc", "2.0"}, {"id", id}, {"method", method}, {"params", param}};
    std::string content = rpc.dump();
    writeToServer(content);
}

hxLSPClient::~hxLSPClient()
{
    {
        if (clientProcess != nullptr)
        {
            clientProcess->kill();
            delete clientProcess;
        }
    }
}
