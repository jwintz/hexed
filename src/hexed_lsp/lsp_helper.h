#include <QtCore>
#include <QtGui>
#include <QtQml>
#include <QtQuick>

#include "lsp_client.h"

#include <private/qquicktextedit_p.h>

class hxLSPHelper : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString server READ server NOTIFY serverChanged)

public:
    hxLSPHelper(QObject *parent = nullptr) : QObject(parent) {}
   ~hxLSPHelper(void);

signals:
    void monitoring(void);

signals:
    void diagnostics(QJsonArray items);
    void completions(QJsonArray items);

signals:
    void serverChanged(void);

public:
    Q_INVOKABLE void setup(const QString& source, QQuickTextEdit *);

public:
    Q_INVOKABLE QString server(void);

public:
    QTextDocument *code_d;
    // QTextDocument *outp_d;

public:
    hxLSPClient *client;

public:
    QFile     file;
    QFileInfo file_info;

public:
    int   _line { -1 };
    int _column { -1 };

public:
    QTimer timer;
};

