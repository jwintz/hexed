#pragma once

#include "lsp.h"
#include "lsp_uri.h"

#include <QJsonDocument>
#include <QJsonObject>
#include <QObject>
#include <QProcess>

class hxLSPClient : public QObject
{
    Q_OBJECT

    using RequestID = std::string;

  public:
    explicit hxLSPClient(QString processPath, QStringList args);

    hxLSPClient(hxLSPClient &&) = delete;
    hxLSPClient(hxLSPClient &) = delete;

    hxLSPClient &operator=(hxLSPClient &&) = delete;
    hxLSPClient &operator=(hxLSPClient &) = delete;

    ~hxLSPClient() final;

    // LSP methods Requests to send to server
    RequestID initialize(option<DocumentUri> rootUri = {});
    RequestID shutdown();
    RequestID sync();
    RequestID registerCapability();

    RequestID rangeFomatting(DocumentUri uri, hxLSPRange range);
    RequestID foldingRange(DocumentUri uri);
    RequestID selectionRange(DocumentUri uri, std::vector<hxLSPPosition> &positions);
    RequestID onTypeFormatting(DocumentUri uri, hxLSPPosition position, string_ref ch);

    RequestID formatting(DocumentUri uri);
    RequestID codeAction(DocumentUri uri, hxLSPRange range, hxLSPCodeActionContext context);
    RequestID completion(DocumentUri uri, hxLSPPosition position, option<hxLSPCompletionContext> context = {});
    RequestID signatureHelp(DocumentUri uri, hxLSPPosition position);
    RequestID gotoDefinition(DocumentUri uri, hxLSPPosition position);
    RequestID gotoDeclaration(DocumentUri uri, hxLSPPosition position);
    RequestID references(DocumentUri uri, hxLSPPosition position);
    RequestID switchSourceHeader(DocumentUri uri);
    RequestID rename(DocumentUri uri, hxLSPPosition position, string_ref newName);
    RequestID hover(DocumentUri uri, hxLSPPosition position);
    RequestID documentSymbol(DocumentUri uri);
    RequestID documentColor(DocumentUri uri);
    RequestID documentHighlight(DocumentUri uri, hxLSPPosition position);
    RequestID symbolInfo(DocumentUri uri, hxLSPPosition position);
    RequestID typeHierarchy(DocumentUri uri, hxLSPPosition position, hxLSPTypeHierarchyDirection direction, int resolve);
    RequestID workspaceSymbol(string_ref query);
    RequestID executeCommand(string_ref cmd, option<hxLSPTweakArgs> tweakArgs = {},
                             option<hxLSPWorkspaceEdit> workspaceEdit = {});

    RequestID didChangeWatchedFiles(std::vector<hxLSPFileEvent> &changes);
    RequestID didChangeConfiguration(hxLSPConfigurationSettings &settings);

    // LSP methods notifications to Send to server
    void exit();
    void initialized();
    void didOpen(DocumentUri uri, string_ref code, string_ref lang);
    void didClose(DocumentUri uri);
    void didChange(DocumentUri uri, std::vector<hxLSPTextDocumentContentChangeEvent> &changes,
                   option<bool> wantDiagnostics = {});

    // General sender and requester for sever
    void sendNotification(string_ref method, QJsonDocument &jsonDoc);
    RequestID sendRequest(string_ref method, QJsonDocument &jsonDoc);

  signals:
    void onNotify(QString method, QJsonObject param);
    void onResponse(QJsonObject id, QJsonObject response);
    void onRequest(QString method, QJsonObject param, QJsonObject id);
    void onError(QJsonObject id, QJsonObject error);
    void onServerError(QProcess::ProcessError error);
    void onServerFinished(int exitCode, QProcess::ExitStatus status);
    void newStderr(const QString &content);

  private slots:
    void onClientReadyReadStdout();
    void onClientReadyReadStderr();
    void onClientError(QProcess::ProcessError error);
    void onClientFinished(int exitCode, QProcess::ExitStatus status);

  private:
    QProcess *clientProcess = nullptr;
    std::vector<std::string> writeToServerBuffer;
    bool hasInitialized = false;

    void writeToServer(std::string &in);

    QJsonDocument toJSONDoc(json &nlohman);
    json toNlohmann(QJsonDocument &doc);

    void notify(string_ref method, json value);
    void request(string_ref mthod, json param, RequestID id);

    void SendNotification(string_ref method, json jsonDoc);
    RequestID SendRequest(string_ref method, json jsonDoc);
};
