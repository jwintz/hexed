#pragma once

#include "lsp_uri.h"

#include <map>
#include <string>
#include <tuple>
#include <memory>
#include <vector>

#define MAP_JSON(...)                                                                                                  \
    {                                                                                                                  \
        j = {__VA_ARGS__};                                                                                             \
    }
#define MAP_KEY(KEY)                                                                                                   \
    {                                                                                                                  \
#KEY, value.KEY                                                                                                        \
    }
#define MAP_TO(KEY, TO)                                                                                                \
    {                                                                                                                  \
        KEY, value.TO                                                                                                  \
    }
#define MAP_KV(K, ...)                                                                                                 \
    {                                                                                                                  \
        K,                                                                                                             \
        {                                                                                                              \
            __VA_ARGS__                                                                                                \
        }                                                                                                              \
    }
#define FROM_KEY(KEY)                                                                                                  \
    if (j.contains(#KEY))                                                                                              \
        j.at(#KEY).get_to(value.KEY);
#define JSON_SERIALIZE(Type, TO, FROM)                                                                                 \
    namespace nlohmann                                                                                                 \
    {                                                                                                                  \
    template <> struct adl_serializer<Type>                                                                            \
    {                                                                                                                  \
        static void to_json(json &j, const Type &value) TO static void from_json(const json &j, Type &value) FROM      \
    };                                                                                                                 \
    }
using TextType = string_ref;
enum class hxLSPErrorCode
{
    // Defined by JSON RPC.
    ParseError = -32700,
    InvalidRequest = -32600,
    MethodNotFound = -32601,
    InvalidParams = -32602,
    InternalError = -32603,
    ServerNotInitialized = -32002,
    UnknownErrorCode = -32001,
    // Defined by the protocol.
    RequestCancelled = -32800,
};
class hxLSPError
{
  public:
    TextType Message;
    hxLSPErrorCode Code;
    static char ID;
    hxLSPError(TextType Message, hxLSPErrorCode Code) : Message(Message), Code(Code)
    {
    }
};
JSON_SERIALIZE(
    URIForFile, { j = value.file; }, { value.file = j.get<std::string>(); })
struct hxLSPTextDocumentIdentifier
{
    /// The text document's URI.
    DocumentUri uri;
};
JSON_SERIALIZE(hxLSPTextDocumentIdentifier, MAP_JSON(MAP_KEY(uri)), {})

struct hxLSPVersionedTextDocumentIdentifier : public hxLSPTextDocumentIdentifier
{
    int version = 0;
};
JSON_SERIALIZE(hxLSPVersionedTextDocumentIdentifier, MAP_JSON(MAP_KEY(uri), MAP_KEY(version)), {})

struct hxLSPPosition
{
    /// Line position in a document (zero-based).
    int line = 0;
    /// Character offset on a line in a document (zero-based).
    /// WARNING: this is in UTF-16 codepoints, not bytes or characters!
    /// Use the functions in SourceCode.h to construct/interpret Positions.
    int character = 0;
    friend bool operator==(const hxLSPPosition &LHS, const hxLSPPosition &RHS)
    {
        return std::tie(LHS.line, LHS.character) == std::tie(RHS.line, RHS.character);
    }
    friend bool operator!=(const hxLSPPosition &LHS, const hxLSPPosition &RHS)
    {
        return !(LHS == RHS);
    }
    friend bool operator<(const hxLSPPosition &LHS, const hxLSPPosition &RHS)
    {
        return std::tie(LHS.line, LHS.character) < std::tie(RHS.line, RHS.character);
    }
    friend bool operator<=(const hxLSPPosition &LHS, const hxLSPPosition &RHS)
    {
        return std::tie(LHS.line, LHS.character) <= std::tie(RHS.line, RHS.character);
    }
};
JSON_SERIALIZE(hxLSPPosition, MAP_JSON(MAP_KEY(line), MAP_KEY(character)), {
    FROM_KEY(line);
    FROM_KEY(character)
})

struct hxLSPRange
{
    /// The range's start position.
    hxLSPPosition start;

    /// The range's end position.
    hxLSPPosition end;

    friend bool operator==(const hxLSPRange &LHS, const hxLSPRange &RHS)
    {
        return std::tie(LHS.start, LHS.end) == std::tie(RHS.start, RHS.end);
    }
    friend bool operator!=(const hxLSPRange &LHS, const hxLSPRange &RHS)
    {
        return !(LHS == RHS);
    }
    friend bool operator<(const hxLSPRange &LHS, const hxLSPRange &RHS)
    {
        return std::tie(LHS.start, LHS.end) < std::tie(RHS.start, RHS.end);
    }
    bool contains(hxLSPPosition Pos) const
    {
        return start <= Pos && Pos < end;
    }
    bool contains(hxLSPRange Rng) const
    {
        return start <= Rng.start && Rng.end <= end;
    }
};
JSON_SERIALIZE(hxLSPRange, MAP_JSON(MAP_KEY(start), MAP_KEY(end)), {
    FROM_KEY(start);
    FROM_KEY(end)
})

struct hxLSPLocation
{
    /// The text document's URI.
    std::string uri;
    hxLSPRange range;

    friend bool operator==(const hxLSPLocation &LHS, const hxLSPLocation &RHS)
    {
        return LHS.uri == RHS.uri && LHS.range == RHS.range;
    }
    friend bool operator!=(const hxLSPLocation &LHS, const hxLSPLocation &RHS)
    {
        return !(LHS == RHS);
    }
    friend bool operator<(const hxLSPLocation &LHS, const hxLSPLocation &RHS)
    {
        return std::tie(LHS.uri, LHS.range) < std::tie(RHS.uri, RHS.range);
    }
};
JSON_SERIALIZE(hxLSPLocation, MAP_JSON(MAP_KEY(uri), MAP_KEY(range)), {
    FROM_KEY(uri);
    FROM_KEY(range)
})

struct hxLSPTextEdit
{
    /// The range of the text document to be manipulated. To insert
    /// text into a document create a range where start === end.
    hxLSPRange range;

    /// The string to be inserted. For delete operations use an
    /// empty string.
    std::string newText;
};
JSON_SERIALIZE(hxLSPTextEdit, MAP_JSON(MAP_KEY(range), MAP_KEY(newText)), {
    FROM_KEY(range);
    FROM_KEY(newText);
})

struct hxLSPTextDocumentItem
{
    /// The text document's URI.
    DocumentUri uri;

    /// The text document's language identifier.
    string_ref languageId;

    /// The version number of this document (it will strictly increase after each
    int version = 0;

    /// The content of the opened text document.
    string_ref text;
};
JSON_SERIALIZE(hxLSPTextDocumentItem, MAP_JSON(MAP_KEY(uri), MAP_KEY(languageId), MAP_KEY(version), MAP_KEY(text)), {})

enum class hxLSPTraceLevel
{
    Off = 0,
    Messages = 1,
    Verbose = 2,
};
enum class hxLSPTextDocumentSyncKind
{
    /// Documents should not be synced at all.
    None = 0,

    /// Documents are synced by always sending the full content of the document.
    Full = 1,

    /// Documents are synced by sending the full content on open.  After that
    /// only incremental updates to the document are send.
    Incremental = 2,
};
enum class hxLSPCompletionItemKind
{
    Missing = 0,
    Text = 1,
    Method = 2,
    Function = 3,
    Constructor = 4,
    Field = 5,
    Variable = 6,
    Class = 7,
    Interface = 8,
    Module = 9,
    Property = 10,
    Unit = 11,
    Value = 12,
    Enum = 13,
    Keyword = 14,
    Snippet = 15,
    Color = 16,
    File = 17,
    Reference = 18,
    Folder = 19,
    EnumMember = 20,
    Constant = 21,
    Struct = 22,
    Event = 23,
    Operator = 24,
    TypeParameter = 25,
};
enum class hxLSPSymbolKind
{
    File = 1,
    Module = 2,
    Namespace = 3,
    Package = 4,
    Class = 5,
    Method = 6,
    Property = 7,
    Field = 8,
    Constructor = 9,
    Enum = 10,
    Interface = 11,
    Function = 12,
    Variable = 13,
    Constant = 14,
    String = 15,
    Number = 16,
    Boolean = 17,
    Array = 18,
    Object = 19,
    Key = 20,
    Null = 21,
    EnumMember = 22,
    Struct = 23,
    Event = 24,
    Operator = 25,
    TypeParameter = 26
};
enum class hxLSPOffsetEncoding
{
    // Any string is legal on the wire. Unrecognized encodings parse as this.
    UnsupportedEncoding,
    // Length counts code units of UTF-16 encoded text. (Standard LSP behavior).
    UTF16,
    // Length counts bytes of UTF-8 encoded text. (Clangd extension).
    UTF8,
    // Length counts codepoints in unicode text. (Clangd extension).
    UTF32,
};
enum class hxLSPMarkupKind
{
    PlainText,
    Markdown,
};
enum class hxLSPResourceOperationKind
{
    Create,
    Rename,
    Delete
};
enum class hxLSPFailureHandlingKind
{
    Abort,
    Transactional,
    Undo,
    TextOnlyTransactional
};
NLOHMANN_JSON_SERIALIZE_ENUM(hxLSPOffsetEncoding, {
                                                 {hxLSPOffsetEncoding::UnsupportedEncoding, "unspported"},
                                                 {hxLSPOffsetEncoding::UTF8, "utf-8"},
                                                 {hxLSPOffsetEncoding::UTF16, "utf-16"},
                                                 {hxLSPOffsetEncoding::UTF32, "utf-32"},
                                             })
NLOHMANN_JSON_SERIALIZE_ENUM(hxLSPMarkupKind, {
                                             {hxLSPMarkupKind::PlainText, "plaintext"},
                                             {hxLSPMarkupKind::Markdown, "markdown"},
                                         })
NLOHMANN_JSON_SERIALIZE_ENUM(hxLSPResourceOperationKind, {{hxLSPResourceOperationKind::Create, "create"},
                                                     {hxLSPResourceOperationKind::Rename, "rename"},
                                                     {hxLSPResourceOperationKind::Delete, "dename"}})
NLOHMANN_JSON_SERIALIZE_ENUM(hxLSPFailureHandlingKind,
                             {{hxLSPFailureHandlingKind::Abort, "abort"},
                              {hxLSPFailureHandlingKind::Transactional, "transactional"},
                              {hxLSPFailureHandlingKind::Undo, "undo"},
                              {hxLSPFailureHandlingKind::TextOnlyTransactional, "textOnlyTransactional"}})

struct hxLSPClientCapabilities
{
    /// The supported set of SymbolKinds for workspace/symbol.
    /// workspace.symbol.symbolKind.valueSet
    std::vector<hxLSPSymbolKind> WorkspaceSymbolKinds;
    /// Whether the client accepts diagnostics with codeActions attached inline.
    /// textDocument.publishDiagnostics.codeActionsInline.
    bool DiagnosticFixes = true;

    /// Whether the client accepts diagnostics with related locations.
    /// textDocument.publishDiagnostics.relatedInformation.
    bool DiagnosticRelatedInformation = true;

    /// Whether the client accepts diagnostics with category attached to it
    /// using the "category" extension.
    /// textDocument.publishDiagnostics.categorySupport
    bool DiagnosticCategory = true;

    /// Client supports snippets as insert text.
    /// textDocument.completion.completionItem.snippetSupport
    bool CompletionSnippets = true;

    bool CompletionDeprecated = true;

    /// Client supports completions with additionalTextEdit near the cursor.
    /// This is a clangd extension. (LSP says this is for unrelated text only).
    /// textDocument.completion.editsNearCursor
    bool CompletionFixes = true;

    /// Client supports hierarchical document symbols.
    bool HierarchicalDocumentSymbol = true;

    /// Client supports processing label offsets instead of a simple label string.
    bool OffsetsInSignatureHelp = true;

    /// The supported set of CompletionItemKinds for textDocument/completion.
    /// textDocument.completion.completionItemKind.valueSet
    std::vector<hxLSPCompletionItemKind> CompletionItemKinds;

    /// Client supports CodeAction return value for textDocument/codeAction.
    /// textDocument.codeAction.codeActionLiteralSupport.
    bool CodeActionStructure = true;
    /// Supported encodings for LSP character offsets. (clangd extension).
    std::vector<hxLSPOffsetEncoding> offsetEncoding = {hxLSPOffsetEncoding::UTF8};
    /// The content format that should be used for Hover requests.
    std::vector<hxLSPMarkupKind> HoverContentFormat = {hxLSPMarkupKind::PlainText};

    bool ApplyEdit = false;
    bool DocumentChanges = false;
    hxLSPClientCapabilities()
    {
        for (int i = 1; i <= 26; ++i)
        {
            WorkspaceSymbolKinds.push_back((hxLSPSymbolKind)i);
        }
        for (int i = 0; i <= 25; ++i)
        {
            CompletionItemKinds.push_back((hxLSPCompletionItemKind)i);
        }
    }
};
JSON_SERIALIZE(
    hxLSPClientCapabilities,
    MAP_JSON(MAP_KV("textDocument",
                    MAP_KV("publishDiagnostics", // PublishDiagnosticsClientCapabilities
                           MAP_TO("categorySupport", DiagnosticCategory), MAP_TO("codeActionsInline", DiagnosticFixes),
                           MAP_TO("relatedInformation", DiagnosticRelatedInformation), ),
                    MAP_KV("completion", // CompletionClientCapabilities
                           MAP_KV("completionItem", MAP_TO("snippetSupport", CompletionSnippets),
                                  MAP_TO("deprecatedSupport", CompletionDeprecated)),
                           MAP_KV("completionItemKind", MAP_TO("valueSet", CompletionItemKinds)),
                           MAP_TO("editsNearCursor", CompletionFixes)),
                    MAP_KV("codeAction", MAP_TO("codeActionLiteralSupport", CodeActionStructure)),
                    MAP_KV("documentSymbol", MAP_TO("hierarchicalDocumentSymbolSupport", HierarchicalDocumentSymbol)),
                    MAP_KV("hover", // HoverClientCapabilities
                           MAP_TO("contentFormat", HoverContentFormat)),
                    MAP_KV("signatureHelp", MAP_KV("signatureInformation",
                                                   MAP_KV("parameterInformation",
                                                          MAP_TO("labelOffsetSupport", OffsetsInSignatureHelp))))),
             MAP_KV("workspace",     // WorkspaceEditClientCapabilities
                    MAP_KV("symbol", // WorkspaceSymbolClientCapabilities
                           MAP_KV("symbolKind", MAP_TO("valueSet", WorkspaceSymbolKinds))),
                    MAP_TO("applyEdit", ApplyEdit),
                    MAP_KV("workspaceEdit", // WorkspaceEditClientCapabilities
                           MAP_TO("documentChanges", DocumentChanges))),
             MAP_TO("offsetEncoding", offsetEncoding)),
    {})

struct hxLSPClangdCompileCommand
{
    TextType workingDirectory;
    std::vector<TextType> compilationCommand;
};
JSON_SERIALIZE(hxLSPClangdCompileCommand, MAP_JSON(MAP_KEY(workingDirectory), MAP_KEY(compilationCommand)), {})

struct hxLSPConfigurationSettings
{
    // Changes to the in-memory compilation database.
    // The key of the map is a file name.
    std::map<std::string, hxLSPClangdCompileCommand> compilationDatabaseChanges;
};
JSON_SERIALIZE(hxLSPConfigurationSettings, MAP_JSON(MAP_KEY(compilationDatabaseChanges)), {})

struct hxLSPInitializationOptions
{
    // What we can change throught the didChangeConfiguration request, we can
    // also set through the initialize request (initializationOptions field).
    hxLSPConfigurationSettings configSettings;

    option<TextType> compilationDatabasePath;
    // Additional flags to be included in the "fallback command" used when
    // the compilation database doesn't describe an opened file.
    // The command used will be approximately `clang $FILE $fallbackFlags`.
    std::vector<TextType> fallbackFlags;

    /// Clients supports show file status for textDocument/clangd.fileStatus.
    bool clangdFileStatus = false;
};
JSON_SERIALIZE(hxLSPInitializationOptions,
               MAP_JSON(MAP_KEY(configSettings), MAP_KEY(compilationDatabasePath), MAP_KEY(fallbackFlags),
                        MAP_KEY(clangdFileStatus)),
               {})

struct hxLSPInitializeParams
{
    unsigned processId = 0;
    hxLSPClientCapabilities capabilities;
    option<DocumentUri> rootUri;
    option<TextType> rootPath;
    hxLSPInitializationOptions initializationOptions;
};
JSON_SERIALIZE(hxLSPInitializeParams,
               MAP_JSON(MAP_KEY(processId), MAP_KEY(capabilities), MAP_KEY(rootUri), MAP_KEY(initializationOptions),
                        MAP_KEY(rootPath)),
               {})

enum class hxLSPMessageType
{
    /// An error message.
    Error = 1,
    /// A warning message.
    Warning = 2,
    /// An information message.
    Info = 3,
    /// A log message.
    Log = 4,
};
struct hxLSPShowMessageParams
{
    /// The message type.
    hxLSPMessageType type = hxLSPMessageType::Info;
    /// The actual message.
    std::string message;
};
JSON_SERIALIZE(hxLSPShowMessageParams, {}, {
    FROM_KEY(type);
    FROM_KEY(message)
})

struct hxLSPRegistration
{
    /**
     * The id used to register the request. The id can be used to deregister
     * the request again.
     */
    TextType id;
    /**
     * The method / capability to register for.
     */
    TextType method;
};
JSON_SERIALIZE(hxLSPRegistration, MAP_JSON(MAP_KEY(id), MAP_KEY(method)), {})

struct hxLSPRegistrationParams
{
    std::vector<hxLSPRegistration> registrations;
};
JSON_SERIALIZE(hxLSPRegistrationParams, MAP_JSON(MAP_KEY(registrations)), {})

struct hxLSPUnregistrationParams
{
    std::vector<hxLSPRegistration> unregisterations;
};
JSON_SERIALIZE(hxLSPUnregistrationParams, MAP_JSON(MAP_KEY(unregisterations)), {})

struct hxLSPDidOpenTextDocumentParams
{
    /// The document that was opened.
    hxLSPTextDocumentItem textDocument;
};
JSON_SERIALIZE(hxLSPDidOpenTextDocumentParams, MAP_JSON(MAP_KEY(textDocument)), {})

struct hxLSPDidCloseTextDocumentParams
{
    /// The document that was closed.
    hxLSPTextDocumentIdentifier textDocument;
};
JSON_SERIALIZE(hxLSPDidCloseTextDocumentParams, MAP_JSON(MAP_KEY(textDocument)), {})

struct hxLSPTextDocumentContentChangeEvent
{
    /// The range of the document that changed.
    option<hxLSPRange> range;

    /// The length of the range that got replaced.
    option<int> rangeLength;
    /// The new text of the range/document.
    std::string text;
};
JSON_SERIALIZE(hxLSPTextDocumentContentChangeEvent, MAP_JSON(MAP_KEY(range), MAP_KEY(rangeLength), MAP_KEY(text)), {})

struct hxLSPDidChangeTextDocumentParams
{
    /// The document that did change. The version number points
    /// to the version after all provided content changes have
    /// been applied.
    hxLSPTextDocumentIdentifier textDocument;

    /// The actual content changes.
    std::vector<hxLSPTextDocumentContentChangeEvent> contentChanges;

    /// Forces diagnostics to be generated, or to not be generated, for this
    /// version of the file. If not set, diagnostics are eventually consistent:
    /// either they will be provided for this version or some subsequent one.
    /// This is a clangd extension.
    option<bool> wantDiagnostics;
};
JSON_SERIALIZE(hxLSPDidChangeTextDocumentParams,
               MAP_JSON(MAP_KEY(textDocument), MAP_KEY(contentChanges), MAP_KEY(wantDiagnostics)), {})

enum class hxLSPFileChangeType
{
    /// The file got created.
    Created = 1,
    /// The file got changed.
    Changed = 2,
    /// The file got deleted.
    Deleted = 3
};
struct hxLSPFileEvent
{
    /// The file's URI.
    URIForFile uri;
    /// The change type.
    hxLSPFileChangeType type = hxLSPFileChangeType::Created;
};
JSON_SERIALIZE(hxLSPFileEvent, MAP_JSON(MAP_KEY(uri), MAP_KEY(type)), {})

struct hxLSPDidChangeWatchedFilesParams
{
    /// The actual file events.
    std::vector<hxLSPFileEvent> changes;
};
JSON_SERIALIZE(hxLSPDidChangeWatchedFilesParams, MAP_JSON(MAP_KEY(changes)), {})

struct hxLSPDidChangeConfigurationParams
{
    hxLSPConfigurationSettings settings;
};
JSON_SERIALIZE(hxLSPDidChangeConfigurationParams, MAP_JSON(MAP_KEY(settings)), {})

struct hxLSPDocumentRangeFormattingParams
{
    /// The document to format.
    hxLSPTextDocumentIdentifier textDocument;

    /// The range to format
    hxLSPRange range;
};
JSON_SERIALIZE(hxLSPDocumentRangeFormattingParams, MAP_JSON(MAP_KEY(textDocument), MAP_KEY(range)), {})

struct hxLSPDocumentOnTypeFormattingParams
{
    /// The document to format.
    hxLSPTextDocumentIdentifier textDocument;

    /// The position at which this request was sent.
    hxLSPPosition position;

    /// The character that has been typed.
    TextType ch;
};
JSON_SERIALIZE(hxLSPDocumentOnTypeFormattingParams, MAP_JSON(MAP_KEY(textDocument), MAP_KEY(position), MAP_KEY(ch)), {})

struct hxLSPFoldingRangeParams
{
    /// The document to format.
    hxLSPTextDocumentIdentifier textDocument;
};
JSON_SERIALIZE(hxLSPFoldingRangeParams, MAP_JSON(MAP_KEY(textDocument)), {})

enum class hxLSPFoldingRangeKind
{
    Comment,
    Imports,
    Region,
};
NLOHMANN_JSON_SERIALIZE_ENUM(hxLSPFoldingRangeKind, {{hxLSPFoldingRangeKind::Comment, "comment"},
                                                {hxLSPFoldingRangeKind::Imports, "imports"},
                                                {hxLSPFoldingRangeKind::Region, "region"}})

struct hxLSPFoldingRange
{
    /**
     * The zero-based line number from where the folded range starts.
     */
    int startLine;
    /**
     * The zero-based character offset from where the folded range starts.
     * If not defined, defaults to the length of the start line.
     */
    int startCharacter;
    /**
     * The zero-based line number where the folded range ends.
     */
    int endLine;
    /**
     * The zero-based character offset before the folded range ends.
     * If not defined, defaults to the length of the end line.
     */
    int endCharacter;

    hxLSPFoldingRangeKind kind;
};
JSON_SERIALIZE(hxLSPFoldingRange, {}, {
    FROM_KEY(startLine);
    FROM_KEY(startCharacter);
    FROM_KEY(endLine);
    FROM_KEY(endCharacter);
    FROM_KEY(kind);
})

struct hxLSPSelectionRangeParams
{
    /// The document to format.
    hxLSPTextDocumentIdentifier textDocument;
    std::vector<hxLSPPosition> positions;
};
JSON_SERIALIZE(hxLSPSelectionRangeParams, MAP_JSON(MAP_KEY(textDocument), MAP_KEY(positions)), {})

struct hxLSPSelectionRange
{
    hxLSPRange range;
    std::unique_ptr<hxLSPSelectionRange> parent;
};
JSON_SERIALIZE(hxLSPSelectionRange, {}, {
    FROM_KEY(range);
    if (j.contains("parent"))
    {
        value.parent = std::make_unique<hxLSPSelectionRange>();
        j.at("parent").get_to(*value.parent);
    }
})

struct hxLSPDocumentFormattingParams
{
    /// The document to format.
    hxLSPTextDocumentIdentifier textDocument;
};
JSON_SERIALIZE(hxLSPDocumentFormattingParams, MAP_JSON(MAP_KEY(textDocument)), {})

struct hxLSPDocumentSymbolParams
{
    // The text document to find symbols in.
    hxLSPTextDocumentIdentifier textDocument;
};
JSON_SERIALIZE(hxLSPDocumentSymbolParams, MAP_JSON(MAP_KEY(textDocument)), {})

struct hxLSPDiagnosticRelatedInformation
{
    /// The location of this related diagnostic information.
    hxLSPLocation location;
    /// The message of this related diagnostic information.
    std::string message;
};
JSON_SERIALIZE(hxLSPDiagnosticRelatedInformation, MAP_JSON(MAP_KEY(location), MAP_KEY(message)), {
    FROM_KEY(location);
    FROM_KEY(message);
})
struct hxLSPCodeAction;

struct hxLSPDiagnostic
{
    /// The range at which the message applies.
    hxLSPRange range;

    /// The diagnostic's severity. Can be omitted. If omitted it is up to the
    /// client to interpret diagnostics as error, warning, info or hint.
    int severity = 0;

    /// The diagnostic's code. Can be omitted.
    std::string code;

    /// A human-readable string describing the source of this
    /// diagnostic, e.g. 'typescript' or 'super lint'.
    std::string source;

    /// The diagnostic's message.
    std::string message;

    /// An array of related diagnostic information, e.g. when symbol-names within
    /// a scope collide all definitions can be marked via this property.
    option<std::vector<hxLSPDiagnosticRelatedInformation>> relatedInformation;

    /// The diagnostic's category. Can be omitted.
    /// An LSP extension that's used to send the name of the category over to the
    /// client. The category typically describes the compilation stage during
    /// which the issue was produced, e.g. "Semantic Issue" or "Parse Issue".
    option<std::string> category;

    /// Clangd extension: code actions related to this diagnostic.
    /// Only with capability textDocument.publishDiagnostics.codeActionsInline.
    /// (These actions can also be obtained using textDocument/codeAction).
    option<std::vector<hxLSPCodeAction>> codeActions;
};
JSON_SERIALIZE(hxLSPDiagnostic,
               MAP_JSON(MAP_KEY(range), MAP_KEY(code), MAP_KEY(source), MAP_KEY(message), MAP_KEY(relatedInformation),
                        MAP_KEY(category), MAP_KEY(codeActions)),
               {
                   FROM_KEY(range);
                   FROM_KEY(code);
                   FROM_KEY(source);
                   FROM_KEY(message);
                   FROM_KEY(relatedInformation);
                   FROM_KEY(category);
                   FROM_KEY(codeActions);
               })

struct hxLSPPublishDiagnosticsParams
{
    /**
     * The URI for which diagnostic information is reported.
     */
    std::string uri;
    /**
     * An array of diagnostic information items.
     */
    std::vector<hxLSPDiagnostic> diagnostics;
};
JSON_SERIALIZE(hxLSPPublishDiagnosticsParams, {}, {
    FROM_KEY(uri);
    FROM_KEY(diagnostics);
})

struct hxLSPCodeActionContext
{
    /// An array of diagnostics.
    std::vector<hxLSPDiagnostic> diagnostics;
};
JSON_SERIALIZE(hxLSPCodeActionContext, MAP_JSON(MAP_KEY(diagnostics)), {})

struct hxLSPCodeActionParams
{
    /// The document in which the command was invoked.
    hxLSPTextDocumentIdentifier textDocument;

    /// The range for which the command was invoked.
    hxLSPRange range;

    /// Context carrying additional information.
    hxLSPCodeActionContext context;
};
JSON_SERIALIZE(hxLSPCodeActionParams, MAP_JSON(MAP_KEY(textDocument), MAP_KEY(range), MAP_KEY(context)), {})

struct hxLSPWorkspaceEdit
{
    /// Holds changes to existing resources.
    option<std::map<std::string, std::vector<hxLSPTextEdit>>> changes;

    /// Note: "documentChanges" is not currently used because currently there is
    /// no support for versioned edits.
};
JSON_SERIALIZE(hxLSPWorkspaceEdit, MAP_JSON(MAP_KEY(changes)), { FROM_KEY(changes); })

struct hxLSPTweakArgs
{
    /// A file provided by the client on a textDocument/codeAction request.
    std::string file;
    /// A selection provided by the client on a textDocument/codeAction request.
    hxLSPRange selection;
    /// ID of the tweak that should be executed. Corresponds to Tweak::id().
    std::string tweakID;
};
JSON_SERIALIZE(hxLSPTweakArgs, MAP_JSON(MAP_KEY(file), MAP_KEY(selection), MAP_KEY(tweakID)), {
    FROM_KEY(file);
    FROM_KEY(selection);
    FROM_KEY(tweakID);
})

struct hxLSPExecuteCommandParams
{
    std::string command;
    // Arguments
    option<hxLSPWorkspaceEdit> workspaceEdit;
    option<hxLSPTweakArgs> tweakArgs;
};
JSON_SERIALIZE(hxLSPExecuteCommandParams, MAP_JSON(MAP_KEY(command), MAP_KEY(workspaceEdit), MAP_KEY(tweakArgs)), {})

struct hxLSPCommand : public hxLSPExecuteCommandParams
{
    std::string title;
};
JSON_SERIALIZE(hxLSPCommand, MAP_JSON(MAP_KEY(command), MAP_KEY(workspaceEdit), MAP_KEY(tweakArgs), MAP_KEY(title)), {
    FROM_KEY(command);
    FROM_KEY(workspaceEdit);
    FROM_KEY(tweakArgs);
    FROM_KEY(title);
})

struct hxLSPCodeAction
{
    /// A short, human-readable, title for this code action.
    std::string title;

    /// The kind of the code action.
    /// Used to filter code actions.
    option<std::string> kind;
    /// The diagnostics that this code action resolves.
    option<std::vector<hxLSPDiagnostic>> diagnostics;

    /// The workspace edit this code action performs.
    option<hxLSPWorkspaceEdit> edit;

    /// A command this code action executes. If a code action provides an edit
    /// and a command, first the edit is executed and then the command.
    option<hxLSPCommand> command;
};
JSON_SERIALIZE(hxLSPCodeAction,
               MAP_JSON(MAP_KEY(title), MAP_KEY(kind), MAP_KEY(diagnostics), MAP_KEY(edit), MAP_KEY(command)), {
                   FROM_KEY(title);
                   FROM_KEY(kind);
                   FROM_KEY(diagnostics);
                   FROM_KEY(edit);
                   FROM_KEY(command)
               })

struct hxLSPSymbolInformation
{
    /// The name of this symbol.
    std::string name;
    /// The kind of this symbol.
    hxLSPSymbolKind kind = hxLSPSymbolKind::Class;
    /// The location of this symbol.
    hxLSPLocation location;
    /// The name of the symbol containing this symbol.
    std::string containerName;
};
JSON_SERIALIZE(hxLSPSymbolInformation, MAP_JSON(MAP_KEY(name), MAP_KEY(kind), MAP_KEY(location), MAP_KEY(containerName)), {
    FROM_KEY(name);
    FROM_KEY(kind);
    FROM_KEY(location);
    FROM_KEY(containerName)
})

struct hxLSPSymbolDetails
{
    TextType name;
    TextType containerName;
    /// Unified Symbol Resolution identifier
    /// This is an opaque string uniquely identifying a symbol.
    /// Unlike SymbolID, it is variable-length and somewhat human-readable.
    /// It is a common representation across several clang tools.
    /// (See USRGeneration.h)
    TextType USR;
    option<TextType> ID;
};

struct hxLSPWorkspaceSymbolParams
{
    /// A non-empty query string
    TextType query;
};
JSON_SERIALIZE(hxLSPWorkspaceSymbolParams, MAP_JSON(MAP_KEY(query)), {})

struct hxLSPApplyWorkspaceEditParams
{
    hxLSPWorkspaceEdit edit;
};
JSON_SERIALIZE(hxLSPApplyWorkspaceEditParams, MAP_JSON(MAP_KEY(edit)), {})

struct hxLSPTextDocumentPositionParams
{
    /// The text document.
    hxLSPTextDocumentIdentifier textDocument;

    /// The position inside the text document.
    hxLSPPosition position;
};
JSON_SERIALIZE(hxLSPTextDocumentPositionParams, MAP_JSON(MAP_KEY(textDocument), MAP_KEY(position)), {})

enum class hxLSPCompletionTriggerKind
{
    /// Completion was triggered by typing an identifier (24x7 code
    /// complete), manual invocation (e.g Ctrl+Space) or via API.
    Invoked = 1,
    /// Completion was triggered by a trigger character specified by
    /// the `triggerCharacters` properties of the `CompletionRegistrationOptions`.
    TriggerCharacter = 2,
    /// Completion was re-triggered as the current completion list is incomplete.
    TriggerTriggerForIncompleteCompletions = 3
};
struct hxLSPCompletionContext
{
    /// How the completion was triggered.
    hxLSPCompletionTriggerKind triggerKind = hxLSPCompletionTriggerKind::Invoked;
    /// The trigger character (a single character) that has trigger code complete.
    /// Is undefined if `triggerKind !== CompletionTriggerKind.TriggerCharacter`
    option<TextType> triggerCharacter;
};
JSON_SERIALIZE(hxLSPCompletionContext, MAP_JSON(MAP_KEY(triggerKind), MAP_KEY(triggerCharacter)), {})

struct hxLSPCompletionParams : hxLSPTextDocumentPositionParams
{
    option<hxLSPCompletionContext> context;
};
JSON_SERIALIZE(hxLSPCompletionParams, MAP_JSON(MAP_KEY(context), MAP_KEY(textDocument), MAP_KEY(position)), {})

struct hxLSPMarkupContent
{
    hxLSPMarkupKind kind = hxLSPMarkupKind::PlainText;
    std::string value;
};
JSON_SERIALIZE(hxLSPMarkupContent, {}, {
    FROM_KEY(kind);
    FROM_KEY(value)
})

struct hxLSPHover
{
    /// The hover's content
    hxLSPMarkupContent contents;

    /// An optional range is a range inside a text document
    /// that is used to visualize a hover, e.g. by changing the background color.
    option<hxLSPRange> range;
};
JSON_SERIALIZE(hxLSPHover, {}, {
    FROM_KEY(contents);
    FROM_KEY(range)
})

enum class hxLSPInsertTextFormat
{
    Missing = 0,
    /// The primary text to be inserted is treated as a plain string.
    PlainText = 1,
    /// The primary text to be inserted is treated as a snippet.
    ///
    /// A snippet can define tab stops and placeholders with `$1`, `$2`
    /// and `${3:foo}`. `$0` defines the final tab stop, it defaults to the end
    /// of the snippet. Placeholders with equal identifiers are linked, that is
    /// typing in one will update others too.
    ///
    /// See also:
    /// https//github.com/Microsoft/vscode/blob/master/src/vs/editor/contrib/snippet/common/snippet.md
    Snippet = 2,
};
struct hxLSPCompletionItem
{
    /// The label of this completion item. By default also the text that is
    /// inserted when selecting this completion.
    std::string label;

    /// The kind of this completion item. Based of the kind an icon is chosen by
    /// the editor.
    hxLSPCompletionItemKind kind = hxLSPCompletionItemKind::Missing;

    /// A human-readable string with additional information about this item, like
    /// type or symbol information.
    std::string detail;

    /// A human-readable string that represents a doc-comment.
    std::string documentation;

    /// A string that should be used when comparing this item with other items.
    /// When `falsy` the label is used.
    std::string sortText;

    /// A string that should be used when filtering a set of completion items.
    /// When `falsy` the label is used.
    std::string filterText;

    /// A string that should be inserted to a document when selecting this
    /// completion. When `falsy` the label is used.
    std::string insertText;

    /// The format of the insert text. The format applies to both the `insertText`
    /// property and the `newText` property of a provided `textEdit`.
    hxLSPInsertTextFormat insertTextFormat = hxLSPInsertTextFormat::Missing;

    /// An edit which is applied to a document when selecting this completion.
    /// When an edit is provided `insertText` is ignored.
    ///
    /// Note: The range of the edit must be a single line range and it must
    /// contain the position at which completion has been requested.
    hxLSPTextEdit textEdit;

    /// An optional array of additional text edits that are applied when selecting
    /// this completion. Edits must not overlap with the main edit nor with
    /// themselves.
    std::vector<hxLSPTextEdit> additionalTextEdits;

    /// Indicates if this item is deprecated.
    bool deprecated = false;

    // TODO(krasimir): The following optional fields defined by the language
    // server protocol are unsupported:
    //
    // data?: any - A data entry field that is preserved on a completion item
    //              between a completion and a completion resolve request.
};
JSON_SERIALIZE(hxLSPCompletionItem, {}, {
    FROM_KEY(label);
    FROM_KEY(kind);
    FROM_KEY(detail);
    FROM_KEY(documentation);
    FROM_KEY(sortText);
    FROM_KEY(filterText);
    FROM_KEY(insertText);
    FROM_KEY(insertTextFormat);
    FROM_KEY(textEdit);
    FROM_KEY(additionalTextEdits);
})

struct hxLSPCompletionList
{
    /// The list is not complete. Further typing should result in recomputing the
    /// list.
    bool isIncomplete = false;

    /// The completion items.
    std::vector<hxLSPCompletionItem> items;
};
JSON_SERIALIZE(hxLSPCompletionList, {}, {
    FROM_KEY(isIncomplete);
    FROM_KEY(items);
})

struct hxLSPParameterInformation
{

    /// The label of this parameter. Ignored when labelOffsets is set.
    std::string labelString;

    /// Inclusive start and exclusive end offsets withing the containing signature
    /// label.
    /// Offsets are computed by lspLength(), which counts UTF-16 code units by
    /// default but that can be overriden, see its documentation for details.
    option<std::pair<unsigned, unsigned>> labelOffsets;

    /// The documentation of this parameter. Optional.
    std::string documentation;
};
JSON_SERIALIZE(hxLSPParameterInformation, {}, {
    FROM_KEY(labelString);
    FROM_KEY(labelOffsets);
    FROM_KEY(documentation);
})
struct hxLSPSignatureInformation
{

    /// The label of this signature. Mandatory.
    std::string label;

    /// The documentation of this signature. Optional.
    std::string documentation;

    /// The parameters of this signature.
    std::vector<hxLSPParameterInformation> parameters;
};
JSON_SERIALIZE(hxLSPSignatureInformation, {}, {
    FROM_KEY(label);
    FROM_KEY(documentation);
    FROM_KEY(parameters);
})
struct hxLSPSignatureHelp
{
    /// The resulting signatures.
    std::vector<hxLSPSignatureInformation> signatures;
    /// The active signature.
    int activeSignature = 0;
    /// The active parameter of the active signature.
    int activeParameter = 0;
    /// Position of the start of the argument list, including opening paren. e.g.
    /// foo("first arg",   "second arg",
    ///    ^-argListStart   ^-cursor
    /// This is a clangd-specific extension, it is only available via C++ API and
    /// not currently serialized for the LSP.
    hxLSPPosition argListStart;
};
JSON_SERIALIZE(hxLSPSignatureHelp, {}, {
    FROM_KEY(signatures);
    FROM_KEY(activeParameter);
    FROM_KEY(argListStart);
})

struct hxLSPRenameParams
{
    /// The document that was opened.
    hxLSPTextDocumentIdentifier textDocument;

    /// The position at which this request was sent.
    hxLSPPosition position;

    /// The new name of the symbol.
    std::string newName;
};
JSON_SERIALIZE(hxLSPRenameParams, MAP_JSON(MAP_KEY(textDocument), MAP_KEY(position), MAP_KEY(newName)), {})

enum class hxLSPDocumentHighlightKind
{
    Text = 1,
    Read = 2,
    Write = 3
};

struct hxLSPDocumentHighlight
{
    /// The range this highlight applies to.
    hxLSPRange range;
    /// The highlight kind, default is DocumentHighlightKind.Text.
    hxLSPDocumentHighlightKind kind = hxLSPDocumentHighlightKind::Text;
    friend bool operator<(const hxLSPDocumentHighlight &LHS, const hxLSPDocumentHighlight &RHS)
    {
        int LHSKind = static_cast<int>(LHS.kind);
        int RHSKind = static_cast<int>(RHS.kind);
        return std::tie(LHS.range, LHSKind) < std::tie(RHS.range, RHSKind);
    }
    friend bool operator==(const hxLSPDocumentHighlight &LHS, const hxLSPDocumentHighlight &RHS)
    {
        return LHS.kind == RHS.kind && LHS.range == RHS.range;
    }
};
enum class hxLSPTypeHierarchyDirection
{
    Children = 0,
    Parents = 1,
    Both = 2
};

struct hxLSPTypeHierarchyParams : public hxLSPTextDocumentPositionParams
{
    /// The hierarchy levels to resolve. `0` indicates no level.
    int resolve = 0;

    /// The direction of the hierarchy levels to resolve.
    hxLSPTypeHierarchyDirection direction = hxLSPTypeHierarchyDirection::Parents;
};
JSON_SERIALIZE(hxLSPTypeHierarchyParams,
               MAP_JSON(MAP_KEY(resolve), MAP_KEY(direction), MAP_KEY(textDocument), MAP_KEY(position)), {})

struct hxLSPTypeHierarchyItem
{
    /// The human readable name of the hierarchy item.
    std::string name;

    /// Optional detail for the hierarchy item. It can be, for instance, the
    /// signature of a function or method.
    option<std::string> detail;

    /// The kind of the hierarchy item. For instance, class or interface.
    hxLSPSymbolKind kind;

    /// `true` if the hierarchy item is deprecated. Otherwise, `false`.
    bool deprecated;

    /// The URI of the text document where this type hierarchy item belongs to.
    DocumentUri uri;

    /// The range enclosing this type hierarchy item not including
    /// leading/trailing whitespace but everything else like comments. This
    /// information is typically used to determine if the client's cursor is
    /// inside the type hierarch item to reveal in the symbol in the UI.
    hxLSPRange range;

    /// The range that should be selected and revealed when this type hierarchy
    /// item is being picked, e.g. the name of a function. Must be contained by
    /// the `range`.
    hxLSPRange selectionRange;

    /// If this type hierarchy item is resolved, it contains the direct parents.
    /// Could be empty if the item does not have direct parents. If not defined,
    /// the parents have not been resolved yet.
    option<std::vector<hxLSPTypeHierarchyItem>> parents;

    /// If this type hierarchy item is resolved, it contains the direct children
    /// of the current item. Could be empty if the item does not have any
    /// descendants. If not defined, the children have not been resolved.
    option<std::vector<hxLSPTypeHierarchyItem>> children;

    /// The protocol has a slot here for an optional 'data' filed, which can
    /// be used to identify a type hierarchy item in a resolve request. We don't
    /// need this (the item itself is sufficient to identify what to resolve)
    /// so don't declare it.
};

struct hxLSPReferenceParams : public hxLSPTextDocumentPositionParams
{
    // For now, no options like context.includeDeclaration are supported.
};
JSON_SERIALIZE(hxLSPReferenceParams, MAP_JSON(MAP_KEY(textDocument), MAP_KEY(position)), {})
struct hxLSPFileStatus
{
    /// The text document's URI.
    DocumentUri uri;
    /// The human-readable string presents the current state of the file, can be
    /// shown in the UI (e.g. status bar).
    TextType state;
    // FIXME: add detail messages.
};
