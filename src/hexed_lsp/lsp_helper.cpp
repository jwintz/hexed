#include "lsp_helper.h"

hxLSPHelper::~hxLSPHelper(void)
{
    client->didClose("file://" + file.fileName().toStdString());
    client->shutdown();

    delete client;
}

void hxLSPHelper::setup(const QString& source, QQuickTextEdit *edit)
{
    this->code_d = edit->textDocument()->textDocument();

    if(!this->code_d) return;

    timer.setInterval(500);
    timer.setSingleShot(true);
    timer.start();

    if(!source.isEmpty()) {
        file.setFileName(source);
    } else {
        file.setFileName("/tmp/0");
    }

    file.open(QIODevice::ReadWrite);
    file_info.setFile(file);

    if (file_info.suffix() == "cpp")
        client = new hxLSPClient("clangd", {});
    else
        client = new hxLSPClient("pylsp", {});

    client->initialize();
    client->didOpen("file://" + file.fileName().toStdString(), "", file_info.suffix().toStdString());

// /////////////////////////////////////////////////////////////////////////////
// Position tracking
// /////////////////////////////////////////////////////////////////////////////

    connect(edit, &QQuickTextEdit::cursorPositionChanged, [=] (void) -> void
    {
        QTextBlock block = edit->textDocument()->textDocument()->findBlock(edit->cursorPosition());

          _line = block.blockNumber() + 1;
        _column = edit->cursorPosition() - block.position() + 1;

// ' '      space
// '\t'     horizontal tab
// '\n'     newline
// '\v'     vertical tab
// '\f'     form feed
// '\r'

        emit this->completions(QJsonArray());
    });

// /////////////////////////////////////////////////////////////////////////////
// Diagnostics request
// /////////////////////////////////////////////////////////////////////////////

    connect(code_d, &QTextDocument::contentsChanged, [=] (void) -> void
    {
        timer.start();

        emit this->monitoring();
    });

// /////////////////////////////////////////////////////////////////////////////
// Completion
// /////////////////////////////////////////////////////////////////////////////

    connect(code_d, &QTextDocument::contentsChange, [=] (int position, int charsRemoved, int charsAdded) -> void
    {
        if(charsRemoved)
            emit this->completions(QJsonArray());
        else if(charsAdded)
            client->completion("file://" + file.fileName().toStdString(), { _line - 1 , _column - 1 }, option<hxLSPCompletionContext>(hxLSPCompletionContext {}));

        hxLSPTextDocumentContentChangeEvent ev;
        ev.text = code_d->toPlainText().toStdString();

        std::vector<hxLSPTextDocumentContentChangeEvent> ch;
        ch.push_back(ev);

        DocumentUri uri = "file://" + file.fileName().toStdString();

        client->didChange(uri, ch, false);
    });

// /////////////////////////////////////////////////////////////////////////////

    connect(client, &hxLSPClient::onError, [=] (QJsonObject id, QJsonObject err)
    {
        // qDebug() << id << err << Q_FUNC_INFO << 1;
    });

    connect(client, &hxLSPClient::onNotify, [=] (QString method, QJsonObject param)
    {
        // qDebug() << method << param << Q_FUNC_INFO << 2;

        QJsonDocument document(param);

        // outp_d->setPlainText(document.toJson(QJsonDocument::Indented));

        QJsonArray errors;

        for(int i = 0; i < param["diagnostics"].toArray().count(); i++) {

            errors.append(QJsonObject
            {
            { "line", param["diagnostics"].toArray().at(i)["range"]["start"]["line"].toInt() + 1 },
            { "message", param["diagnostics"].toArray().at(i)["message"].toString() },
            { "severity", param["diagnostics"].toArray().at(i)["severity"].toInt() },
            { "source", param["diagnostics"].toArray().at(i)["source"].toString() }
            });
        }

        emit this->diagnostics(errors);
    });

    connect(client, &hxLSPClient::onRequest, [=] (QString method, QJsonObject param, QJsonObject id)
    {
        // qDebug() << method << param << id << Q_FUNC_INFO << 3;
    });

    connect(client, &hxLSPClient::onResponse, [=] (QJsonObject id, QJsonObject response)
    {
        // qDebug() << id << response << Q_FUNC_INFO << 4;

        QJsonArray candidates;

        for(int i = 0; i < response["items"].toArray().count(); i++) {

            candidates.append(QJsonObject
            {
            { "candidate", response["items"].toArray().at(i)["insertText"].toString() },
            { "label", response["items"].toArray().at(i)["label"].toString() },
            { "line", response["items"].toArray().at(i)["textEdit"]["range"]["end"]["line"].toInt() + 1 },
            });
        }

        emit this->completions(candidates);

    });

    connect(client, &hxLSPClient::onServerError, [=] (QProcess::ProcessError err)
    {
        // qDebug() << Q_FUNC_INFO << 4;
    });

    connect(client, &hxLSPClient::onServerFinished, [=] (int exitCode, QProcess::ExitStatus status)
    {
        // qDebug() << Q_FUNC_INFO << 5;
    });

    connect(&timer, &QTimer::timeout, this, [=] (void) -> void
    {
        hxLSPTextDocumentContentChangeEvent ev;
        ev.text = code_d->toPlainText().toStdString();

        std::vector<hxLSPTextDocumentContentChangeEvent> ch;
        ch.push_back(ev);

        client->didChange("file://" + file.fileName().toStdString(), ch, true);
    });
}

QString hxLSPHelper::server(void)
{
    if(file_info.suffix() == "cpp")
        return "clangd";

    return "pylsp";
}
