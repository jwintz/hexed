#pragma once

#include <QObject>
#include <QRect>
#include <QRgb>
#include <QVector>

#include "term_ptyiface.h"

struct hxTermChar
{
    // TODO: Replace with the version in Parser.
    enum TextAttributes
    {
        NoAttributes = 0x00,
        BoldAttribute = 0x01,
        ItalicAttribute = 0x02,
        UnderlineAttribute = 0x04,
        NegativeAttribute = 0x08,
        BlinkAttribute = 0x10
    };

    QChar c;
    QRgb fgColor;
    QRgb bgColor;
    TextAttributes attrib;
};
inline hxTermChar::TextAttributes operator~(hxTermChar::TextAttributes a) { return (hxTermChar::TextAttributes) ~(int)a; }
inline hxTermChar::TextAttributes operator|(hxTermChar::TextAttributes a, hxTermChar::TextAttributes b) { return (hxTermChar::TextAttributes)((int)a | (int)b); }
inline hxTermChar::TextAttributes operator&(hxTermChar::TextAttributes a, hxTermChar::TextAttributes b) { return (hxTermChar::TextAttributes)((int)a & (int)b); }
inline hxTermChar::TextAttributes operator^(hxTermChar::TextAttributes a, hxTermChar::TextAttributes b) { return (hxTermChar::TextAttributes)((int)a ^ (int)b); }
inline hxTermChar::TextAttributes& operator|=(hxTermChar::TextAttributes& a, hxTermChar::TextAttributes b) { return (hxTermChar::TextAttributes&)((int&)a |= (int)b); }
inline hxTermChar::TextAttributes& operator&=(hxTermChar::TextAttributes& a, hxTermChar::TextAttributes b) { return (hxTermChar::TextAttributes&)((int&)a &= (int)b); }
inline hxTermChar::TextAttributes& operator^=(hxTermChar::TextAttributes& a, hxTermChar::TextAttributes b) { return (hxTermChar::TextAttributes&)((int&)a ^= (int)b); }

const QByteArray multiCharEscapes("().*+-/%#");

struct hxTermAttribs
{
    QPoint cursorPos;

    bool wrapAroundMode;
    bool originMode;

    QRgb currentFgColor;
    QRgb currentBgColor;
    hxTermChar::TextAttributes currentAttrib;
};

class hxTerminalLine
{
public:
    int size() const { return m_contents.size(); }
    void append(const hxTermChar& tc) { m_contents.append(tc); }
    void insert(int pos, const hxTermChar& tc) { m_contents.insert(pos, tc); }
    void removeAt(int pos) { m_contents.removeAt(pos); }
    void clear() { m_contents.clear(); }
    hxTermChar& operator[](int pos) { return m_contents[pos]; }
    const hxTermChar& operator[](int pos) const { return m_contents[pos]; }
    const hxTermChar& at(int pos) const { return m_contents.at(pos); }

private:
    QVector<hxTermChar> m_contents;
};

class hxTerminalBuffer
{
public:
    int size() const { return m_buffer.size(); }
    void append(const hxTerminalLine& l) { m_buffer.append(l); }
    void insert(int pos, const hxTerminalLine& l) { m_buffer.insert(pos, l); }
    void removeAt(int pos) { m_buffer.removeAt(pos); }
    hxTerminalLine takeAt(int pos) { return m_buffer.takeAt(pos); }
    void clear() { m_buffer.clear(); }
    hxTerminalLine& operator[](int pos) { return m_buffer[pos]; }
    const hxTerminalLine& operator[](int pos) const { return m_buffer[pos]; }
    const hxTerminalLine& at(int pos) const { return m_buffer.at(pos); }

private:
    QVector<hxTerminalLine> m_buffer;
};

class hxTerminal : public QObject
{
    Q_OBJECT

public:
    explicit hxTerminal(QObject* parent = 0);
    virtual ~hxTerminal() { }

    void init();

    QPoint cursorPos();
    void setCursorPos(QPoint pos);
    bool showCursor();

    QSize termSize() const { return iTermSize; }
    void setTermSize(QSize size);

    hxTerminalBuffer& buffer();
    const hxTerminalBuffer& buffer() const;
    hxTerminalBuffer& backBuffer() { return iBackBuffer; }
    const hxTerminalBuffer& backBuffer() const { return iBackBuffer; }

    hxTerminalLine& currentLine();

    bool inverseVideoMode() const { return m_inverseVideoMode; }

    void keyPress(int key, int modifiers, const QString& text = "");
    const QStringList printableLinesFromCursor(int lines);
    void putString(QString str);
    void paste(const QString& text);
    const QStringList grabURLsFromBuffer();

    void scrollBackBufferFwd(int lines);
    void scrollBackBufferBack(int lines);
    int backBufferScrollPos() const { return iBackBufferScrollPos; }
    void resetBackBufferScrollPos();

    QString selectedText() const;
    void setSelection(QPoint start, QPoint end, bool selectionOngoing);
    QRect selection() const;
    void clearSelection();

    int rows() const;
    int columns() const;

    bool useAltScreenBuffer() const { return iUseAltScreenBuffer; }

    hxTermChar zeroChar;

signals:
    void cursorPosChanged(QPoint newPos);
    void termSizeChanged(int rows, int columns);
    void displayBufferChanged();
    void selectionChanged();
    void scrollBackBufferAdjusted(bool reset);
    void selectionFinished();
    void visualBell();
    void windowTitleChanged(const QString& windowTitle);
    void workingDirectoryChanged(const QString& workingDirectory);
    void hangupReceived();

protected:
    void timerEvent(QTimerEvent*) override;
    void insertInBuffer(const QString& chars);

private slots:
    void onDataAvailable();

private:
    Q_DISABLE_COPY(hxTerminal)

    void insertAtCursor(QChar c, bool overwriteMode = true, bool advanceCursor = true);
    void eraseLineAtCursor(int from = -1, int to = -1);
    void clearAll(bool wholeBuffer = false);
    void ansiSequence(const QString& seq);
    void handleMode(int mode, bool set, const QString& extra);
    bool handleIL(const QList<int>& params, const QString& extra);
    bool handleDL(const QList<int>& params, const QString& extra);
    bool handleDCH(const QList<int>& params, const QString& extra);
    bool handleICH(const QList<int>& params, const QString& extra);
    bool handleDECSED(const QList<int>& params, const QString& extra);
    bool handleEL(const QList<int>& params, const QString& extra);
    bool handleECH(const QList<int>& params, const QString& extra);
    void oscSequence(const QString& seq);
    void escControlChar(const QString& seq);
    void trimBackBuffer();
    void scrollBack(int lines, int insertAt = -1);
    void scrollFwd(int lines, int removeAt = -1);

    enum class ResetMode
    {
        Soft,
        Hard,
    };
    void resetTerminal(ResetMode);
    void resetTabs();
    void adjustSelectionPosition(int lines);
    void forwardTab();
    void backwardTab();

    // ### consider making this not a pointer
    hxPtyIFace* m_pty;

    hxTerminalBuffer iBuffer;
    hxTerminalBuffer iAltBuffer;
    hxTerminalBuffer iBackBuffer;
    QVector<QVector<int>> iTabStops;

    QSize iTermSize;
    bool iEmitCursorChangeSignal;

    bool iShowCursor;
    bool iUseAltScreenBuffer;
    bool iAppCursorKeys;
    bool iReplaceMode;
    bool iNewLineMode;
    bool m_inverseVideoMode;
    bool m_bracketedPasteMode;

    int iMarginTop;
    int iMarginBottom;

    int iBackBufferScrollPos;

    hxTermAttribs iTermAttribs;
    hxTermAttribs iTermAttribs_saved;
    hxTermAttribs iTermAttribs_saved_alt;

    QString escSeq;
    QString oscSeq;
    int escape;
    QRect iSelection;
    QVector<QRgb> iColorTable;
    int m_dispatch_timer;

    friend class hxTermRender;
};
