#include <QClipboard>
#include <QDebug>
#include <QFontDatabase>
#include <QFontInfo>
#include <QGuiApplication>
#include <QQuickView>

#include "term_terminal.h"
#include "term_textrender.h"
#include "term_utilities.h"

#ifdef HAVE_FEEDBACK
#    include <QFeedbackEffect>
#endif

static hxTermUtil* s_instance;

hxTermUtil::hxTermUtil(const QString& settingsFile, QObject* parent)
    : QObject(parent)
    , m_settings(settingsFile, QSettings::IniFormat)
    , iWindow(0)
{
    Q_ASSERT(s_instance == nullptr);
    s_instance = this;
}

hxTermUtil::~hxTermUtil()
{
    s_instance = nullptr;
}

hxTermUtil* hxTermUtil::instance()
{
    return s_instance;
}

QString hxTermUtil::panLeftTitle() const
{
    return settingsValue("gestures/panLeftTitle", "Alt-Right").toString();
}

QString hxTermUtil::panLeftCommand() const
{
    return settingsValue("gestures/panLeftCommand", "\\e\\e[C").toString();
}

QString hxTermUtil::panRightTitle() const
{
    return settingsValue("gestures/panRightTitle", "Alt-Left").toString();
}

QString hxTermUtil::panRightCommand() const
{
    return settingsValue("gestures/panRightCommand", "\\e\\e[D").toString();
}

QString hxTermUtil::panDownTitle() const
{
    return settingsValue("gestures/panDownTitle", "Page Up").toString();
}

QString hxTermUtil::panDownCommand() const
{
    return settingsValue("gestures/panDownCommand", "\\e[5~").toString();
}

QString hxTermUtil::panUpTitle() const
{
    return settingsValue("gestures/panUpTitle", "Page Down").toString();
}

QString hxTermUtil::panUpCommand() const
{
    return settingsValue("gestures/panUpCommand", "\\e[6~").toString();
}

QString hxTermUtil::startupErrorMessage() const
{
    return m_startupErrorMessage;
}

void hxTermUtil::setStartupErrorMessage(const QString& message)
{
    Q_ASSERT(m_startupErrorMessage.isNull()); // CONSTANT
    m_startupErrorMessage = message;
}

QByteArray hxTermUtil::terminalEmulator() const
{
    return m_settings.value("terminal/envVarTERM", "xterm-256color").toByteArray();
}

QString hxTermUtil::terminalCommand() const
{
    return m_settings.value("general/execCmd").toString();
}

int hxTermUtil::terminalScrollbackSize() const
{
    return m_settings.value("terminal/scrollbackLineLimit", "3000").toInt();
}

//void hxTermUtil::setWindow(QQuickView* win)
//{
//    if (iWindow)
//        qFatal("Should set window property only once");
//    iWindow = win;
//    if (!iWindow)
//        qFatal("invalid main window");
//    connect(win, SIGNAL(contentOrientationChanged(Qt::ScreenOrientation)), this, SIGNAL(windowOrientationChanged()));
//    connect(win, SIGNAL(windowTitleChanged(QString)), this, SIGNAL(windowTitleChanged()));
//}
//
//void hxTermUtil::setWindowTitle(QString title)
//{
//    iWindow->setTitle(title);
//    emit windowTitleChanged();
//}
//
//QString hxTermUtil::windowTitle()
//{
//    return iWindow->title();
//}
//
//int hxTermUtil::windowOrientation()
//{
//    return iWindow->contentOrientation();
//}
//
//void hxTermUtil::setWindowOrientation(int orientation)
//{
//    iWindow->reportContentOrientationChange(static_cast<Qt::ScreenOrientation>(orientation));
//}
//
// void hxTermUtil::openNewWindow()
// {
//     QProcess::startDetached("/usr/bin/literm", QStringList());
// }

QString hxTermUtil::getUserMenuXml()
{
    QString ret;
    QFile f(configPath() + "/menu.xml");
    if (f.open(QIODevice::ReadOnly | QIODevice::Text)) {
        ret = f.readAll();
        f.close();
    }

    return ret; 
}

QString hxTermUtil::configPath()
{
    QFileInfo f(m_settings.fileName());
    return f.path();
}

QVariant hxTermUtil::settingsValue(QString key, const QVariant& defaultValue) const
{
    return m_settings.value(key, defaultValue);
}

void hxTermUtil::setSettingsValue(QString key, QVariant value)
{
    m_settings.setValue(key, value);
}

QString hxTermUtil::versionString()
{
    return "internal";
}

int hxTermUtil::uiFontSize()
{
    return 12;
}

int hxTermUtil::fontSize()
{
#if defined(Q_OS_MAC)
    return settingsValue("ui/fontSize", 11).toInt();
#endif

    return settingsValue("ui/fontSize", 11).toInt();
}

void hxTermUtil::setFontSize(int size)
{
    if (size == fontSize()) {
        return;
    }

    setSettingsValue("ui/fontSize", size);
    emit fontSizeChanged();
}

void hxTermUtil::keyPressFeedback()
{
    if (!settingsValue("ui/keyPressFeedback", true).toBool())
        return;

#ifdef HAVE_FEEDBACK
    QFeedbackEffect::playThemeEffect(QFeedbackEffect::PressWeak);
#endif
}

void hxTermUtil::keyReleaseFeedback()
{
    if (!settingsValue("ui/keyPressFeedback", true).toBool())
        return;

        // TODO: check what's more comfortable, only press, or press and release
#ifdef HAVE_FEEDBACK
    QFeedbackEffect::playThemeEffect(QFeedbackEffect::ReleaseWeak);
#endif
}

bool hxTermUtil::visualBellEnabled() const
{
    return settingsValue("general/visualBell", true).toBool();
}

int hxTermUtil::cursorAnimationStartPauseDuration() const
{
    return settingsValue("cursor/animation/startPauseDuration", 500).toInt();
}

int hxTermUtil::cursorAnimationFadeInDuration() const
{
    return settingsValue("cursor/animation/fadeInDuration", 0).toInt();
}

int hxTermUtil::cursorAnimationMiddlePauseDuration() const
{
    return settingsValue("cursor/animation/middlePauseDuration", 500).toInt();
}

int hxTermUtil::cursorAnimationFadeOutDuration() const
{
    return settingsValue("cursor/animation/fadeOutDuration", 0).toInt();
}

int hxTermUtil::cursorAnimationEndPauseDuration() const
{
    return settingsValue("cursor/animation/endPauseDuration", 0).toInt();
}

QString hxTermUtil::fontFamily()
{
    // QFont defaultFont(QFontDatabase::systemFont(QFontDatabase::FixedFont));
    // QFontInfo fi(defaultFont);
    return settingsValue("ui/fontFamily", "Operator Mono").toString();
}

hxTermRender::DragMode hxTermUtil::dragMode()
{
// #if defined(MOBILE_BUILD)
//     QString defaultDragMode("scroll");
// #elif defined(DESKTOP_BUILD) || defined(TEST_MODE)
    QString defaultDragMode("select");
// #else
// #    error Unknown default dragMode
// #endif
    QString mode = settingsValue("ui/dragMode", defaultDragMode).toString();

    if (mode == "gestures") {
        return hxTermRender::DragGestures;
    } else if (mode == "scroll") {
        return hxTermRender::DragScroll;
    } else if (mode == "select") {
        return hxTermRender::DragSelect;
    } else {
        return hxTermRender::DragOff;
    }
}

void hxTermUtil::setDragMode(hxTermRender::DragMode mode)
{
    if (mode == dragMode()) {
        return;
    }

    QString modeString;
    switch (mode) {
    case hxTermRender::DragGestures:
        modeString = "gestures";
        break;
    case hxTermRender::DragScroll:
        modeString = "scroll";
        break;
    case hxTermRender::DragSelect:
        modeString = "select";
        break;
    case hxTermRender::DragOff:
    default:
        modeString = "off";
    }

    setSettingsValue("ui/dragMode", modeString);
    emit dragModeChanged();
}

int hxTermUtil::keyboardMode()
{
    QString mode = settingsValue("ui/vkbShowMethod", "move").toString();

    if (mode == "fade") {
        return KeyboardFade;
    } else if (mode == "move") {
        return KeyboardMove;
    } else {
        return KeyboardOff;
    }
}

void hxTermUtil::setKeyboardMode(int mode)
{
    if (mode == keyboardMode()) {
        return;
    }

    QString modeString;
    switch (mode) {
    case KeyboardFade:
        modeString = "fade";
        break;
    case KeyboardMove:
        modeString = "move";
        break;
    case KeyboardOff:
    default:
        modeString = "off";
    }

    setSettingsValue("ui/vkbShowMethod", modeString);
    emit keyboardModeChanged();
}

int hxTermUtil::keyboardFadeOutDelay()
{
    return settingsValue("ui/keyboardFadeOutDelay", 4000).toInt();
}

void hxTermUtil::setKeyboardFadeOutDelay(int delay)
{
    if (delay == keyboardFadeOutDelay()) {
        return;
    }

    setSettingsValue("ui/keyboardFadeOutDelay", delay);
    emit keyboardFadeOutDelayChanged();
}

QString hxTermUtil::keyboardLayout()
{
    return settingsValue("ui/keyboardLayout", "english").toString();
}

void hxTermUtil::setKeyboardLayout(const QString& layout)
{
    if (layout == keyboardLayout()) {
        return;
    }

    setSettingsValue("ui/keyboardLayout", layout);
    emit keyboardLayoutChanged();
}

int hxTermUtil::extraLinesFromCursor()
{
    return settingsValue("ui/showExtraLinesFromCursor", 1).toInt();
}

QString hxTermUtil::charset()
{
    return settingsValue("terminal/charset", "UTF-8").toString();
}

int hxTermUtil::keyboardMargins()
{
    return settingsValue("ui/keyboardMargins", 10).toInt();
}

int hxTermUtil::orientationMode()
{
    QString mode = settingsValue("ui/orientationLockMode", "auto").toString();

    if (mode == "auto") {
        return OrientationAuto;
    } else if (mode == "landscape") {
        return OrientationLandscape;
    } else {
        return OrientationPortrait;
    }
}

void hxTermUtil::setOrientationMode(int mode)
{
    if (mode == orientationMode()) {
        return;
    }

    QString modeString;
    switch (mode) {
    case OrientationAuto:
        modeString = "auto";
        break;
    case OrientationLandscape:
        modeString = "landscape";
        break;
    case OrientationPortrait:
    default:
        modeString = "portrait";
    }

    setSettingsValue("ui/orientationLockMode", modeString);
    emit orientationModeChanged();
}

void hxTermUtil::notifyText(QString text)
{
    emit notify(text);
}

void hxTermUtil::fakeKeyPress(int key, int modifiers)
{
    QKeyEvent pev(QEvent::KeyPress, key, Qt::KeyboardModifiers(modifiers));
    QCoreApplication::sendEvent(iWindow, &pev);
    QKeyEvent rev(QEvent::KeyRelease, key, Qt::KeyboardModifiers(modifiers));
    QCoreApplication::sendEvent(iWindow, &rev);
}

void hxTermUtil::copyTextToClipboard(QString str)
{
    QClipboard* cb = QGuiApplication::clipboard();

    cb->clear();
    cb->setText(str);
}
