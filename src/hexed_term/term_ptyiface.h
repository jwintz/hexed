#pragma once

#include <QByteArray>
#include <QObject>
#include <QSize>
#include <QSocketNotifier>
#include <QTextCodec>

class hxTerminal;

class hxPtyIFace : public QObject
{
    Q_OBJECT
public:
    explicit hxPtyIFace(hxTerminal* term, const QString& charset, const QByteArray& terminalEnv, const QString& commandOverride, QObject* parent);
    virtual ~hxPtyIFace();

    void writeTerm(const QString& chars);
    bool failed() { return iFailed; }

    QString takeData()
    {
        QString tmp = m_pendingData;
        m_pendingData = QString();
        return tmp;
    }

private slots:
    void resize(int rows, int columns);
    void readActivated();

signals:
    void dataAvailable();
    void hangupReceived();

private slots:
    void checkForDeadPids();

private:
    Q_DISABLE_COPY(hxPtyIFace)

    void writeTerm(const QByteArray& chars);

    hxTerminal* iTerm;
    int iPid;
    int iMasterFd;
    bool iFailed;
    bool m_childProcessQuit;
    int m_childProcessPid;

    QSocketNotifier* iReadNotifier;

    QTextCodec* iTextCodec;

    QString m_pendingData;

    static void sighandler(int sig);
    static std::vector<int> m_deadPids;
    static bool m_initializedSignalHandler;
};
