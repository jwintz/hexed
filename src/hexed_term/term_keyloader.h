#pragma once

#include <QtCore>

class hxTermUtil;

struct hxKeyData
{
    QString label;
    int code;
    QString label_alt;
    int code_alt;
    int width;
    bool isModifier;
};

class hxKeyLoader : public QObject
{
    Q_OBJECT
public:
    explicit hxKeyLoader(QObject* parent = 0);
    virtual ~hxKeyLoader();

    void setUtil(hxTermUtil* util) { iUtil = util; }

    Q_INVOKABLE bool loadLayout(QString layout);

    Q_INVOKABLE int vkbRows() { return iVkbRows; }
    Q_INVOKABLE int vkbColumns() { return iVkbColumns; }
    Q_INVOKABLE QVariantList keyAt(int row, int col);
    Q_INVOKABLE const QStringList availableLayouts();

signals:

public slots:

private:
    Q_DISABLE_COPY(hxKeyLoader)
    bool loadLayoutInternal(QIODevice& from);
    void cleanUpKey(hxKeyData& key);

    int iVkbRows;
    int iVkbColumns;

    QList<QList<hxKeyData>> iKeyData;

    hxTermUtil* iUtil;
};
