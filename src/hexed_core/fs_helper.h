#pragma once

#include <QtCore>

class hxFSHelper : public QObject
{
    Q_OBJECT
    
public:
    hxFSHelper(QObject *parent = nullptr);
   ~hxFSHelper(void);

public slots:
    QString homePath(void);
    
public:
    Q_INVOKABLE bool exists(const QString&);
    
public:
    Q_INVOKABLE QString    fileName(const QString&);
    Q_INVOKABLE QString projectName(const QString&);
    
    Q_INVOKABLE QString projectPath(const QString&);
    
    Q_INVOKABLE QString size(const QString&);
};
