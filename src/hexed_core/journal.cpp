#include "journal.h"
#include "journal_item.h"

class hxJournalPrivate
{
public:
    int infoCount = 0;
    int debugCount = 0;
    int errorCount = 0;
    int warningCount = 0;

public:
    QList<hxJournalItem *> items;
};

hxJournal *hxJournal::instance(void)
{
    if(!s_instance)
        s_instance = new hxJournal;

    return s_instance;
}

int hxJournal::infoCount(void)
{
    return d->infoCount;
}

int hxJournal::debugCount(void)
{
    return d->debugCount;
}

int hxJournal::errorCount(void)
{
    return d->errorCount;
}

int hxJournal::warningCount(void)
{
    return d->warningCount;
}

void hxJournal::clear(void)
{
    d->items.clear();

    d->infoCount = 0;
    d->debugCount = 0;
    d->warningCount = 0;
    d->errorCount = 0;

    emit itemsChanged();

    emit infoCountChanged();
    emit debugCountChanged();
    emit errorCountChanged();
    emit warningCountChanged();
}

void hxJournal::addInfoMsg(const QString& msg)
{
    d->items.append(new hxJournalItem(QString(msg).remove('\n'), hxJournalItem::Info));
    d->infoCount++;
    
    emit infoCountChanged();
    emit itemsChanged();
}

void hxJournal::addDebugMsg(const QString& msg)
{
    d->items.append(new hxJournalItem(QString(msg).remove('\n'), hxJournalItem::Debug));
    d->debugCount++;
    
    emit debugCountChanged();
    emit itemsChanged();
}

void hxJournal::addWarningMsg(const QString& msg)
{
    d->items.append(new hxJournalItem(QString(msg).remove('\n'), hxJournalItem::Warning));
    d->warningCount++;

    emit warningCountChanged();
    emit itemsChanged();
}

void hxJournal::addErrorMsg(const QString& msg)
{
    d->items.append(new hxJournalItem(QString(msg).remove('\n'), hxJournalItem::Error));
    d->errorCount++;

    emit errorCountChanged();
    emit itemsChanged();
}

QQmlListProperty<hxJournalItem> hxJournal::items(void)
{
    return QQmlListProperty<hxJournalItem>(
        this, nullptr,
        &hxJournal::itemCount,
        &hxJournal::itemAt);
}

qsizetype hxJournal::itemCount(QQmlListProperty<hxJournalItem> *list)
{
    auto parent = qobject_cast<hxJournal *>(list->object);

    if (parent)
        return parent->d->items.count();

    return 0;
}

hxJournalItem *hxJournal::itemAt(QQmlListProperty<hxJournalItem> *list, qsizetype i)
{
    auto parent = qobject_cast<hxJournal *>(list->object);

    if (parent)
        return parent->d->items.at(i);

    return 0;
}

hxJournal::hxJournal(void)
{
    d = new hxJournalPrivate;
}

hxJournal::~hxJournal(void)
{
    delete d;
}

hxJournal* hxJournal::s_instance = nullptr;
