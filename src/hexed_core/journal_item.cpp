#include "journal_item.h"

class hxJournalItemPrivate
{
public:
    QString toString(hxJournalItem::Type);

public:
    hxJournalItem::Type type;
    QString description;
    QString label;
    QString time;
};

hxJournalItem::hxJournalItem(const QString& message, Type type) : QObject()
{
    d = new hxJournalItemPrivate;

    d->type = type;
    d->description = message;
    d->time = QDateTime::currentDateTime().toString("HH:mm:ss");
    d->label = d->toString(type);
}

hxJournalItem::~hxJournalItem(void)
{
    delete d;
}

QString hxJournalItem::description(void)
{
    return d->description;
}

hxJournalItem::Type hxJournalItem::type(void)
{
    return d->type;
}

QString hxJournalItem::label(void)
{
    return d->label;
}

QString hxJournalItem::time(void)
{
    return d->time;
}

QString hxJournalItemPrivate::toString(hxJournalItem::Type type)
{
    switch (type) {
        case hxJournalItem::Info: return "Info";
        case hxJournalItem::Debug: return "Debug";
        case hxJournalItem::Warning: return "Warning";
        case hxJournalItem::Error: return "Error";
    }

    return QString("Error");
}
