#include "completer_fs.h"
#include "matcher.h"

#include <QtGui>

class hxCompleterFSPrivate
{
public:
    QDir dir;
};

hxCompleterFS::hxCompleterFS(QObject *parent) : hxCompleter(parent)
{
    d = new hxCompleterFSPrivate;
}

hxCompleterFS::~hxCompleterFS(void)
{
    delete d;
}

QString hxCompleterFS::apply(const QString& path, const QString& candidate)
{
    QFileInfo info(path);
    
    QString applied;
    
    if (info.isDir()) {
        applied = path;
    } else {
        QStringList segments = path.split("/");
        QString current = segments.takeLast();
        QString enclosing = segments.join("/");
        applied = enclosing;
    }
    
    if(!applied.endsWith("/"))
        applied += "/";
    
    applied += candidate;
    
    info.setFile(applied);
    
    if (info.isDir() && !applied.endsWith("/"))
        applied += "/";
    
    return applied;
}

QVariantMap hxCompleterFS::complete(const QString& path)
{
    QFileInfo info(path);
    
    if (info.isDir()) {
        d->dir.setPath(path);
        
        QVariantMap matches;
        matches.insert("results", QVariant::fromValue(d->dir.entryList(QDir::AllEntries | QDir::NoDotAndDotDot, QDir::Name | QDir::DirsFirst)));
        matches.insert("starts",  QVariant::fromValue(QStringList()));
        matches.insert("lengths", QVariant::fromValue(QStringList()));
        
        return matches;
    }
    
    QStringList segments = path.split("/");
    
    QString current = segments.takeLast();
    
    QString enclosing = segments.join("/");

    d->dir.setPath(enclosing);
    
    QStringList candidates = d->dir.entryList(QDir::AllEntries | QDir::NoDotAndDotDot, QDir::Name | QDir::DirsFirst);

    hxMatcher::MatchFilterResult match = hxMatcher::match(current, candidates, Qt::CaseInsensitive);
        
    QVariantMap matches;
    matches.insert("results", QVariant::fromValue(match.results));
    matches.insert("starts",  QVariant::fromValue(match.starts));
    matches.insert("lengths", QVariant::fromValue(match.lengths));

    return matches;
}
