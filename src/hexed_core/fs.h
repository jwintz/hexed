#pragma once

#include <QtCore>

class hxFS : public QObject
{
    Q_OBJECT
    
public:
    hxFS(QObject *parent = nullptr);
   ~hxFS(void);

public:
    Q_INVOKABLE QString read(const QString&);
    Q_INVOKABLE   void write(const QString&, const QString&);
};
