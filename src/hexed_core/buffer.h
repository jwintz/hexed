#pragma once

#include <QtCore>
#include <QtGui>
#include <QtQml>
#include <QtQuick>
#include <QtQuickControls2>

#include <private/qquicktextedit_p.h>

class hxBuffer : public QObject
{
    Q_OBJECT

    Q_PROPERTY(int line READ line WRITE setLine NOTIFY lineChanged)
    Q_PROPERTY(int column READ column NOTIFY columnChanged)

    Q_PROPERTY(int selectedLineCount READ selectedLineCount NOTIFY selectedLineCountChanged)
    Q_PROPERTY(int selectedCharCount READ selectedCharCount NOTIFY selectedCharCountChanged)
    
public:
     hxBuffer(QObject *parent = nullptr);
    ~hxBuffer(void);

signals:
    void   lineChanged(void);
    void columnChanged(void);

    void selectedCharCountChanged(void);
    void selectedLineCountChanged(void);
    
public:
    int   line(void) { return _line; }
    int column(void) { return _column; }

    int selectedCharCount(void) { return _selected_char_count; }
    int selectedLineCount(void) { return _selected_line_count; }
    
public:
    void setLine(int line)
    {
        QTextCursor cursor(text_doct->textDocument()->findBlockByLineNumber(line - 1));
        
        text_edit->setCursorPosition(cursor.position());
    }
    
public:
    Q_INVOKABLE void eradicate(void)
    {
        this->deleteLater();
    }
    
    Q_INVOKABLE int wordCount(const QString& text)
    {
        if(text.trimmed().isEmpty())
            return 0;

        return text.trimmed().split(QRegularExpression("\\s+")).count();
    }

public:
    Q_INVOKABLE void setup(QQuickTextEdit *edit)
    {
        this->setParent(edit);
        
        this->text_edit = edit;
        this->text_doct = edit->textDocument();
        
        connect(edit, &QQuickTextEdit::cursorPositionChanged, [=] (void) -> void
        {
            QTextBlock block = edit->textDocument()->textDocument()->findBlock(edit->cursorPosition());

              _line = block.blockNumber() + 1;
            _column = edit->cursorPosition() - block.position() + 1;

            emit   lineChanged();
            emit columnChanged();
        });
        
        connect(edit, &QQuickTextEdit::selectedTextChanged, [=] (void) -> void
        {
            QString selection = edit->selectedText();

            _selected_char_count = selection.length();
            _selected_line_count = selection.split(QRegularExpression("[\r\n]"), Qt::KeepEmptyParts).count();

            emit selectedCharCountChanged();
            emit selectedLineCountChanged();
        });
    }

private:
    int _line { 1 };
    int _column { 1 };

    int _selected_char_count { 0 };
    int _selected_line_count { 0 };
    
private:
    QQuickTextEdit     *text_edit = 0;
    QQuickTextDocument *text_doct = 0;
};

