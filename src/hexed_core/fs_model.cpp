#include "fs_model.h"

// ////////////////////////////////////////////////////////////////////////////
// Working, suboptimal solution
// ////////////////////////////////////////////////////////////////////////////

hxFSModel::hxFSModel(QObject *parent) : hxTreeModel(parent)
{
    connect(&watcher, &QFileSystemWatcher::fileChanged, [=] (const QString &) {
        this->setRootPath(this->root_path);
    });

    connect(&watcher, &QFileSystemWatcher::directoryChanged, [=] (const QString &) {
        this->setRootPath(this->root_path);
    });
}

hxFSModel::~hxFSModel(void)
{
    
}

void hxFSModel::setRootPath(const QString& root_path)
{
    if (root_path == this->root_path)
        return;
    
    std::function<void (const QString&, hxTreeItem *)> recurse;
    
    recurse = [&] (const QString& path, hxTreeItem *parent) {
        
        QDir dir(path);
        
        QFileInfoList info_list = dir.entryInfoList(QDir::Dirs | QDir::Files | QDir::NoSymLinks | QDir::NoDotAndDotDot, QDir::Name | QDir::DirsFirst);
        
        foreach(QFileInfo info, info_list) {
        
            hxTreeItem *item = new hxTreeItem(info.fileName());
            
            if (info.isDir())
                recurse(info.absoluteFilePath(), item);
            
            this->addItem(parent, item);
        }
    };
    
    this->clear();
    this->watcher.removePath(this->root_path);
    this->root_path = root_path;
    this->watcher.addPath(this->root_path);
    
    QFileInfo root_info(root_path);
    
    hxTreeItem *root = new hxTreeItem(root_info.fileName());
    
    recurse(root_path, root);
    
    this->addTopLevelItem(root);
}

