#include "matcher.h"

QRegularExpression createRegExp(const QString &pattern, hxMatcher::MatchOptions options);
QRegularExpression createRegExp(const QString &pattern, Qt::CaseSensitivity case_sensivity);

hxMatcher::MatchResult synthetise(const QRegularExpressionMatch &match);

// --

hxMatcher::MatchResult hxMatcher::match(const QString& string, const QString& with, Qt::CaseSensitivity options)
{
    return synthetise(createRegExp(string, options).match(with));
}

hxMatcher::MatchFilterResult hxMatcher::match(const QString& string, const QStringList& with, Qt::CaseSensitivity options)
{
    hxMatcher::MatchFilterResult result;
    result.matched = false;
    
    QRegularExpression re = createRegExp(string, options);
    
    foreach(const QString& s, with) {
        
        hxMatcher::MatchResult r = synthetise(re.match(s));
        
        if(!r.matched)
            continue;
        
        result.matched = result.matched || r.matched;
        result.results << s;
        result.starts  << r.starts;
        result.lengths << r.lengths;
    }
    
    return result;
}

QVariantMap hxMatcher::filter(const QString& string, const QStringList& with)
{
    hxMatcher::MatchFilterResult match = hxMatcher::match(string, with, Qt::CaseInsensitive);
        
    QVariantMap matches;
    matches.insert("results", QVariant::fromValue(match.results));
    matches.insert("starts",  QVariant::fromValue(match.starts));
    matches.insert("lengths", QVariant::fromValue(match.lengths));

    return matches;
}

// --

QRegularExpression createRegExp(const QString &pattern, hxMatcher::MatchOptions options)
{
    if (pattern.isEmpty())
        return QRegularExpression();

    QString keyRegExp;
    QString plainRegExp;
    bool first = true;
    const QChar asterisk = '*';
    const QChar question = '?';
    const QLatin1String uppercaseWordFirst("(?<=\\b|[a-z0-9_])");
    const QLatin1String lowercaseWordFirst("(?<=\\b|[A-Z0-9_])");
    const QLatin1String uppercaseWordContinuation("[a-z0-9_]*");
    const QLatin1String lowercaseWordContinuation("(?:[a-zA-Z0-9]*_)?");
    const QLatin1String upperSnakeWordContinuation("[A-Z0-9]*_?");
    keyRegExp += "(?:";
    for (const QChar &c : pattern) {
        if (!c.isLetterOrNumber()) {
            if (c == question) {
                keyRegExp += '.';
                plainRegExp += ").(";
            } else if (c == asterisk) {
                keyRegExp += ".*";
                plainRegExp += ").*(";
            } else {
                const QString escaped = QRegularExpression::escape(c);
                keyRegExp += '(' + escaped + ')';
                plainRegExp += escaped;
            }
        } else if (options == hxMatcher::MatchOptions::CaseInsensitive ||
                  (options == hxMatcher::MatchOptions::FirstLetterCaseSensitive && !first)) {

            const QString upper = QRegularExpression::escape(c.toUpper());
            const QString lower = QRegularExpression::escape(c.toLower());
            keyRegExp += "(?:";
            keyRegExp += first ? uppercaseWordFirst : uppercaseWordContinuation;
            keyRegExp += '(' + upper + ')';
            if (first) {
                keyRegExp += '|' + lowercaseWordFirst + '(' + lower + ')';
            } else {
                keyRegExp += '|' + lowercaseWordContinuation + '(' + lower + ')';
                keyRegExp += '|' + upperSnakeWordContinuation + '(' + upper + ')';
            }
            keyRegExp += ')';
            plainRegExp += '[' + upper + lower + ']';
        } else {
            if (!first) {
                if (c.isUpper())
                    keyRegExp += uppercaseWordContinuation;
                else
                    keyRegExp += lowercaseWordContinuation;
            }
            const QString escaped = QRegularExpression::escape(c);
            keyRegExp += escaped;
            plainRegExp += escaped;
        }

        first = false;
    }
    keyRegExp += ')';
    
    return QRegularExpression('(' + plainRegExp + ")|" + keyRegExp);
}

QRegularExpression createRegExp(const QString &pattern, Qt::CaseSensitivity case_sensivity)
{
    const hxMatcher::MatchOptions sensitivity = (case_sensivity == Qt::CaseSensitive)
            ? hxMatcher::MatchOptions::CaseSensitive
            : hxMatcher::MatchOptions::CaseInsensitive;

    return createRegExp(pattern, sensitivity);
}

hxMatcher::MatchResult synthetise(const QRegularExpressionMatch &match)
{
    hxMatcher::MatchResult result;

    result.matched = (match.capturedTexts().size() > 0);
    
    for (int i = 1, size = match.capturedTexts().size(); i < size; ++i) {
        
        if (match.capturedStart(i) < 0)
            continue;

        if (!result.starts.isEmpty()
                && (result.starts.last() + result.lengths.last() == match.capturedStart(i))) {
            result.lengths.last() += match.capturedLength(i);
        } else {
            result.starts.append(match.capturedStart(i));
            result.lengths.append(match.capturedLength(i));
        }
    }

    return result;
}
