#pragma once

#include "completer.h"

class hxCompleterFS : public hxCompleter
{
    Q_OBJECT
    
public:
    hxCompleterFS(QObject *parent = nullptr);
   ~hxCompleterFS(void);

public slots:
    QString        apply(const QString&, const QString&) override;
    QVariantMap complete(const QString&)                 override;

private:
    class hxCompleterFSPrivate *d;
};
