#include "tree_model.h"

hxTreeModel::hxTreeModel(QObject* parent)
: QAbstractItemModel(parent),
_rootItem{new hxTreeItem()}
{
}

hxTreeModel::~hxTreeModel()
{
    delete _rootItem;
}

int hxTreeModel::rowCount(const QModelIndex& parent) const
{
    if (!parent.isValid()){
        return _rootItem->childCount();
    }
    
    return internalPointer(parent)->childCount();
}

int hxTreeModel::columnCount(const QModelIndex&  /*parent*/) const
{
    // This is basically flatten as a list model
    return 1;
}

QModelIndex hxTreeModel::index(const int row, const int column, const QModelIndex& parent) const
{
    if (!hasIndex(row, column, parent)){
        return {};
    }
    
    hxTreeItem* item = _rootItem;
    if (parent.isValid()){
        item = internalPointer(parent);
    }
    
    if (auto child = item->child(row)){
        return createIndex(row, column, child);
    }
    
    return {};
}

QModelIndex hxTreeModel::parent(const QModelIndex& index) const
{
    if (!index.isValid()){
        return {};
    }
    
    hxTreeItem* childItem = internalPointer(index);
    hxTreeItem* parentItem = childItem->parentItem();
    
    if (!parentItem){
        return {};
    }
    
    if (parentItem == _rootItem){
        return {};
    }
    
    return createIndex(parentItem->row(), 0, parentItem);
}

QVariant hxTreeModel::data(const QModelIndex& index, const int role) const
{
    if (!index.isValid() || role != Qt::DisplayRole) {
        return QVariant();
    }
    
    return internalPointer(index)->data();
}

bool hxTreeModel::setData(const QModelIndex& index, const QVariant& value, int /*role*/)
{
    if(!index.isValid()){
        return false;
    }
    
    if(auto item = internalPointer(index)){
        item->setData(value);
        emit dataChanged(index, index, {Qt::EditRole});
    }
    
    return false;
}

void hxTreeModel::addTopLevelItem(hxTreeItem* child)
{
    if(child){
        addItem(_rootItem, child);
    }
}

void hxTreeModel::addItem(hxTreeItem* parent, hxTreeItem* child)
{
    if(!child || !parent){
        return;
    }
    
    emit layoutAboutToBeChanged();
    
    if (child->parentItem()) {
        beginRemoveRows(QModelIndex(), child->parentItem()->childCount() - 1, child->parentItem()->childCount());
        child->parentItem()->removeChild(child);
        endRemoveRows();
    }
    
    beginInsertRows(QModelIndex(), parent->childCount() - 1, parent->childCount() - 1);
    child->setParentItem(parent);
    parent->appendChild(child);
    endInsertRows();
    
    emit layoutChanged();
}

void hxTreeModel::removeItem(hxTreeItem* item)
{
    if(!item){
        return;
    }
    
    emit layoutAboutToBeChanged();
    
    if (item->parentItem()) {
        beginRemoveRows(QModelIndex(), item->parentItem()->childCount() - 1, item->parentItem()->childCount());
        item->parentItem()->removeChild(item);
        endRemoveRows();
    }
    
    emit layoutChanged();
}

hxTreeItem* hxTreeModel::rootItem() const
{
    return _rootItem;
}

QModelIndex hxTreeModel::rootIndex()
{
    return {};
}

int hxTreeModel::depth(const QModelIndex& index) const
{
    int count = 0;
    auto anchestor = index;
    if(!index.isValid()){
        return 0;
    }
    while(anchestor.parent().isValid()){
        anchestor = anchestor.parent();
        ++count;
    }
    
    return count;
}

void hxTreeModel::clear()
{
    emit layoutAboutToBeChanged();
    beginResetModel();
    delete _rootItem;
    _rootItem = new hxTreeItem();
    endResetModel();
    emit layoutChanged();
}

hxTreeItem* hxTreeModel::internalPointer(const QModelIndex& index) const
{
    return static_cast<hxTreeItem* >(index.internalPointer());
}
