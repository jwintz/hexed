#pragma once

#include <QtCore>
#include <QtQml>

class hxJournalItem;

class hxJournal : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QQmlListProperty<hxJournalItem> items READ items NOTIFY itemsChanged);
    Q_PROPERTY(int infoCount READ infoCount NOTIFY infoCountChanged);
    Q_PROPERTY(int debugCount READ debugCount NOTIFY debugCountChanged);
    Q_PROPERTY(int warningCount READ warningCount NOTIFY warningCountChanged);
    Q_PROPERTY(int errorCount READ errorCount NOTIFY errorCountChanged);

public:
    static hxJournal *instance(void);

signals:
    void    infoCountChanged(void);
    void   debugCountChanged(void);
    void   errorCountChanged(void);
    void warningCountChanged(void);

signals:
    void itemsChanged(void);

public:
    int   infoCount(void);
    int  debugCount(void);
    int  errorCount(void);
    int warningCount(void);

    void addInfoMsg(const QString&);
    void addDebugMsg(const QString&);
    void addWarningMsg(const QString&);
    void addErrorMsg(const QString&);

    QQmlListProperty<hxJournalItem> items(void);

public:
    static qsizetype itemCount(QQmlListProperty<hxJournalItem> *);

public:
    static hxJournalItem *itemAt(QQmlListProperty<hxJournalItem> *, qsizetype);

public slots:
    void clear(void);

private:
     hxJournal(void);
    ~hxJournal(void);

private:
    static hxJournal* s_instance;

private:
    class hxJournalPrivate *d;
};
