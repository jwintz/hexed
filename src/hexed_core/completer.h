#pragma once

#include <QtCore>

class hxCompleter : public QObject
{
    Q_OBJECT
    
public:
     hxCompleter(QObject *parent = nullptr);
    ~hxCompleter(void);
    
public slots:
    virtual QString        apply(const QString&, const QString&) = 0;
    virtual QVariantMap complete(const QString&) = 0;
};

