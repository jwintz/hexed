#include "tree_item.h"

hxTreeItem::hxTreeItem()
: _itemData(),
_parentItem(nullptr)
{
}

hxTreeItem::hxTreeItem(const QVariant& data)
: _itemData(data),
_parentItem(nullptr)
{
}

hxTreeItem::~hxTreeItem()
{
    qDeleteAll(_childItems);
}

hxTreeItem* hxTreeItem::parentItem()
{
    return _parentItem;
}

void hxTreeItem::setParentItem(hxTreeItem* parentItem)
{
    _parentItem = parentItem;
}

void hxTreeItem::appendChild(hxTreeItem* item)
{
    if(item && !_childItems.contains(item)){
        _childItems.append(item);
    }
}

void hxTreeItem::removeChild(hxTreeItem* item)
{
    if(item){
        _childItems.removeAll(item);
    }
}

hxTreeItem* hxTreeItem::child(int row)
{
    return _childItems.value(row);
}

int hxTreeItem::childCount() const
{
    return _childItems.count();
}

const QVariant& hxTreeItem::data() const
{
    return _itemData;
}

void hxTreeItem::setData(const QVariant& data)
{
    _itemData = data;
}

bool hxTreeItem::isLeaf() const
{
    return _childItems.isEmpty();
}

int hxTreeItem::depth() const
{
    int depth = 0;
    hxTreeItem* anchestor = _parentItem;
    while(anchestor){
        ++depth;
        anchestor = anchestor->parentItem();
    }
    
    return depth;
}

int hxTreeItem::row() const
{
    if (_parentItem){
        return _parentItem->_childItems.indexOf(const_cast<hxTreeItem* >(this));
    }
    
    return 0;
}
