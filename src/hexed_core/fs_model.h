#pragma once

#include <QtCore>
#include <QtGui>

#include "tree_item.h"
#include "tree_model.h"

class hxFSModel : public hxTreeModel
{
    Q_OBJECT

public:
     hxFSModel(QObject *parent = nullptr);
    ~hxFSModel(void);

public slots:
    void setRootPath(const QString&);

private:
    QString root_path;
    
private:
    QFileSystemModel model;
    QFileSystemWatcher watcher;
};
