#pragma once

#include <QtCore>

class hxMatcher : public QObject
{
    Q_OBJECT

public:
    enum MatchOptions {
        CaseInsensitive,
        CaseSensitive,
        FirstLetterCaseSensitive
    };

    class MatchResult {
    public:
        bool matched;
        QVector<int> starts;
        QVector<int> lengths;
    };
    
    class MatchFilterResult {
    public:
        bool matched;
        QStringList results;
        QList<QVector<int>> starts;
        QList<QVector<int>> lengths;
    };
    
public:
    hxMatcher(QObject *parent = 0) : QObject(parent) {}
   ~hxMatcher(void) {}
    
public:
    static MatchResult       match(const QString&, const QString&,     Qt::CaseSensitivity);
    static MatchFilterResult match(const QString&, const QStringList&, Qt::CaseSensitivity);
    
public:
    Q_INVOKABLE QVariantMap filter(const QString&, const QStringList&);
};
