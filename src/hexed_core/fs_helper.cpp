#include "fs_helper.h"

hxFSHelper::hxFSHelper(QObject *parent) : QObject(parent)
{
    
}

hxFSHelper::~hxFSHelper(void)
{
    
}

QString hxFSHelper::homePath(void)
{
    return QDir::homePath() + "/";
}

bool hxFSHelper::exists(const QString& path)
{
    QFileInfo info(path);
    
    return info.exists();
}

QString hxFSHelper::fileName(const QString& path)
{
    QFileInfo info(path);
    
    return info.fileName();
}

QString hxFSHelper::projectName(const QString&) {
    
    return "Project Name (TODO)";
}

QString hxFSHelper::projectPath(const QString& file_path)
{
    QFileInfo file_info(file_path);
    
    QDir file_dir = file_info.dir();
    
    std::function<QString (const QDir&)> recurse;
    
    recurse = [&] (const QDir& dir) {
    
        // qDebug() << "[qml]" << Q_FUNC_INFO << "Testing" << dir.absolutePath();
        
        if(dir == QDir::home() || dir == QDir::root())
            return file_dir.absolutePath();
        
        QStringList dir_entries = dir.entryList(QDir::Dirs | QDir::Hidden | QDir::NoSymLinks);
    
        if (dir_entries.contains(".git")) {
            return dir.absolutePath();
        } else {
            QDir parent(dir);
            parent.cdUp();
            return recurse(parent);
        }
    };
    
    return recurse(file_dir);
}

QString hxFSHelper::size(const QString& contents)
{
    QLocale locale;
    
    return locale.formattedDataSize(contents.toUtf8().size());
}
