#pragma once

#include <QAbstractItemModel>

#include "tree_item.h"

class hxTreeModel : public QAbstractItemModel
{
    Q_OBJECT
    
public:
    explicit hxTreeModel(QObject *parent = nullptr);
    virtual ~hxTreeModel(void) override;
    
public:
    virtual int rowCount(const QModelIndex& index) const override;
    virtual int columnCount(const QModelIndex& index) const override;
    
    virtual QModelIndex index(int row, int column, const QModelIndex& parent) const override;
    virtual QModelIndex parent(const QModelIndex& childIndex) const override;
    
    virtual QVariant data(const QModelIndex& index, int role = 0) const override;
    virtual bool setData(const QModelIndex& index, const QVariant& value, int role = Qt::EditRole) override;
    
public:
    void addTopLevelItem(hxTreeItem *child);
    void addItem(hxTreeItem *parent, hxTreeItem *child);
    void removeItem(hxTreeItem *item);
    
    hxTreeItem* rootItem(void) const;
    
    Q_INVOKABLE virtual QModelIndex rootIndex(void);
    Q_INVOKABLE virtual int depth(const QModelIndex& index) const;
    Q_INVOKABLE virtual void clear(void);
    
private:
    hxTreeItem *internalPointer(const QModelIndex& index) const;
    
private:
    hxTreeItem *_rootItem;
};
