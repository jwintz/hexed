#include "fs.h"

hxFS::hxFS(QObject *parent) : QObject(parent)
{
    
}

hxFS::~hxFS(void)
{
    
}

QString hxFS::read(const QString& path)
{
    QFile file(path);

    if(!file.open(QIODevice::ReadOnly))
        return QString();

    QString contents = file.readAll();

    file.close();

    return contents;
}

void hxFS::write(const QString& path, const QString& contents)
{
    QFile file(path);
 
    if(!file.open(QIODevice::WriteOnly | QIODevice::Truncate | QIODevice::Text))
        return;

    QTextStream out(&file);
    out << contents;

    file.close();
}
