#pragma once

#include <QVariant>

class hxTreeItem
{
    friend class hxTreeModel;
    
public:
    hxTreeItem();
    explicit hxTreeItem(const QVariant& data);
    ~hxTreeItem();
    
    const QVariant& data() const;
    void setData(const QVariant& data);
    
    int childCount() const;
    int row() const;
    bool isLeaf() const;
    int depth() const;
    
private:
    hxTreeItem* parentItem();
    void setParentItem(hxTreeItem* parentItem);
    
    void appendChild(hxTreeItem* item);
    void removeChild(hxTreeItem* item);
    
    hxTreeItem* child(int row);
    
private:
    QVariant _itemData;
    hxTreeItem* _parentItem;
    QVector<hxTreeItem*> _childItems;
};
