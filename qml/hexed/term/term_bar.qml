import QtQuick

Rectangle {
    id: tabBar
    color: "black"
    width: parent.width
    height: tabCount > 1 ? _tabHeight : 0
    visible: height > 0

    property int currentIndex

    property var tabArray
    property int tabCount

    signal removeIndex(int index)

    property int _animationDuration: 100

    property int _tabHeight: tm.height + 10
    TextMetrics {
        id: tm
        text: "Hello, world"
    }

    property int _maxTabWidth: (width / _minVisibleTabs)
    property real _minTabWidth: (width / _maxVisibleTabs) // real is deliberate, otherwise loss of precision stacks up

    readonly property int _maxVisibleTabs: 8
    readonly property int _minVisibleTabs: 2

    property real _currentTabWidth: Math.max(tabBar._minTabWidth, Math.min(tabBar._maxTabWidth, tabBar.width / tabBar.tabCount))
    Behavior on _currentTabWidth {
        NumberAnimation {
            duration: tabBar._animationDuration
        }
    }

    ListView {
        id: tabListView
        orientation: Qt.Horizontal
        interactive: false
        anchors.fill: parent
        model: tabBar.tabCount
        delegate: Rectangle {
            id: delegateItem
            color: tabBar.currentIndex == index ? "#eeeeee" : "#999999"
            
            SequentialAnimation {
                id: add_a;
                PropertyAction {
                    target: delegateItem
                    property: "width"
                    value: 0
                }
                NumberAnimation {
                    target: delegateItem
                    property: "width"
                    to: tabBar._currentTabWidth
                    duration: tabBar._animationDuration
                    easing.type: Easing.InOutQuad
                }
            }
            
            SequentialAnimation {
                id: rem_a;
                PropertyAction {
                    target: delegateItem;
                    property: "ListView.delayRemove";
                    value: true
                }
                NumberAnimation {
                    target: delegateItem
                    property: "width"
                    to: 0
                    duration: tabBar._animationDuration
                    easing.type: Easing.InOutQuad
                }
                PropertyAction {
                    target: delegateItem;
                    property: "ListView.delayRemove";
                    value: false
                }
            }
            
            ListView.onAdd: add_a.start();
            ListView.onRemove: rem_a.start();

            width: tabBar._currentTabWidth
            height: tabBar._tabHeight

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    tabBar.currentIndex = index
                }
            }

            MouseArea {
                id: closeIcon
                height: parent.height
                width: 10
                anchors.left: parent.left
                anchors.leftMargin: 10
                onClicked: tabBar.removeIndex(index)

                // TODO: an icon or something would be nice...
                Text {
                    text: "x"
                    anchors.centerIn: parent
                }
            }

            Text {
                id: tabTitle
                text: tabBar.tabArray[index]
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: closeIcon.right
                anchors.right: parent.right
                anchors.margins: 10
                elide: Text.ElideRight
            }

            Rectangle {
                anchors.right: parent.right
                width: 1
                height: parent.height
                color: "black"
            }
        }
    }

    // TODO: an overflow menu of sorts.
    Rectangle {
        x: parent.width - width
        height: parent.height
        width: 20
        color: "red"
        visible: tabBar.tabCount > tabBar._maxVisibleTabs

        MouseArea {
            anchors.fill: parent
            onClicked: {
                pickerDialog.visible = true
            }
        }

        Rectangle {
            id: pickerDialog
            visible: false
            height: childrenRect.height
            width: childrenRect.width
            color: "white"
            x: parent.width - (width + 10)
            y: 10

            Column {
                Repeater {
                    model: tabBar.tabCount
                    delegate: Rectangle {
                        width: 250
                        height: 25
                        visible: index >= tabBar._maxVisibleTabs
                        color: ma.containsMouse ? "#999999" : "white"

                        MouseArea {
                            id: ma
                            anchors.fill: parent
                            hoverEnabled: true
                            onClicked: {
                                tabBar.currentIndex = index
                                pickerDialog.visible = false
                            }
                        }

                        Text {
                            anchors.verticalCenter: parent.verticalCenter
                            width: parent.width - 20
                            x: 10
                            elide: Text.ElideRight
                            text: tabBar.tabArray[index]
                        }
                        Rectangle {
                            color: "black"
                            width: parent.width
                            height: 1
                            anchors.top: parent.top
                        }
                        Rectangle {
                            color: "black"
                            width: parent.width
                            height: 1
                            anchors.bottom: parent.bottom
                        }
                        Rectangle {
                            color: "black"
                            height: parent.height
                            width: 1
                            anchors.left: parent.left
                        }
                        Rectangle {
                            color: "black"
                            height: parent.height
                            width: 1
                            anchors.right: parent.right
                        }
                    }
                }
            }
        }
    }
}
