import QtQuick

Column {
    id: root
    property alias currentIndex: tabBar.currentIndex
    property int count: tabContainer.children.length
    property Item activeTabItem
    
    TermBar {
        id: tabBar
        z: 1
        tabArray: tabContainer.tabTitles
        tabCount: 0

        onRemoveIndex: {
            root.removeTab(index)
        }
    }
    
    Item {
        id: tabContainer
        height: parent.height - tabBar.height
        width: parent.width
        property var tabTitles: []
    }

    onCurrentIndexChanged: {
        fixupVisibility()
    }

    function fixupVisibility() {
        var set = false
        for (var i = 0; i < tabContainer.children.length; ++i) {
            var child = tabContainer.children[i]
            child.visible = i == currentIndex
            if (child.visible) {
                child.forceActiveFocus();
                activeTabItem = child
                set = true
            }
        }

        if (!set) {
            activeTabItem = null
        }
    }

    function _updateTabTitle() {
        for (var i = 0; i < tabContainer.children.length; ++i) {
            var obj = tabContainer.children[i]
            if (obj == this) {
                tabContainer.tabTitles[i] = this.title

                tabBar.tabCount = 0
                tabBar.tabCount = tabContainer.tabTitles.length
                return
            }
        }
    }

    function addTab(titleText, comp) {
        if (comp.status != Component.Ready) {
            console.warn("Component not ready! " + comp.errorString)
            return
        }
        var tabInstance = comp.createObject(tabContainer)
        var title = tabInstance.title ? tabInstance.title : "Shell " + tabContainer.children.length
        tabInstance.titleChanged.connect(_updateTabTitle.bind(tabInstance))
        tabContainer.tabTitles.push(title);
        tabBar.tabCount = tabContainer.tabTitles.length

        fixupVisibility();

        return tabInstance
    }

    function removeTab(tabIndex) {
        var tab = tabContainer.children[tabIndex]
        tab.parent = null
        tab.destroy()
        tabContainer.tabTitles.splice(tabIndex, 1)
        tabBar.tabCount = tabContainer.tabTitles.length
        var ci = currentIndex
        if (tabIndex >= ci)
            ci--
        if (ci < 0)
            ci = 0
        currentIndex = ci

        fixupVisibility(); 
    }

    function getTab(tabIndex) {
        return tabContainer.children[tabIndex]
    }
}
