import QtQuick
import QtQuick.Controls

import hexed.Term as H

Control {

    id: self

    background: Rectangle {
        color: "#221E28";
    }

    H.TermView {
        id: tabView
        anchors.fill: parent

        Component.onCompleted: {
            createTab();
        }

        Component {
            id: terminalScreenComponent
            H.TermRender {
                id: textrender
                focus: true

                onPanLeft: {
                    H.TermUtil.notifyText(H.TermUtil.panLeftTitle)
                    textrender.putString(H.TermUtil.panLeftCommand)
                }
                onPanRight: {
                    H.TermUtil.notifyText(H.TermUtil.panRightTitle)
                    textrender.putString(H.TermUtil.panRightCommand)
                }
                onPanUp: {
                    H.TermUtil.notifyText(H.TermUtil.panUpTitle)
                    textrender.putString(H.TermUtil.panUpCommand)
                }
                onPanDown: {
                    H.TermUtil.notifyText(H.TermUtil.panDownTitle)
                    textrender.putString(H.TermUtil.panDownCommand)
                }

                onDisplayBufferChanged: {
                    textrender.cutAfter = textrender.height;
                    textrender.y = 0;
                }
                onTitleChanged: {
                    H.TermUtil.windowTitle = title
                }
                // dragMode: H.TermUtil.dragMode
                onVisualBell: {
                    if (H.TermUtil.visualBellEnabled)
                        bellTimer.start()
                }
                contentItem: Item {
                    anchors.fill: parent
                    Behavior on opacity {
                        NumberAnimation { duration: textrender.duration; easing.type: Easing.InOutQuad }
                    }
                    Behavior on y {
                        NumberAnimation { duration: textrender.duration; easing.type: Easing.InOutQuad }
                    }
                }

                // ScrollIndicator {
                //     anchors.right: parent.right
                //     anchors.top: parent.top
                //     anchors.bottom: parent.bottom
                //     position: ((parent.contentY) / parent.contentHeight)
                //     size: (parent.visibleHeight / parent.contentHeight)
                //     orientation: Qt.Vertical
                //     visible: parent.contentHeight > parent.visibleHeight
                // }

                cellDelegate: Rectangle {
                    
                }
                
                cellContentsDelegate: Text {
                    id: text
                    property bool blinking: false

                    textFormat: Text.PlainText
                    // opacity: blinking ? 0.5 : 1.0
                    // SequentialAnimation {
                    //     running: blinking
                    //     loops: Animation.Infinite
                    //     NumberAnimation {
                    //         target: text
                    //         property: "opacity"
                    //         to: 0.8
                    //         duration: 200
                    //     }
                    //     PauseAnimation {
                    //         duration: 400
                    //     }
                    //     NumberAnimation {
                    //         target: text
                    //         property: "opacity"
                    //         to: 0.5
                    //         duration: 200
                    //     }
                    // }
                }
                cursorDelegate: Rectangle {
                    id: cursor
                    opacity: 0.5
                    // SequentialAnimation {
                    //     running: {
                    //         if (Qt.application.state != Qt.ApplicationActive) {
                    //             return false;
                    //         }

                    //         return startPauseAnim.duration > 0 ||
                    //                fadeInAnim.duration > 0 ||
                    //                middlePauseAnim.duration > 0 ||
                    //                fadeOutAnim.duration > 0 ||
                    //                endPauseAnim.duration > 0
                    //     }
                    //     loops: Animation.Infinite
                    //     PauseAnimation {
                    //         id: startPauseAnim
                    //         duration: H.TermUtil.cursorAnimationStartPauseDuration
                    //     }
                    //     NumberAnimation {
                    //         id: fadeInAnim
                    //         target: cursor
                    //         property: "opacity"
                    //         to: 0.8
                    //         duration: H.TermUtil.cursorAnimationFadeInDuration
                    //     }
                    //     PauseAnimation {
                    //         id: middlePauseAnim
                    //         duration: H.TermUtil.cursorAnimationMiddlePauseDuration
                    //     }
                    //     NumberAnimation {
                    //         id: fadeOutAnim
                    //         target: cursor
                    //         property: "opacity"
                    //         to: 0.5
                    //         duration: H.TermUtil.cursorAnimationFadeOutDuration
                    //     }
                    //     PauseAnimation {
                    //         id: endPauseAnim
                    //         duration: H.TermUtil.cursorAnimationEndPauseDuration
                    //     }
                    // }
                }
                selectionDelegate: Rectangle {
                    color: "blue"
                    opacity: 0.5
                }

                Rectangle {
                    id: bellTimerRect
                    visible: opacity > 0
                    opacity: bellTimer.running ? 0.5 : 0.0
                    anchors.fill: parent
                    color: "#ffffff"
                    Behavior on opacity {
                        NumberAnimation {
                            duration: 200
                        }
                    }
                }

                property int duration
                property int cutAfter: height

                anchors.fill: parent
                font.family: H.TermUtil.fontFamily
                font.pointSize: H.TermUtil.fontSize
                // allowGestures: !menu.showing && !urlWindow.show && !aboutDialog.show && !layoutWindow.show

                onCutAfterChanged: {
                    // this property is used in the paint function, so make sure that the element gets
                    // painted with the updated value (might not otherwise happen because of caching)
                    textrender.redraw();
                }
            }
        }

        function createTab() {
            var tab = tabView.addTab("", terminalScreenComponent);
            tab.hangupReceived.connect(function() {
                closeTab(tab)
            });
            tabView.currentIndex = tabView.count - 1;
        }

        function closeTab(screenItem) {
            if (tabView.count == 1) {
                Qt.quit();
                return;
            }
            for (var i = 0; i < tabView.count; i++) {
                if (tabView.getTab(i) === screenItem) {
                    tabView.removeTab(i);
                    break;
                }
            }
        }

        property string tabChangeKey: "Ctrl+Alt+Shift"

        // Shortcut {
        //     sequence: Qt.platform.os == "osx" ? "Ctrl+C" : "Ctrl+Shift+C"
        //     onActivated: {
        //         tabView.activeTabItem.copy();
        //     }
        // }
        // Shortcut {
        //     sequence: Qt.platform.os == "osx" ? "Ctrl+C" : "Ctrl+Shift+V"
        //     onActivated: {
        //         if (tabView.activeTabItem.canPaste)
        //             tabView.activeTabItem.paste();
        //     }
        // }

        // Shortcut {
        //     sequence: self.tabChangeKey + "+1"
        //     onActivated: {
        //         if (tabView.count >= 2)
        //             tabView.currentIndex = 0 // yes, this is right. 0 indexed.
        //     }
        // }
        // Shortcut {
        //     sequence: self.tabChangeKey + "+2"
        //     onActivated: {
        //         if (tabView.count >= 2)
        //             tabView.currentIndex = 1 // yes, this is right. 0 indexed.
        //     }
        // }
        // Shortcut {
        //     sequence: self.tabChangeKey + "+3"
        //     onActivated: {
        //         if (tabView.count >= 3)
        //             tabView.currentIndex = 2 // yes, this is right. 0 indexed.
        //     }
        // }
        // Shortcut {
        //     sequence: self.tabChangeKey + "+4"
        //     onActivated: {
        //         if (tabView.count >= 4)
        //             tabView.currentIndex = 3 // yes, this is right. 0 indexed.
        //     }
        // }
        // Shortcut {
        //     sequence: self.tabChangeKey + "+5"
        //     onActivated: {
        //         if (tabView.count >= 5)
        //             tabView.currentIndex = 4 // yes, this is right. 0 indexed.
        //     }
        // }
        // Shortcut {
        //     sequence: self.tabChangeKey + "+6"
        //     onActivated: {
        //         if (tabView.count >= 6)
        //             tabView.currentIndex = 5 // yes, this is right. 0 indexed.
        //     }
        // }
        // Shortcut {
        //     sequence: self.tabChangeKey + "+7"
        //     onActivated: {
        //         if (tabView.count >= 7)
        //             tabView.currentIndex = 6 // yes, this is right. 0 indexed.
        //     }
        // }
        // Shortcut {
        //     sequence: self.tabChangeKey + "+8"
        //     onActivated: {
        //         if (tabView.count >= 8)
        //             tabView.currentIndex = 7 // yes, this is right. 0 indexed.
        //     }
        // }
        // Shortcut {
        //     sequence: self.tabChangeKey + "+9"
        //     onActivated: {
        //         if (tabView.count >= 9)
        //             tabView.currentIndex = 8 // yes, this is right. 0 indexed.
        //     }
        // }
        // Shortcut {
        //     sequence: self.tabChangeKey + "+0"
        //     onActivated: {
        //         if (tabView.count >= 10)
        //             tabView.currentIndex = 9 // yes, this is right. 0 indexed.
        //     }
        // }
        // Shortcut {
        //     sequence: self.tabChangeKey + "+T"
        //     onActivated: {
        //         console.info("Here");
        //         tabView.createTab();
        //     }
        // }
        // Shortcut {
        //     sequence: self.tabChangeKey + "+W"
        //     onActivated: {
        //         tabView.closeTab(tabView.activeTabItem)
        //     }
        // }
        // Shortcut {
        //     sequence: self.tabChangeKey + "+]"
        //     onActivated: {
        //         tabView.currentIndex = (tabView.currentIndex + 1) % tabView.count;
        //     }
        // }
        // Shortcut {
        //     sequence: self.tabChangeKey + "+["
        //     onActivated: {
        //         if (tabView.currentIndex > 0) {
        //             tabView.currentIndex--;
        //         } else {
        //             tabView.currentIndex = tabView.count -1;
        //         }
        //     }
        // }
    }

    Timer {
        id: bellTimer

        interval: 80
    }
}
