import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

import hexed.Controls as H

Control {
    id: self;
    
    height: 28;
    width: parent ? parent.width : 0;

    background: Rectangle {
        color: index % 2 ? "transparent" : "#11000000";
    }

    hoverEnabled: true
    
    RowLayout {

        anchors.fill: parent;

        Label {
            text: modelData.label;

            font.bold: modelData.type > 1;
            font.pointSize: 10
            
            color:
              modelData.type == 0 ? "#fec38f"
            : modelData.type == 2 ? "#b07745"
            : modelData.type == 3 ? "#765f4c"
            :                       "#D4A67A";

            Layout.minimumWidth: 48 + 8;
            Layout.maximumWidth: 48 + 8;

            leftPadding: 4;
        }

        Label {
            text: modelData.description;
            clip: true;

            font.pointSize: 10
            leftPadding: 6;
            elide: Text.ElideRight
            Layout.fillWidth: true;
        }

        Label {
            text: modelData.time;
            font.pointSize: 10
            Layout.minimumWidth: 64;
            Layout.maximumWidth: 64;
        }
    }
    
    H.ToolTip { text: modelData.description; visible: self.hovered; position: Qt.AlignRight;
        color:
          modelData.type == 0 ? "#fec38f"
        : modelData.type == 2 ? "#b07745"
        : modelData.type == 3 ? "#765f4c"
        :                       "#D4A67A";
    }
}
