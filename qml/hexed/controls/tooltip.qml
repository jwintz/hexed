import QtQuick
import QtQuick.Controls

ToolTip
{
    id: control

    property int position: Qt.AlignTop
    property alias color: bg.color;
    
    font.pointSize: 10
    
    x: {
        if(!parent)
            return 0;
        
        switch(position) {
        case Qt.AlignBottom | Qt.AlignLeft:
        case Qt.AlignTop    | Qt.AlignLeft:
            return -width - margins / 2
        case Qt.AlignBottom:
        case Qt.AlignVCenter | Qt.AlignHCenter:
        case Qt.AlignTop:
            return (parent.width - width) / 2
        case Qt.AlignBottom | Qt.AlignRight:
        case Qt.AlignTop    | Qt.AlignRight:
            return parent.width + margins / 2
        case Qt.AlignRight:
            return parent.width + margins / 2
        case Qt.AlignLeft:
            return -width - margins / 2
        }
        
        console.warn(`Unknown Position : ${position}`);
        
        return 0;
    }

    y:
    {
        if(!parent) return 0;
        switch(position)
        {
        case Qt.AlignTop | Qt.AlignLeft:
        case Qt.AlignTop:
        case Qt.AlignTop | Qt.AlignRight:
            return -height - margins / 2

        case Qt.AlignBottom | Qt.AlignLeft:
        case Qt.AlignBottom:
        case Qt.AlignBottom | Qt.AlignRight:
            return parent.height + margins / 2

        case Qt.AlignRight:
        case Qt.AlignHCenter | Qt.AlignVCenter:
        case Qt.AlignLeft:
            return (parent.height - height) / 2
        }

        X.Logger.warn(`Unknown Position : ${position}`);
        
        return -height - margins / 2;
    }

    implicitWidth: Math.max(implicitBackgroundWidth + leftInset + rightInset, implicitContentWidth + leftPadding + rightPadding)
    implicitHeight: Math.max(implicitBackgroundHeight + topInset + bottomInset, implicitContentHeight + topPadding + bottomPadding)

    margins: 12
    horizontalPadding: 16
    verticalPadding: 0

    closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutsideParent | Popup.CloseOnReleaseOutsideParent

    enter: Transition {
        NumberAnimation {
            property: "opacity";from: 0.0;to: 1.0;easing.type: Easing.OutQuad;duration: 100
        }
    }

    exit: Transition {
        NumberAnimation {
            property: "opacity";from: 1.0;to: 0.0;easing.type: Easing.InQuad;duration: 200
        }
    }

    contentItem: Label {
        text: control.text
        font: control.font
        color: "#eeeeee";
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
    }

    background: Rectangle {
        id: bg
        implicitWidth: 200
        implicitHeight: 32
        color: "#dd444444"
        opacity: 0.9
        radius: 4
    }
}
