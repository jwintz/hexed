import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

import hexed.Core as H
import hexed.Controls as HC

Control
{
    id: self;

    signal itemsChanged();

    ColumnLayout {

        anchors.fill: parent;

        spacing: 0;

        RowLayout {

            Layout.fillWidth: true;
            Layout.minimumHeight: 28-2;
            Layout.maximumHeight: 28-2;

            Label {
                text: "Type"

                font.pointSize: 12
                
                Layout.leftMargin: 10;
                Layout.minimumWidth: 48;
                Layout.maximumWidth: 48;
            }

            Rectangle { Layout.fillHeight: true; width: 1; color: "#4A434D" }

            Label {
                text: "Description";

                font.pointSize: 12
                
                Layout.fillWidth: true;
            }

            Rectangle { Layout.fillHeight: true; width: 1; color: "#4A434D" }

            Label {
                text: "Time"

                font.pointSize: 12
                
                Layout.minimumWidth: 64;
                Layout.maximumWidth: 64;
            }
        }

        Rectangle { Layout.fillWidth: true; height: 1; color: "#4A434D" }
        
        ListView {

            id: _console_list;

            Layout.fillWidth: true;
            Layout.fillHeight: true;

            clip: true;

            model: H.Journal.items;

            delegate: HC.JournalDelegate {

            }

            ScrollBar.vertical: ScrollBar { visible: _console_list.contentHeight > _console_list.height; }
        }

        Rectangle { Layout.fillWidth: true; height: 1; color: "#4A434D" }

        RowLayout {
            Layout.fillWidth: true;
            Layout.leftMargin: 8;
            Layout.rightMargin: 8;
            Layout.minimumHeight: 28 - 1;
            Layout.maximumHeight: 28 - 1;

            spacing: 10;
            
            Item {
                
                Layout.minimumWidth: 28;
                Layout.maximumWidth: 28;
                Layout.fillHeight: true
                
                RowLayout {
                    anchors.fill: parent;
                    spacing: 0;
                    
                    Text {
                        color: "#fec38f"
                        font.family: "Material Design Icons"
                        font.pointSize: 13
                        text: '󰋼'
                    }
                    
                    Label { text: H.Journal.infoCount; font.pointSize: 11 }
                }
                
                MouseArea {
                        id: area_i; anchors.fill: parent; hoverEnabled: true
                }
                
                HC.ToolTip { text: 'Info'; visible: area_i.containsMouse; position: Qt.AlignTop; implicitWidth: 48 }
            }
            
            Item {
                
                Layout.minimumWidth: 28;
                Layout.maximumWidth: 28;
                Layout.fillHeight: true
                
                RowLayout {
                    anchors.fill: parent;
                    spacing: 0;
                    
                    Text {
                        color: "#E2B182"
                        font.family: "Material Design Icons"
                        font.pointSize: 13
                        text: '󰹯'
                    }
                    
                    Label { text: H.Journal.debugCount; font.pointSize: 11 }
                }
                
                MouseArea {
                        id: area_d; anchors.fill: parent; hoverEnabled: true
                }
                
                HC.ToolTip { text: 'Debug'; visible: area_d.containsMouse; position: Qt.AlignTop; implicitWidth: 48 }
            }
            
            Item {
                
                Layout.minimumWidth: 28;
                Layout.maximumWidth: 28;
                Layout.fillHeight: true
                
                RowLayout {
                    anchors.fill: parent;
                    spacing: 0;
                    
                    Text {
                        color: "#b07745"
                        font.family: "Material Design Icons"
                        font.pointSize: 13
                        text: '󰀨'
                    }
                    
                    Label { text: H.Journal.warningCount; font.pointSize: 11 }
                }
                
                MouseArea {
                        id: area_w; anchors.fill: parent; hoverEnabled: true
                }
                
                HC.ToolTip { text: 'Warning'; visible: area_w.containsMouse; position: Qt.AlignTop; implicitWidth: 48 }
            }
            
            Item {
                
                Layout.minimumWidth: 28;
                Layout.maximumWidth: 28;
                Layout.fillHeight: true
                
                RowLayout {
                    anchors.fill: parent;
                    spacing: 0;
                    
                    Text {
                        color: "#765f4c"
                        font.family: "Material Design Icons"
                        font.pointSize: 13
                        text: '󰀩'
                    }
                    
                    Label { text: H.Journal.errorCount; font.pointSize: 11 }
                }
                
                MouseArea {
                        id: area_e; anchors.fill: parent; hoverEnabled: true
                }
                
                HC.ToolTip { text: 'Error'; visible: area_e.containsMouse; position: Qt.AlignTop; implicitWidth: 48 }
            }
            
            Item {
                Layout.fillWidth: true;
            }

            Rectangle { Layout.fillHeight: true; width: 1; color: "#4A434D" }
            
            Label {

                text: 'Clear';
                font.pointSize: 11

                MouseArea {
                    anchors.fill: parent
                    onClicked: H.Journal.clear();
                }
            }
        }
    }

    Connections {
        target: H.Journal;

        function onItemsChanged() {
            self.itemsChanged();
        }
    }

    background: Rectangle {
        color: "#00000000";
    }
}
