import QtQuick
import QtQuick.Window
import QtQuick.Controls
import QtQml

import hexed.Controls as H

Flickable {
    id: root
    
    implicitWidth: 400
    implicitHeight: 400
    clip: true
    
    property var model
    readonly property alias currentIndex: tree.selectedIndex
    readonly property alias currentItem: tree.currentItem
    property var currentData
    
    property alias handle: tree.handle
    property alias contentItem: tree.contentItem
    property Component highlight: Rectangle {
        color: root.selectedColor
    }
    
    property alias selectionEnabled: tree.selectionEnabled
    property alias hoverEnabled: tree.hoverEnabled
    
    property alias color: tree.color
    property alias handleColor: tree.handleColor
    property alias hoverColor: tree.hoverColor
    property alias selectedColor: tree.selectedColor
    property alias selectedItemColor: tree.selectedItemColor
    
    property alias rowHeight: tree.rowHeight
    property alias rowPadding: tree.rowPadding
    property alias rowSpacing: tree.rowSpacing
    
    property alias fontMetrics: tree.fontMetrics
    property alias font: tree.font
    
    enum Handle {
        Triangle,
        TriangleSmall,
        TriangleOutline,
        TriangleSmallOutline,
        Chevron,
        Arrow
    }
    
    property int handleStyle: TreeView.Handle.Triangle
    
    contentHeight: tree.height
    contentWidth: width
    boundsBehavior: Flickable.StopAtBounds
    
    ScrollIndicator.vertical: H.ScrollIndicator {}
    
    Connections { function onCurrentIndexChanged() { if(currentIndex) currentData = model.data(currentIndex) }  }
    
    TreeViewItem {
        id: tree
        
        model: root.model
        parentIndex: model.rootIndex()
        childCount: model.rowCount(parentIndex)
        
        itemLeftPadding: 0
        color: root.color
        handleColor: root.handleColor
        hoverColor: root.hoverColor
        selectedColor: root.selectedColor
        selectedItemColor: root.selectedItemColor
        defaultIndicator: indicatorToString(handleStyle)
        z: 1
        
        Connections {
            target: root.model
            ignoreUnknownSignals: true
            function onLayoutChanged() {
                tree.childCount = root.model ? root.model.rowCount(tree.parentIndex) : 0
            }
        }
    }
    
    Loader {
        id: highlightLoader
        sourceComponent: highlight
        
        width: root.width
        height: root.rowHeight
        z: 0
        visible: root.selectionEnabled && tree.currentItem !== null
        
        Binding {
            target: highlightLoader.item
            property: "y"
            value: tree.currentItem ? tree.currentItem.mapToItem(tree, 0, 0).y + tree.anchors.topMargin : 0
            when: highlightLoader.status === Loader.Ready
        }
    }
    
    function indicatorToString(handle){
        switch (handle){
            case TreeView.Handle.Triangle: return "▶";
            case TreeView.Handle.TriangleSmall: return "►";
            case TreeView.Handle.TriangleOutline: return "▷";
            case TreeView.Handle.TriangleSmallOutline: return "⊳";
            case TreeView.Handle.Chevron: return "❱";
            case TreeView.Handle.Arrow: return "➤";
            default: return "▶";
        }
    }
    
    focus: true;
    
    Keys.onUpPressed: {
        console.log('TODO: Get prev item');
    }
    Keys.onLeftPressed: {
        console.log('TODO: Collapse current item if possible');
    }
    Keys.onRightPressed: {
        console.log('TODO: Expand current item if possible');
    }
    Keys.onDownPressed: {
        console.log('TODO: Get next item');
    }
}
