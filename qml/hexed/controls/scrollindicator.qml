import QtQuick
import QtQuick.Controls

ScrollIndicator {
    id: control;
    
    contentItem: Rectangle {
        implicitWidth: 4
        implicitHeight: 100
        radius: 2;
        color: "#aaaaaa"
        
        opacity: (control.active && control.size < 1.0) ? 0.75 : 0
        
        Behavior on opacity {
            NumberAnimation {}
        }
    }
}
