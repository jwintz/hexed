import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

import hexed.Backend  as H
import hexed.Core     as H
import hexed.Controls as H
import hexed.Frontend as H
import hexed.LSP      as HLSP

Control {
    id: self

    property var _editor: null

    // HLSP.Helper {
    //     id: _lsp_helper
    // }}
    
    ColumnLayout {
        
        anchors.fill: parent;
        spacing: 0;
        
        Item {

            Layout.fillWidth: true
            Layout.fillHeight: true
            
            Layout.minimumHeight: 28 - 2;
            Layout.maximumHeight: 28 - 2;

            Label {
                text: 'Diagnostics'

                // font.pointSize: 11
            
                anchors.centerIn: parent
            }
            
            Rectangle {

                id: _diagnostics_indicator;

                anchors.right: parent.right;
                anchors.rightMargin: 20;
                anchors.verticalCenter: parent.verticalCenter;

                width: 12;
                height: 12;
                radius: 6;
                color: "#7DCD85";

                opacity: 0;

                SequentialAnimation on opacity {

                    id: _diagnostics_indicator_animation;

                    loops: Animation.Infinite
                    running: false;

                    PropertyAnimation { to: 1; duration: 500; }
                    PropertyAnimation { to: 0; duration: 500; }
                }
            }
        }

        Rectangle {
            Layout.fillWidth: true;
            height: 1
            color: "#66666266";
        }

        ListView {

            id: _diagnostics;

            Layout.fillWidth: true;
            Layout.fillHeight: true;

            clip: true;

            model: ListModel {
                id: _diagnostics_model;
            }

            delegate: Control {

                id: _diagnostics_delegate

                width: _diagnostics.width
                height: 22;

                property int severity: model.severity
                property string source: model.source
                property string message: model.message

                RowLayout {

                    anchors.fill: parent;

                    Label {
                        text: model.line;
                        Layout.minimumWidth: 30;
                        Layout.maximumWidth: 30;
                        leftPadding: 10;
                    }
                    
                    Label {
                        Layout.fillWidth: true;
                        
                        text: model.message;
                    }
                }

                H.ToolTip {
                    position: Qt.AlignLeft;

                    text: 'Origin: ' + _diagnostics_delegate.source + ' Severity: ' + _diagnostics_delegate.severity + ' Message: ' + _diagnostics_delegate.message

                    visible: _diagnostics_delegate.hovered;

                    color:
                    _diagnostics_delegate.severity <= 1 ? '#b07745'
                        : _diagnostics_delegate.severity <= 2 ? '#D4A67A'
                        : _diagnostics_delegate.severity <= 3 ? '#765f4c'
                        : 'red';
                }
                
                background: Rectangle {
                    color: index % 2 ? "transparent" : "#11000000";
                }
            }
        }
    }
    
    // Connections {

    //     target: _lsp_helper;

    //     function onMonitoring() {

    //         _diagnostics_indicator.opacity = 0;
    //         _diagnostics_indicator_animation.running = true;
    //     }

    //     function onDiagnostics(items) {

    //         _editor.errors.clear();
    //         _diagnostics_model.clear();

    //         for(var i = 0; i < items.length; i++) {
    //             _editor.errors.append(items[i]);
    //             _diagnostics_model.append(items[i]);
    //         }

    //         _diagnostics_indicator_animation.running = false;
    //         _diagnostics_indicator.opacity = 1;
    //     }

    //     function onCompletions(items) {

    //         _editor.completions.clear();

    //         for(var i = 0; i < items.length; i++) {
    //             _editor.completions.append(items[i]);
    //         }
    //     }
    // }

    function setup_lsp_h() {
        
        // self._editor = H.Layout.bt.curt.item;
        
        // let buffer_file = H.Layout.bt.curt.item.buffer.file;
        
        // _lsp_helper.setup(buffer_file, self._editor.edit);
    }
    
    Component.onCompleted: {
        // H.Hooks.add_hook('file_find_hooks',     setup_lsp_h);
        // H.Hooks.add_hook('buffer_switch_hooks', setup_lsp_h);
        // H.Hooks.add_hook('window_focus_hooks',  setup_lsp_h);
    }
}
