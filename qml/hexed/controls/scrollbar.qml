import QtQuick
import QtQuick.Controls

ScrollBar {
    id: self

    implicitWidth: Math.max(implicitBackgroundWidth + leftInset + rightInset, implicitContentWidth + leftPadding + rightPadding)
    implicitHeight: Math.max(implicitBackgroundHeight + topInset + bottomInset, implicitContentHeight + topPadding + bottomPadding)

    padding: 0
    visible: self.policy !== ScrollBar.AlwaysOff
    rightPadding: 4
    leftPadding: 4

    contentItem: Rectangle {
        implicitWidth: 6
        implicitHeight: 100
        radius: width / 2
        color: "#4A434D"
    }
    
    background: Item {}
}
