import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

import hexed.Controls as H

H.Dialog {
    
    id: self;
    
    implicitWidth: window.width * 3/4;
    implicitHeight: window.height * 3/4;
    
    // --
    
    property var candidate_index: 0;
    
    property alias model: bufferModel;
    
    // --
    
    function reset() {
        candidate_index = 0;
    }
    
    // --
    
    ListModel {
        id: bufferModel;
    }
    
    contentItem: ListView {
        
        id: candidates;
        
        anchors.fill: parent;
        anchors.margins: 8;
        
        model: bufferModel;
        
        focus: true;
        
        delegate: Item {
            
            width: ListView.view.width;
            height: 32;
            
            Rectangle {
        
                anchors.fill: parent;
                anchors.leftMargin: 2;
                anchors.rightMargin: 2;
        
                color: "#00000000";
        
                Text {
                    color: "white";
                    text: buffer;
                    
                    anchors.verticalCenter: parent.verticalCenter;
                    anchors.left: parent.left;
                    anchors.leftMargin: 9;
                }
                
                Text {
                    color: "#999999";
                    text: path;
                    
                    anchors.verticalCenter: parent.verticalCenter;
                    anchors.left: parent.left;
                    anchors.leftMargin: self.width / 2 + 10;
                }
            }
        }
        
        highlight: Rectangle {
               
            width: candidates.width;
            height: 32;
               
            y: candidates.currentItem ? candidates.currentItem.y : 0;
               
            color: "#44000000";
               
            radius: 4;
               
            Behavior on y { NumberAnimation { duration: 100 } }
        }
        
        Keys.onDownPressed: {
            
            if (candidates.currentIndex <  candidates.count - 1)
                candidates.currentIndex = (candidates.currentIndex + 1);
            
            self.candidate_index = candidates.currentIndex;
        }
        
        Keys.onUpPressed: {
            
            if (candidates.currentIndex > 0)
                candidates.currentIndex = (candidates.currentIndex - 1);
            
            self.candidate_index = candidates.currentIndex;
        }
        
        Keys.onReturnPressed: {
            self.accept();
            self.reset();
        }
        
        Keys.onEscapePressed: {
            self.reject();
            self.reset();
        }
        
        Keys.onPressed: (event) => {
            
            if (event.modifiers & Qt.MetaModifier && event.key == Qt.Key_G) {
                event.accepted = true;
                self.reject();
                self.reset();
            }
        }
        
        ScrollIndicator.vertical: ScrollIndicator {
            visible: candidates.contentHeight > candidates.height
            
            contentItem: Rectangle {
                implicitWidth: 4
                radius: 2;
                color: "#aaaaaa"
            }
        }
    }
}
