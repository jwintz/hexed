import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

Dialog
{
    id: self
        
    implicitWidth: 600;
    implicitHeight: 200;
    
    height: implicitHeight;
    
    Behavior on implicitHeight { NumberAnimation { duration: 100; } }
    
    x: (parent.width  - implicitWidth)  / 2
    y: (parent.height - implicitHeight) / 2
    
    parent: Overlay.overlay
    
    modal: true
    focus: true
     clip: true
    
    enter: Transition
    {
        NumberAnimation { property: "scale"; from: 0.9; to: 1.0; easing.type: Easing.OutQuint; duration: 220 }
        NumberAnimation { property: "opacity"; from: 0.0; to: 1.0; easing.type: Easing.OutCubic; duration: 150 }
    }
    
    exit: Transition
    {
        NumberAnimation { property: "scale"; from: 1.0; to: 0.9; easing.type: Easing.OutQuint; duration: 220 }
        NumberAnimation { property: "opacity"; from: 1.0; to: 0.0; easing.type: Easing.OutCubic; duration: 150 }
    }
    
    property color overlayColor: "#66000000"
    
    palette.base: "#282526"
    
    background: Rectangle
    {
        radius: 4
        
        color: "#221E28"
        
        width: self.implicitWidth;
        height: self.implicitHeight;
        
        border.width: 1;
        border.color: "#3E3E3E";
    }
    
    Overlay.modal: Rectangle
    {
        color: self.overlayColor
        Behavior on opacity { NumberAnimation { duration: 150 } }
    }
    
    Overlay.modeless: Rectangle
    {
        color: self.overlayColor
        Behavior on opacity { NumberAnimation { duration: 150 } }
    }
}

