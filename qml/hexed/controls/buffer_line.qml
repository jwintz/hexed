import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import QtQuick.Shapes

import hexed.Core as H

Control {
    id: self;
    
    property var value;
    property var edit;
    
    property var current_line;
    property var current_column;
    
    property var selection_char_count;
    property var selection_line_count;
    
    property var file: "";
    
    property var active: edit.activeFocus;
    
    property var hud_total;
    property var hud_displayed;
    property var hud_position;
    
    property var contents;
    
    height: 28;

    clip: true;
    
    // ---
    
    Rectangle {
        height: 1;
        color: "#4A434D";
        anchors.left: self.left;
        anchors.right: self.right;
        anchors.top: self.top;
    }
    
    RowLayout {
        
        anchors.fill: parent;
        
        spacing: 0;
        
        // Item { // Spacer
        //     width: 1;
        //     height: parent.height;
        // }
        
        // HUD segment
        
        Rectangle {
            width: 4;
            y : 1
            height: parent.height - 2;
            clip: true;
            
            color: "#221E28"
            
            Rectangle {
                
                x: 0;
                y: self.hud_position / self.hud_total * self.height;
                
                width: 4;
                height: self.hud_displayed / self.hud_total * self.height;
                
                color: self.active ? "#cab2fa" : "#777777"
            }
        }
        
        Item { // Spacer
            width: 4;
            height: parent.height;
        }
        
        // Buffer id segment
        
        Item {
            width: height;
            height: parent.height;
            clip: true;
            
            Rectangle {
                
                anchors.fill: parent;
                anchors.margins: 8;
                radius: 2;
                
                color: self.active ? "#cab2fa" : "#777777"
                
                Text {
                    anchors.centerIn: parent;
                    
                    color: "#222222";
                    
                    font.family: "Operator Mono";
                    font.pointSize: self.edit.font.pointSize - 2;
                    
                    text: self.value;
                }
            }
        }
        
        Item { // Spacer
            width: 4;
            height: parent.height;
        }
        
        // FileName segment
        
        // Shape {
        //     antialiasing: true;
        //     Layout.maximumWidth: 14;
        //     Layout.minimumWidth: 14;
        //     Layout.fillHeight: true;
        //     ShapePath {
        //         fillColor: edit.activeFocus ? "#221E28" : "#dd221E28"
        //         strokeWidth: 1
        //         strokeColor: "#00000000"
        //         startX: 0; startY: -1
        //         PathLine { x: 15; y: -1 }
        //         PathLine { x: 15; y: 30 }
        //     }
        // }
        
        Item {
            width: 100;
            height: parent.height;
            clip: true;
            
            // color: edit.activeFocus ? "#221E28" : "#dd221E28"
            
            Rectangle {
                height: parent.height;
                 width: parent.width;
                 
                y: -4;
                
                radius: 8;
                 
                color: "#221E28";
                 
                border.width: 1;
                border.color: "#4A434D";
                
                Rectangle { width: parent.width; height: 8; color: "#221E28" }
                Rectangle { width: 1; height: 8; color: "#4A434D"; anchors.left:  parent.left }
                Rectangle { width: 1; height: 8; color: "#4A434D"; anchors.right: parent.right }
                
                visible: self.active
            }
            
            Label {
                
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.top: parent.top
                anchors.topMargin: self.active ? 5 : parent.height / 2 - height / 2;
                
                font.family: "Operator Mono";
                font.pointSize: self.edit.font.pointSize - 2
                
                color: self.active ? "#ffffff" : "#777777";
                text: fs_helper.fileName(self.file);
            }
        }
        
        // Shape {
        //     antialiasing: true;
        //     Layout.maximumWidth: 14;
        //     Layout.minimumWidth: 14;
        //     Layout.fillHeight: true;
        //     ShapePath {
        //         fillColor: edit.activeFocus ? "#221E28" : "#dd221E28"
        //         strokeWidth: 1
        //         strokeColor: "#00000000"
        //         startX: 0; startY: -1
        //         PathLine { x: 15; y: -1 }
        //         PathLine { x: 0; y: 30 }
        //     }
        // }
        
        // Position segment
        
        Item { // Spacer
            width: 8;
            height: parent.height;
        }
        
        Text {
            
            font.family: "Operator Mono"
            font.pointSize: self.edit.font.pointSize - 2
            
            color: self.active ? "#ffffff" : "#777777"
            
            text: self.current_line + ":" + self.current_column + ' ' + fs_helper.size(self.contents);
        }
        
        Item {
            width: 8;
            height: parent.height;
        }
        
        Text {
            font.family: "Operator Mono"
            font.pointSize: self.edit.font.pointSize - 2
            
            color: "#fec88f"
            
            text: self.selection_char_count + "C" + " " +  self.selection_line_count + "L"
            
            visible: self.selection_char_count != 0
        }
                
        Item {
            Layout.fillWidth: true;
        }
        
        // Mode segment
        
        Item {
            width: 100;
            height: parent.height;
            clip: true;
            
            Text {
                anchors.centerIn: parent;
                
                font.family: "Operator Mono";
                font.pointSize: self.edit.font.pointSize - 2
                
                color: self.active ? "#cab2fa" : "#554771";
                
                text: "Fundamental";
            }
        }
        
        // VCS Segment
        
        // Shape {
        //     antialiasing: true;
        //     Layout.maximumWidth: 14;
        //     Layout.minimumWidth: 14;
        //     Layout.fillHeight: true;
        //     ShapePath {
        //         fillColor: edit.activeFocus ? "#221E28" : "#dd221E28"
        //         strokeWidth: 1
        //         strokeColor: "#00000000"
        //         startX: 0; startY: -1
        //         PathLine { x: 15; y: -1 }
        //         PathLine { x: 15; y: 30 }
        //     }
        // }
        
        Item {
            width: 100
            height: parent.height
            clip: true
            
            Rectangle {
                height: parent.height;
                 width: parent.width + 10;
                 
                y: -4;
                
                radius: 8;
                 
                color: "#221E28";
                 
                border.width: 1;
                border.color: "#4A434D";
                
                Rectangle { width: parent.width; height: 8; color: "#221E28" }
                Rectangle { width: 1; height: 8; color: "#4A434D"; anchors.left:  parent.left }
                
                visible: self.active
            }

            Label {
                id: vcs_segment_icon;
                
                anchors.top: parent.top
                anchors.topMargin: self.active ? 5 : parent.height / 2 - height / 2;
                anchors.right: vcs_segment_text.left
                anchors.rightMargin: 4
                
                color: self.active ? "#ffffff" : "#777777"
                
                font.family: "Material Design Icons"
                font.pointSize: self.edit.font.pointSize + 2
                text: '󰘬'
                
                visible: false;
            }
            
            Text {
                id: vcs_segment_text;
                
                anchors.left: parent.horizontalCenter;
                anchors.leftMargin: vcs_segment_icon.visible ? -width/2 + vcs_segment_icon.width / 2 : -width/2;
                anchors.top: parent.top
                anchors.topMargin: self.active ? 5 : parent.height / 2 - height / 2;
                
                font.family: "Operator Mono";
                font.pointSize: self.edit.font.pointSize - 2
                
                color: self.active ? "#ffffff" : "#777777"
                text: 'Unversioned';
            }
        }
    }
    
    background: Rectangle {
        color: self.active ? Qt.lighter("#221E28") : Qt.lighter("#dd221E28")
    }
}
