import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

import hexed.Backend  as H
import hexed.Core     as H
import hexed.Controls as H
import hexed.Frontend as H

H.TreeView {

    rowPadding: 18;
    
    model: H.FSModel {
        id: fs_model
    }
    
    selectionEnabled: true
    
    contentItem: RowLayout {
        
        spacing: 5;
        
        Text {
            color: currentRow.isSelectedIndex ? "#ffffff" : "#ae91e8";
            text: currentRow.hasChildren ? '󰉋' : '󰈙';
            
            font.family: "Material Design Icons";
            font.pointSize: 16;
        }
        
        Text {
            text: currentRow.currentData
            font.pointSize: 14;
            color: "#ffffff";
        }
        
        Item { Layout.fillWidth: true; }
    }
    
    handle: Text {
        color: "#cccccc";
        text: '󰅂';
        
        visible: currentRow.hasChildren
        
        rotation: !currentRow.expanded ? 0 : 90;
        
        font.family: "Material Design Icons";
        font.pointSize: 16;
        
        anchors.top: parent.top;
        anchors.left: parent.left;
        
        Behavior on rotation { NumberAnimation { duration: 50 } }
    }
    
    highlight: Item {
        Rectangle {
            color: "#6C5B8F"
            width: parent.width
            height: parent.height
            anchors.left: parent.left
            radius: 4
        }
        
        Behavior on y { NumberAnimation { duration: 50 }}
    }
    
    // onCurrentIndexChanged: console.log("current index is (row=" + currentIndex.row + ", depth=" + model.depth(currentIndex) + ")")
    // onCurrentDataChanged: console.log("current data is " + currentData)
    // onCurrentItemChanged: console.log("current item is " + currentItem)
    
    // ////////////////////////////////////////////////////////////////////////////
    
// ////////////////////////////////////////////////////////////////////////////
// Hooks
// ////////////////////////////////////////////////////////////////////////////

    function project_navigator_root_path_h() {
        
        let buffer_file = H.Layout.bt.curt.item.buffer.file;
        
        // console.log('navigator_project.qml - project_navigator_root_path_h:', 'file is', buffer_file);
        
        if (buffer_file.startsWith('*')) {
            fs_model.clear();
            return;
        }
        
        let project_root_path = fs_helper.projectPath(buffer_file);
        
        // console.log('navigator_project.qml - project_navigator_root_path_h:', 'found project root path', project_root_path);
        
        fs_model.setRootPath(project_root_path);
    }

// ////////////////////////////////////////////////////////////////////////////
    
    Component.onCompleted: {
        
        // fs_model.setRootPath('/Users/jwintz/Development/hexed');
        
        H.Hooks.add_hook('file_find_hooks',     project_navigator_root_path_h);
        H.Hooks.add_hook('buffer_switch_hooks', project_navigator_root_path_h);
        H.Hooks.add_hook('window_focus_hooks', project_navigator_root_path_h);
    }
}
