import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

import hexed.Core     as C
import hexed.Controls as H
import hexed.Frontend as H

FocusScope {

    id: self;

    property var node;
    property var value;
    property var buffer;
    
    property alias edit: _textEdit;
    property alias document: _textEdit.textDocument;
    property alias actualContents: _textEdit.text;

    property alias errors: _error_model;
    property alias completions: _completion_model;

    property alias contents: _textEdit.text;

    property alias display_lines: _lines.visible;
    property alias display_highl: _highl.visible;
    
    property alias current_line: d.line;
    property alias current_column: d.column;

    property alias selection_char_count: d.selectedCharCount;
    property alias selection_line_count: d.selectedLineCount;
    
    // --
    
    onContentsChanged: {
    
        if(!buffer)
            return;
    
        buffer.contents = contents;
    
        H.Layout.bt.depthFirstPostOrder((node) => {
            if (node.value && node != self.node && node.file == self.node.file) {
                node.item.contents = contents;
            }
        });
    }
    
    onCurrent_lineChanged: {
        buffer.line = current_line;
          node.line = current_line;
    }
    
    onCurrent_columnChanged: {
        node.column = current_column;
    }
    
    onFocusChanged: {
        if (self.activeFocus && H.Layout.bt.curt.value != self.value)
            H.Layout.focus(H.Layout.bt.find(self.value));
    }
    
    // --
    
    clip: true;
    
    ListModel {
        id: _completion_model;
    }

    H.BufferLine {
        id: buffer_line;
        
        file: self.buffer ? self.buffer.file : "";
        edit: self.edit;
        value: self.value;
        contents: self.contents;
        
        current_line: self.current_line;
        current_column: self.current_column;
        
        selection_char_count: self.selection_char_count;
        selection_line_count: self.selection_line_count;
        
        hud_total: _flickable.contentHeight;
        hud_displayed: _flickable.height;
        hud_position: _flickable.contentY;
        
        anchors.bottom: self.bottom;
        anchors.left: self.left;
        anchors.right: self.right;
    }
    
    Rectangle {
    
        id: buffer_bg;
        
        anchors.left: _lines.right;
        anchors.top: self.top;
        anchors.topMargin: 0;
        anchors.bottom: buffer_line.top;
        anchors.right: self.right;
        
        // color: edit.activeFocus ? "#221E28" : "#dd221E28" // TODO: BACKGROUND - get from theme
        color: "#221E28"
        
        Behavior on color { ColorAnimation { duration: 200 } }
    }
    
    Flickable {
        id: _flickable;

        anchors.left: _lines.right;
        anchors.top: self.top;
        anchors.topMargin: 0;
        anchors.bottom: buffer_line.top;
        anchors.right: self.right;

        contentWidth: _textEdit.width;
        contentHeight: _textEdit.height;

        boundsBehavior: Flickable.StopAtBounds
        
        clip: true;

        function updateScrollX(x) {
            if (contentX >= x) {
                contentX = x;
            } else if (contentX + width <= x) {
                contentX = x + 1 - width;
            }
        }

        function updateScrollY(y) {
            if (contentY >= y) {
                contentY = y;
            } else if (contentY + height <= y + _textEdit.cursorHeight) {
                contentY = y + _textEdit.cursorHeight - height;
            }
        }

        Behavior on contentY { id: v_scroll_animation; enabled: false; NumberAnimation { duration: 400 } }
        
        Rectangle {

            id: _highl;

            anchors.left: parent.left;
            anchors.right: parent.right;
        
            y: _textEdit.cursorY;
            height: _textEdit.cursorHeight;

            color: "#3F3553";
            
            Rectangle {
                
                anchors.top: _highl.top;
                anchors.left: _highl.left;
                anchors.right: _highl.right;
                
                color: "#716196";
                
                height: 1;
            }
            
            Rectangle {
                
                anchors.left: _highl.left;
                anchors.right: _highl.right;
                anchors.bottom: _highl.bottom;
                
                color: "#716196";
                
                height: 1;
            }
        }
        
        TextEdit {
            id: _textEdit

            focus: true
            
            readOnly: false
            
            font.family: cartograph.name
            font.pointSize: 12
            font.weight: Font.Light

            selectByMouse: true
            mouseSelectionMode: TextEdit.SelectCharacters
            selectionColor: "#6A598B"
            color: "#f1ebff"

            textFormat: TextEdit.PlainText
            wrapMode: TextEdit.NoWrap
            
            width: self.width - _lines.width;
            //height: Math.max(implicitHeight, self.height);

            topPadding: 0;
            leftPadding: 4;

            property int cursorX: cursorRectangle.x;
            property int cursorY: cursorRectangle.y;
            property int cursorHeight: cursorRectangle.height;
            property bool showCursor: true;

            // text: self.contents;

            onCursorXChanged: {
                _flickable.updateScrollX(cursorX);
            }

            onCursorYChanged: {
                _flickable.updateScrollY(cursorY);
            }

            Rectangle {
                
                id: cursor;
                
                radius: 1;
                width: 2;
                color: "#AE91E8";
                
                x: 4;
                y: 0;
                
                height: 0;
                
                Behavior on x { NumberAnimation { duration: 75 } }
                // Behavior on y { NumberAnimation { duration: 50 } }
                
                // SequentialAnimation on opacity { running: true; loops: Animation.Infinite;
                //     NumberAnimation { to: 0.5; duration: 500; easing.type: "OutQuad"}
                //     NumberAnimation { to: 1.0; duration: 500; easing.type: "InQuad"}
                // }
                
                visible: _textEdit.showCursor;
            }
            
            cursorDelegate: Item {
                
                id: cursor_delegate;
                
                onXChanged: cursor.x = x;
                onYChanged: cursor.y = y;
                onHeightChanged: cursor.height = height;

                // Popup {
                //     id: _completion_popup;

                //     y: _textEdit.cursorHeight + 10;

                //     width: 200
                //     height: Math.min(_completion_model.count * 32, 300);

                //     clip: true;
                //     modal: false;
                //     focus: false;
                //     closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutsideParent

                //     visible: _completion_model.count != 0;

                //     topPadding: 0;
                //     leftPadding: 0;
                //     rightPadding: 0;
                //     bottomPadding: 0;

                //     ListView {

                //         id: _completion_popup_view;

                //         anchors.fill: parent;

                //         model: _completion_model;

                //         clip: true;

                //         delegate: ItemDelegate {

                //             id: _completion_delegate;

                //             height: 32;

                //             text: model.candidate;
                //             width: _completion_popup_view.width;

                //             onClicked: _completion_popup.close();

                //             font: _textEdit.font;
                //         }
                //     }
                // }
            }
            
            Keys.onPressed: (event) =>
            {
                if (commands.attempt(event))
                    event.accepted = false;
            }
        }

        Repeater {

            model: ListModel {
                id: _error_model;
            }

            delegate: Rectangle {

                anchors.left: parent.left;
                anchors.right: parent.right;

                height: _textEdit.cursorHeight;
                y: (Math.max(model.line - 1, 0)) * height;
                color: "#22C54632";

                Rectangle {
                    anchors.right: parent.right;
                    anchors.top: parent.top;
                    anchors.bottom: parent.bottom;
                    anchors.rightMargin: 2;

                    width: _error.width + 16;
                    radius: 2;
                    color: "#933628"

                    Row {
                        id: _error;
                        
                        anchors.centerIn: parent;
                        spacing: 8;
                        Label {
                            anchors.verticalCenter: parent.verticalCenter;
                            font.pointSize: _textEdit.font.pointSize;
                            text: model.message;
                            color: "#eeeeee";
                        }
                    }
                }
            }
        }

        ScrollIndicator.vertical: H.ScrollIndicator {}
    }

    Item {
        id: _lines;

        anchors.left: self.left;
        anchors.top: self.top;
        anchors.topMargin: 0;
        anchors.bottom: buffer_line.top;

        clip: true;
        
        width: _lines.visible ? _linesCol.width + 4 : 0;

        Rectangle {
            
            id: _l_highl_bg;
        
            anchors.fill: parent;
            
            color: edit.activeFocus ? "#221E28" : "#dd221E28" // TODO: BACKGROUND - get from theme
            
            Behavior on color { ColorAnimation { duration: 200 } }
            
            Rectangle {
                anchors.top: _l_highl_bg.top;
                anchors.right: _l_highl_bg.right;
                anchors.bottom: _l_highl_bg.bottom;
                width: 1;
                color: "#27212E";
            }
            
            Rectangle {

                id: _l_highl;

                anchors.left: _l_highl_bg.left;
                anchors.right: _l_highl_bg.right;
                
                y: _textEdit.cursorY - _flickable.contentY;
                height: _textEdit.cursorHeight;

                color: "#3F3553";
                
                Rectangle {
                    
                    anchors.top: _l_highl.top;
                    anchors.left: _l_highl.left;
                    anchors.right: _l_highl.right;
                    
                    color: "#716196";
                    
                    height: 1;
                }
                
                Rectangle {
                    
                    anchors.left: _l_highl.left;
                    anchors.right: _l_highl.right;
                    anchors.bottom: _l_highl.bottom;
                    
                    color: "#716196";
                    
                    height: 1;
                }
            }
        }
        
        Column {
            id: _linesCol;

            y: -_flickable.contentY;

            Repeater {
                model: _textEdit.lineCount;

                delegate: Text {
                    anchors.right: _linesCol.right;
                    rightPadding: 8;
                    leftPadding: 8;

                    height: _textEdit.cursorHeight;
                    
                    font.family: _textEdit.font.family;
                    font.pointSize: _textEdit.font.pointSize;

                    verticalAlignment: Text.AlignVCenter;

                    color: index + 1 == self.current_line ? "#8B7E9C" : "#544d60";
                    text: index + 1;
                }
            }
        }
    }

// --

    function setCurrentLine(line) {
        d.line = line;
    }

    function scroll_up() {
        
        console.log('TODO: Use d to compute position on page down IF possible');
    }
    
// --

    C.Buffer {
        id: d;

        Component.onCompleted: {
            setup(self.edit);
        }
    }
}
